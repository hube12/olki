ifndef VERBOSE
.SILENT:
endif
SHELL := /bin/bash
PATH := $(HOME)/.poetry/bin:$(PATH)
POETRY_VERSION := 1.0.0a4

# thanks https://stackoverflow.com/a/47008498
args = `arg="$(filter-out $@,$(MAKECMDGOALS))" && echo $${arg:-${1}}`

# thanks https://gist.github.com/prwhite/8168133#gistcomment-2749866
.PHONY: help
help:
	@bash support/makefile/banner

	@awk '{ \
			if ($$0 ~ /^.PHONY: [a-zA-Z\-\_0-9]+$$/) { \
				helpCommand = substr($$0, index($$0, ":") + 2); \
				if (helpMessage) { \
					printf "\033[36m%-20s\033[0m %s\n", \
						helpCommand, helpMessage; \
					helpMessage = ""; \
				} \
			} else if ($$0 ~ /^[a-zA-Z\-\_0-9.]+:/) { \
				helpCommand = substr($$0, 0, index($$0, ":")); \
				if (helpMessage) { \
					printf "\033[36m%-20s\033[0m %s\n", \
						helpCommand, helpMessage; \
					helpMessage = ""; \
				} \
			} else if ($$0 ~ /^##/) { \
				if (helpMessage) { \
					helpMessage = helpMessage"\n                     "substr($$0, 3); \
				} else { \
					helpMessage = substr($$0, 3); \
				} \
			} else { \
				if (helpMessage) { \
					print "\n                     "helpMessage"\n" \
				} \
				helpMessage = ""; \
			} \
		}' \
		$(MAKEFILE_LIST)

## -- Development tasks --

## Launch development environment (front)
serve-dev-fe:
	@bash support/makefile/utils ">> Starting frontend at http://localhost:3000 with HMR"
	@cd olki_front ; DEBUG=1 npm run dev

## Launch development environment (back)
serve-dev-be:
	@bash support/makefile/utils ">> Starting backend at http://localhost:5000"
	@$(MAKE) manage.dev runserver 5000

## manage + .env.test
manage.dev:
	@$(MAKE) manage $(call args,'')

## shell  + .env.test
shell.dev:
	@$(MAKE) manage.dev shell_plus DEBUG=1

## Launch Django's test suite
test-be:
	@rm -rf pip-wheel-metadata/olki-0.1.0.dist-info
	# or run `pytest` with the arguments specified in `olki_back/setup.cfg`
	@cd olki_back ; poetry run tox

test-be-pytest-only:
	# @$(MAKE) manage.dev -- test -- -v 2
	@cd olki_back ; poetry run pytest

## Launch test suite for the front-end
test-fe:
	@cd olki_front ; npm run test

## -- Maintenance tasks --

## Access Django's manage.py
manage:
	@$(MAKE) run python olki_back/manage.py -- $(call args,'')

## Access Django's REPL (using manage.py, shell_plus and debug mode)
shell:
	@$(MAKE) manage shell_plus DEBUG=1

## Update the source to the latest stable release (doesn't update the dependencies)
update-release:
	@git fetch
	@git checkout $(git tag -l | sort -Vr | head -n 1)

## Commits a tag, generates changelog, signs packages and uploads a full release
release:
	@command -v release-it >/dev/null 2>&1 || npm install -g release-it # if we lack release-it, we can install it
	@release-it --config support/makefile/.release-it.json

generate-setuppy-reqs:
	@poetry run poetry-setup

## Install development dependencies (production + extra depedencies)
deps-dev:
	@bash support/makefile/utils ">> Checking if needed dependencies are installed"
	@$(MAKE) deps-dev-be
	@$(MAKE) deps-dev-fe

deps-dev-be:
	@command -v poetry >/dev/null 2>&1 || pip install --no-cache-dir --pre -I poetry==${POETRY_VERSION} # if we lack poetry, we can install it
ifndef CI
	@cd olki_back ; poetry install --no-interaction # backend dependencies (python)
else
	@cd olki_back ; poetry install -vvv --no-interaction # backend dependencies (python)
endif

deps-dev-fe:
	@command -v node >/dev/null 2>&1 || exit 1 # if we lack node, we can only fail since it is not our responsibility to install it
	@cd olki_front ; npm i # frontend dependencies (javascript)

## Install production dependencies (production-only dependencies)
deps-prod:
	@bash support/makefile/utils ">> Checking if needed production dependencies are installed"
	@$(MAKE) deps-prod-be

deps-prod-be:
	@command -v poetry >/dev/null 2>&1 || pip install --no-cache-dir --pre -I poetry==${POETRY_VERSION}; # if we lack poetry, we can install it
	@cd olki_back ; poetry install --no-interaction --no-dev # backend dependencies (python)

## -- Production tasks --

run:
	@poetry run $(call args,' --help')

## Launch production environment (EXPERIMENTAL)
serve-prod:
	@bash support/makefile/utils ">> Starting at http://localhost:5000"
	@poetry run python olki_back/manage.py collectstatic --noinput
	@poetry run python olki_back/manage.py collecttemplates
	@poetry run honcho start

post-update:
	@bash support/makefile/utils ">> Running database migration"
	@$(MAKE) manage -- migrate --noinput
	@bash support/makefile/utils ">> Clearing application cache"
	@$(MAKE) manage clear_cache
	@bash support/makefile/utils ">> Running dynamic preferences checks"
	@$(MAKE) manage checkpreferences
	@bash support/makefile/utils ">> Importing FE"
	@$(MAKE) manage collectstatic --no-input -c
	@bash support/makefile/utils ">> Extracting index from FE"
	@$(MAKE) manage collecttemplates

build-fe:
	@bash support/makefile/utils ">> Building OLKi FE"
	@cd olki_front ; npm run build

%:
	@:
