# OLKi

---

**[Website][website]** |
**[Create an instance](#-create-your-own-instance)** |
**[Documentation](#-documentation)** |
**[Contact][contact]**

A self-hosted linguistic corpora exchange platform, that aims to be a simple
gateway to the fediverse for scientific interaction.

To get a copy of the source code and run a local development instance:

```bash
git clone https://framagit.org/synalp/olki/olki --single-branch --depth=1
cd olki
make # to see documentation for the main tasks
```

To see the web client, open two terminals at the project root. Type `make deps-dev-be serve-dev-be` in one, and `make deps-dev-fe serve-dev-fe` in the other. Open your browser to https://localhost:3000. See below to run a production instance.

## :package: Create your own instance

Be part of a wider network of dataset and corpora hosters, with no lock-in
and no fee. Support open research and provide a place for your research teams
to exchange about, document and spread their work with other <span title="users of an OLKi instance">olkindwellers</span> and <span title="wider federated network of instances running compatible protocols (especially flavors of ActivityPub)">fediverse</span> residents.

Production dependencies:

  * any reverse-proxy to provide SSL/TLS termination (we only provide Nginx and Caddy configurations, available in `support/`)
  * PostgreSQL >= 9.6 to run the backend
  * Redis >= 2.8.18 to run the backend
  * Python >= 3.7.x to run the backend

See the [production install guide][install_guide] for a complete setup from source. (SOON)

See the [docker deployment][docker compose] and its [documentation][docker documentation] a complete setup using docker compose.

## :book: Documentation

We try to be as reproduceable as research should be!

See the [user documentation], [admin documentation] or [development documentation].
See the explanations for our technical choices in our [white paper].

To interact with an instance, you can:
  
  * use its [GraphQL API][graphql]
  * be part of the federated network and support the [ActivityPub Activities][activitypub]
    dialect of OLKi
  * use one of the open formats OLKi supports (RSS, OAI-PMH, ActivityPub)
  
## :raised_hands: Contributing, bugs and feedback

OLKi is in very early development, and *will* have the odd rough edge here and there. 
Please be vocal over on the [OLKi issue tracker][issues]. :speech_balloon:

You can also chat with us on [#olki:matrix.org][matrix].

All interactions within the project are subject to our [code of conduct][coc].

## Public money, public code

This work was supported by the french PIA project “Lorraine Université
d’Excellence”, reference ANR-15-IDEX-04-LUE, as part of the [Open Language and Knowledge for Citizen project](https://olki.loria.fr), and developed by Université de Lorraine and its laboratories [LORIA], [ATILF], [IECL], [Archives Henri Poincaré]
and [CREM]. We are concerned about ensuring OLKi is useful not only to
universities, but also governments, businesses and people around the world. But
more than that, we are concerned that the derivatives of our code stay true to
its original mission, so OLKi is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the [GNU Affero
General Public License][agpl] as published by the Free Software Foundation, either
version 3 of it, or (at your option) any later version.  

[issues]: https://framagit.org/synalp/olki/olki/issues
[website]: https://olki.loria.fr/platform/
[contact]: #-contributing-bugs-and-feedback
[install_guide]: # "work in progress"
[user documentation]: https://framagit.org/synalp/olki/olki/wikis/User-documentation
[admin documentation]: https://framagit.org/synalp/olki/olki/wikis/Admin-documentation
[development documentation]: https://framagit.org/synalp/olki/olki/wikis/Admin-documentation
[white paper]: https://framagit.org/synalp/olki/documents/software-requirements-specification/releases
[graphql]: # "work in progress"
[activitypub]: https://framagit.org/synalp/olki/olki/wikis/home/ActivityPub-dialect-documentation
[matrix]: https://matrix.to/#/+olki:matrix.org
[coc]: ./CODE_OF_CONDUCT.md
[docker compose]: ./support/docker/docker-compose.yml
[docker documentation]: https://framagit.org/synalp/olki/olki/wikis/Admin-documentation

<!-- supports -->
[LORIA]: http://www.loria.fr
[ATILF]: http://www.atilf.fr
[IECL]: http://www.iecl.univ-lorraine.fr
[Archives Henri Poincaré]: http://poincare.univ-lorraine.fr
[CREM]: http://crem.univ-lorraine.fr

<!-- AGPL -->
[agpl]: https://www.gnu.org/licenses/agpl-3.0.en.html
