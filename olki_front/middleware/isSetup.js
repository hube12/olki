export default async function({ app, store, redirect }) {
  await store.dispatch('auth/syncUser')
  if (!store.getters['user/isSetup']) {
    return redirect(app.localePath('account-setup'))
  }
}
