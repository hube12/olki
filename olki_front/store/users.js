/**
 * Users is a module to cache information regarding a user, in a list of users.
 *
 * It also serves as a way to select a profile to display in the defaut+profileheader
 * array (used in /user/_id/* for instance)
 */
import exactUserGql from '~/apollo/queries/exactUser'

export const state = () => ({
  users: [], // list of cached users
  currentUser: '' // currently viewed username (used by the default+profileheader layout to look in the users array)
})

export const mutations = {
  addUser(state, data) {
    if (!state.users.find(user => user.pk === data.pk)) state.users.push(data)
  },
  removeUser(state, data) {
    state.users = state.users.filter(user => user.pk !== data.pk)
  },
  setCurrentUser(state, data) {
    state.currentUser = data
  }
}

export const actions = {
  async getUserByUsername({ state }, username) {
    // where username is the user's username
    const userExists = state.users.find(
      user => user.actor.username === username
    )

    if (userExists) return userExists

    try {
      const res = await this.app.apolloProvider.defaultClient
        .query({
          query: exactUserGql,
          variables: { username }
        })
        .then(({ data }) => data.user)
      return res
    } catch (e) {
      console.log(e)
    }
  }
}
