import authenticateUserGql from '~/apollo/mutations/authenticateUser'
import verifyTokenGql from '~/apollo/mutations/verifyToken'
import viewerMeGql from '~/apollo/queries/viewerMe'
import registerAccountGql from '~/apollo/mutations/registerAccount'
import activateAccountGql from '~/apollo/mutations/activateAccount'
import deleteAccountGql from '~/apollo/mutations/deleteAccount'
import resetPasswordGql from '~/apollo/mutations/resetPassword'
import resetPasswordConfirmGql from '~/apollo/mutations/resetPasswordConfirm'

export const state = () => ({
  isAuthenticated: false,
  isToRemember: false
})

export const mutations = {
  login(state, remember) {
    state.isAuthenticated = true
    state.isToRemember = remember
  },
  logout(state) {
    state.isAuthenticated = false
  }
}

export const actions = {
  // called by @plugins/client-init.js
  init({ commit, state }) {
    if (this.app.$apolloHelpers.getToken()) {
      commit('login', state.remember)
    }
  },
  async authentify({ commit, dispatch }, data) {
    try {
      // authentify with credentials
      const res = await this.app.apolloProvider.clients.noAuth
        .mutate({
          mutation: authenticateUserGql,
          variables: data.credentials
        })
        .then(({ data }) => data.login)

      if (res.success) {
        // store token
        await this.app.$apolloHelpers.onLogin(res.token, undefined, 7)
        // modify global store variables to show we have logged-in
        commit('login', data.remember)
        dispatch('syncUser')
        this.$router.push(this.app.localePath('home'))
      }
      return res
    } catch (e) {
      console.error(e)
    }
  },
  async verify({ commit, dispatch, rootGetters }) {
    try {
      // in the absence of token, we don't check it
      if (!this.app.$apolloHelpers.getToken()) {
        commit('logout')
        // No token - skipping verification.
        return
      }
      // verify the token
      // authentify with credentials
      const res = await this.app.apolloProvider.defaultClient.mutate({
        mutation: verifyTokenGql,
        variables: { token: this.app.$apolloHelpers.getToken() }
      })

      if (res.data.verifyToken) {
        // get profile if not stored
        if (rootGetters['profile/get'] !== {}) {
          dispatch('syncUser')
        }
      } else {
        commit('logout')
        this.app.$apolloHelpers.onLogout()
        this.$router.push(this.app.localePath('home'))
      }
    } catch (e) {
      commit('logout')
      this.app.$apolloHelpers.onLogout()
      this.$router.push(this.app.localePath('home'))
      console.error(e)
    }
  },
  async register({ commit, rootGetters }, payload) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: registerAccountGql,
          variables: {
            username: payload.username,
            email: payload.email,
            password: payload.password,
            passwordRepeat: payload.passwordRepeat
          }
        })
        .then(({ data }) => data.registerAccount)
      return res
    } catch (e) {
      console.error(e)
    }
  },
  async activate({ commit }, payload) {
    try {
      const res = await this.app.apolloProvider.clients.noAuth
        .mutate({
          mutation: activateAccountGql,
          variables: {
            token: payload.token,
            uid: payload.uid
          }
        })
        .then(({ data }) => data.activateAccount)
      return res
    } catch (e) {
      console.error(e)
    }
  },
  async syncUser({ commit }) {
    try {
      const resViewer = await this.app.apolloProvider.defaultClient
        .query({
          query: viewerMeGql,
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => ({
          user: data.viewer.user,
          profile: data.viewer.user.profile
        }))
      commit('profile/set', resViewer.profile, { root: true })
      commit('user/set', resViewer.user, { root: true })
    } catch (e) {
      console.log(e)
    }
  },
  async deleteAccount({ commit }, password) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: deleteAccountGql,
          variables: {
            password
          }
        })
        .then(({ data }) => data)
      if (res.deleteAccount.success) {
        commit('logout')
        this.app.$apolloHelpers.onLogout()
        this.$router.push(this.app.localePath('home'))
      }
      return res
    } catch (e) {
      console.error(e)
    }
  },
  reset({ commit }, email) {
    try {
      this.app.apolloProvider.defaultClient
        .mutate({
          mutation: resetPasswordGql,
          variables: {
            email
          }
        })
        .then(({ data }) => data)
    } catch (e) {
      console.error(e)
    }
  },
  async resetConfirm({ commit }, input) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: resetPasswordConfirmGql,
          variables: { ...input }
        })
        .then(({ data }) => data)
      return res
    } catch (e) {
      console.error(e)
    }
  }
}
