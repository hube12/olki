import editProfileGql from '~/apollo/mutations/editProfile'
import changePasswordGql from '~/apollo/mutations/changePassword'

export const state = () => ({
  data: {}
})

export const mutations = {
  set(state, data) {
    state.data = data
  }
}

export const getters = {
  getAvatar(state) {
    return state.data && state.data.avatar
  },
  get: (state, getters) => {
    return state.data
  }
}

export const actions = {
  async setRemote({ commit }, input) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: editProfileGql,
          variables: input
        })
        .then(({ data }) => data.profile)
      commit('profile/set', res, { root: true })
    } catch (e) {
      console.log(e)
    }
  },
  async changePassword({ commit }, input) {
    try {
      const res = await this.app.apolloProvider.defaultClient
        .mutate({
          mutation: changePasswordGql,
          variables: input
        })
        .then(({ data }) => data.changePassword)
      return res
    } catch (e) {
      console.error(e)
    }
  }
}
