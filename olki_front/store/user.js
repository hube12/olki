/**
 * User is a module to store information concerning the current logged user.
 */

export const state = () => ({
  data: {}
})

export const mutations = {
  set(state, data) {
    state.data = data
  }
}

export const getters = {
  get: (state, getters) => {
    return state.data
  },
  isAdministrator: (state, getters) => {
    return getters.get ? getters.get.isSuperuser : false
  },
  isModerator: (state, getters) => {
    return getters.get ? getters.get.isStaff : false
  },
  isSetup: state => {
    return state.data ? state.data.isSetup : true // true is important to not redirect in middleware unless explicitly not setup
  },
  displayName: state => {
    if (state.data && state.data.email) {
      return state.data.actor.name !== ''
        ? state.data.actor.name
        : state.data.actor.username
        ? state.data.actor.username
        : state.data.email.substring(0, 2).toUpperCase()
    } else {
      return ''
    }
  },
  getBio: (state, getters) => {
    return getters.get && getters.get.profile ? getters.get.profile.bio : ''
  },
  getActorOrId: (state, getters) => {
    return getters.get
      ? getters.get.actor
        ? getters.get.actor.username
        : ''
      : ''
  }
}
