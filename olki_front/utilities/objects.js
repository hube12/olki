// pick keys from object
export function pick(object, keys) {
  return keys.reduce((obj, key) => {
    if (object[key]) {
      obj[key] = object[key]
    }
    return obj
  }, {})
}
