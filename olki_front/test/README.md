# Testing

Tests are split in two main categories: unit and end-to-end (E2E) tests.
Each have their own directories and test suites.

`specs`, `specs-render`, and `snapshots` are devoted to unit tests.
`browser` and `browser-reports` are devoted to e2e tests.
`helpers` are common to both.

Note: `render` tests are a variant of the unit tests that makes Vue/Nuxt
render an ensemble of components, usually to check functional correctness
with the browser data store (Vuex) and locale (vue-i18n).
