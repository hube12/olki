Feature('Corpus creation form')

Before(login => {
  login('admin')
})

Scenario('Corpus form displays correctly', I => {
  I.amOnPage('/account/corpora/create')
  I.waitForElement('#createCorpusForm')
  I.see('Drop files here, paste or ')
})

Scenario('Corpus form validates fields correctly', I => {
  I.amOnPage('/account/corpora/create')
  I.waitForElement('#createCorpusForm')
  I.dontSeeElement({ css: '.has-error' })
  I.click('#publishMenu')
  I.waitForElement('#publishPublic')
  I.click('#publishPublic')
  I.seeElement({ css: '.has-error' })
})
