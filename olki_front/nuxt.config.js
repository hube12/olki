const path = require('path')
const webpack = require('webpack')
const pkg = require('./package')

// inject variables in process.env (later used by 'env' to expose some of them to the client)
const env = process.env.NODE_ENV === 'production' ? '.env' : '.env.test'
require('dotenv').config({ path: path.join(__dirname, '../.env.default') })
require('dotenv').config({ path: path.join(__dirname, `../${env}`) })

module.exports = {
  mode: 'spa',
  generate: {
    fallback: true
  },

  /**
   * Make environment variables in the client at 'process.env.VARNAME' via
   * Webpack, which will then find and replace explicit mentions of 'process.env.VARNAME' (full name, beware).
   * Useful to pass environment variables that are usally only available during the build (in process.env).
   */
  env: {
    baseURL:
      process.env.NODE_ENV === 'production' ? '' : 'http://127.0.0.1:5000'
  },

  /*
   ** Headers of the page
   */
  meta: {
    author: false,
    ogTitle: false
  },
  head: {
    title: 'OLKi',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: 'rgba(var(--color-primary), 1)' },
  loadingIndicator: {
    name: 'cube-grid',
    color: 'rgba(var(--color-primary), 1)'
  },

  /*
   ** Global CSS
   */
  css: [
    { src: 'vue-material/dist/vue-material.min.css', lang: 'css' },
    { src: '~plugins/themes/olki-default.scss', lang: 'scss' }
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~plugins/persistedstate', ssr: false },
    { src: '~plugins/init', ssr: false },
    { src: '~plugins/themes/olki-default.js' },
    { src: '~plugins/vue-material', ssr: false },
    { src: '~plugins/vue2-filters', ssr: false },
    { src: '~plugins/vee-validate', ssr: false },
    { src: '~plugins/vue-observe-visibility', ssr: false },
    { src: '~plugins/vue-tippy', ssr: false },
    { src: '~plugins/vue-filter-pretty-bytes', ssr: false },
    { src: '~plugins/vue-shortkey', ssr: false },
    { src: '~plugins/v-clipboard', ssr: false },
    { src: '~plugins/vue-introjs', ssr: false }
  ],

  /*
   ** Router
   */
  router: {
    prefetchLinks: false,
    extendRoutes(routes, resolve) {
      // eslint-disable-next-line no-unused-expressions
      routes.push({
        name: 'home',
        path: '/',
        component: resolve(__dirname, 'pages/index.vue')
      }),
        routes.push({
          path: '/*',
          component: resolve(__dirname, 'layouts/error.vue')
        })
    }
  },

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios', // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/pwa',
    '@nuxtjs/apollo',
    'nuxt-compress',
    [
      'nuxt-i18n',
      {
        parsePages: false, // Disable acorn parsing and keep your sanity
        defaultLocale: 'en',
        noPrefixDefaultLocale: true,
        lazy: true,
        langDir: 'lang/',
        locales: [
          {
            code: 'en',
            name: 'English',
            iso: 'en-US',
            file: 'en-US.js'
          },
          {
            code: 'es',
            name: 'Español',
            iso: 'es-ES',
            file: 'es-ES.js'
          },
          {
            code: 'fr',
            name: 'Français',
            iso: 'fr-FR',
            file: 'fr-FR.js'
          },
          {
            code: 'de',
            name: 'Deutsch',
            iso: 'de-DE',
            file: 'de-DE.js'
          }
        ],
        detectBrowserLanguage: {
          useCookie: true,
          alwaysRedirect: true,
          fallbackLocale: 'en',
          cookieKey: 'lang'
        },
        vueI18n: {
          fallbackLocale: 'en',
          dateTimeFormats: {
            en: {
              short: {
                year: 'numeric',
                month: 'short',
                day: 'numeric'
              },
              long: {
                year: 'numeric',
                month: 'short',
                day: 'numeric',
                weekday: 'short',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true
              }
            },
            fr: {
              short: {
                year: 'numeric',
                month: 'short',
                day: 'numeric'
              },
              long: {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                weekday: 'short',
                hour: 'numeric',
                minute: 'numeric'
              }
            }
          }
        }
      }
    ],
    [
      'nuxt-polyfill',
      {
        features: [
          {
            require: 'focus-visible' // detecting :focus-visible selector support is not easy, so we include it for everyone
          },
          {
            require: 'intersection-observer',
            detect: () => 'IntersectionObserver' in window // the polyfill will not be loaded, parsed and executed if it's not necessary
          }
        ]
      }
    ],
    [
      'nuxt-mq',
      {
        // Breakpoints are the same than for the CSS (Spectre.css).
        // They allow reactivity in Vue that CSS doesn't.
        breakpoints: {
          xs: 480,
          sm: 600,
          md: 840,
          lg: 1050,
          xl: 1750,
          inf: Infinity
        }
      }
    ],
    /* components */
    'nuxt-vue-multiselect',
    [
      '@nuxtjs/toast',
      {
        position: 'bottom-center',
        duration: 3000
      }
    ]
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    browserBaseURL:
      process.env.NODE_ENV === 'production' ? '/' : 'http://127.0.0.1:5000' // https://axios.nuxtjs.org/options#browserbaseurl
  },
  /*
   **This allow you to change the port and the host so you can rebind to an unsused one
   */
  server: {
    port: 3000, // default: 3000
    host: 'localhost' // default: localhost
  },
  /*
   ** Compression options to gzip/brotli assets
   *
   * This makes the 'generate' command pre-compile gzip and brotli versions of the frontend,
   * so that the server can disable compressing and just do redirections. No more heavy-lifting
   * for the server!
   */
  'nuxt-compress': {
    gzip: {
      cache: true
    },
    brotli: {
      threshold: 10240
    }
  },

  // Give apollo module options
  apollo: {
    tokenName: 'tohka',
    authenticationType: 'JWT',
    clientConfigs: {
      default: '~/apollo/defaultClient.js',
      noAuth: '~/apollo/noAuthClient.js'
    }
  },

  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      }
    }
  },

  /*
   ** Build configuration
   */
  build: {
    transpile: [/^zutre/],

    /*
     ** PostCSS config
     */
    postcss: {
      plugins: {
        'postcss-preset-env': {
          browserlist: [
            '> .5%',
            'last 2 versions',
            'Firefox ESR',
            'not ie > 0',
            'not ie_mob > 0',
            'not op_mini all',
            'not dead',
            'not < .1%'
          ]
        },
        'postcss-nested': {},
        'postcss-responsive-type': {}
      }
    },

    /*
     ** Webpack plugins
     */
    plugins: [
      new webpack.ProvidePlugin({
        // other modules
        introJs: ['intro.js']
      })
    ],

    /*
     ** Webpack config extension
     */
    extend(config, ctx) {
      config.resolve.alias.simplemde = 'easymde'

      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
