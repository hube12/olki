export default function(context) {
  return {
    httpEndpoint: context.env.baseURL + '/graphql',
    getAuth: () => undefined
  }
}
