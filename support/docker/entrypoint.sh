#!/bin/bash
set -eu

if [ "$1" == "manage"  ]; then
  shift
  exec python olki_back/manage.py "$@"
fi

# allow the container to be started with `--user`
# /!\ commented-out until we find a way to use shared
# volumes with non-root users
#
#if [ "$1" = 'dumb-init' -a "$(id -u)" = '0'  ]; then
#  exec gosu olki "$0" "$@"
#fi

exec "$@"
