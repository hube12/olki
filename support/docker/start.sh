#!/bin/bash -eux
poetry run python olki_back/manage.py migrate
poetry run python olki_back/manage.py collectstatic --noinput
poetry run python olki_back/manage.py collecttemplates
poetry run honcho start
