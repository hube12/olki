###
## First stage: assembling the dependencies (used in CI and later final image)
## [meant as target]
#
FROM python:3.7-slim-buster AS dependencies
LABEL org.opencontainers.authors Rigel Kent <sendmemail@rigelk.eu>
LABEL org.opencontainers.description OLKi platform runtime for a federated open science
LABEL org.opencontainers.documentation https://framagit.org/synalp/olki/olki/wikis/Admin-documentation
LABEL org.opencontainers.title rigelk/olki
LABEL org.opencontainers.url https://olki.loria.fr/platform

ARG POETRY_VERSION=1.0.0a4
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    BUILD_DEPS="build-essential gcc git"

WORKDIR /app
RUN set -eux; \
  apt-get update; \
  apt-get install -yqq apt-transport-https libsasl2-dev libldap2-dev; \
  apt-get install -yqq --no-install-recommends ${BUILD_DEPS} curl dumb-init gosu; \
  rm -rf /var/lib/apt/lists/*; \
  pip install --no-cache-dir --pre -I poetry==${POETRY_VERSION}; \
  python3 -m venv .venv; \
  mkdir -p /root/.config/pypoetry; \
  poetry config settings.virtualenvs.create false
ENV PATH=/app/.venv/bin:$PATH
COPY pyproject.toml poetry.lock ./
RUN poetry install --no-root
# /.venv is now populated


###
## Second stage: building the frontend (for its artifacts)
## [not meant as target]
#
FROM node:11 as frontend

# Allow to pass extra options to the npm run build
ARG NPM_RUN_BUILD_OPTS
ADD olki_front olki_front
WORKDIR olki_front
RUN set -eux; \
  npm i; \
  npm run build -- ${NPM_RUN_BUILD_OPTS:-}


###
## Final stage: assembling dependencies and frontend artifacts
## [meant as default target]
#
FROM dependencies

ARG DATE
ARG REVISION
ARG SOURCE

LABEL org.opencontainers.created=$DATE
LABEL org.opencontainers.revision=$REVISION
LABEL org.opencontainers.source=$SOURCE

# Add olki user, use gosu to step-down from root
RUN set -eux; \
  gosu nobody true; \
  groupadd -r olki; \
  useradd -r -g olki -m olki

# Install the application
WORKDIR /app
COPY --from=1 olki_front/dist olki_front/dist
COPY . .
ENV PATH=/app/.venv/bin:$PATH
RUN set -eux; \
  mv support/docker/.env .; \
  poetry install --no-dev; \
  chown -R olki:olki /app

# Strip down build dependencies
RUN set -eux; \
  apt-get purge -yqq --auto-remove ${BUILD_DEPS}

VOLUME /app/olki_back/olki/media

# Run the application
ENV DOTFILE .env
HEALTHCHECK --interval=10s --timeout=3s CMD curl --fail http://localhost:5000/ping || exit 1
ENTRYPOINT ["/app/support/docker/entrypoint.sh"]
CMD ["dumb-init", "/app/support/docker/start.sh"]
EXPOSE 5000
USER root
