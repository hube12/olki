__version__ = '0.2.0-alpha.0'

import os
import environ


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.path.dirname(BASE_DIR)


def env():
    DOTFILE = os.environ.get('DOTFILE', '.env.test')
    environ.Env.read_env(os.path.join(PROJECT_DIR, DOTFILE))
    environ.Env.read_env(os.path.join(PROJECT_DIR, '.env.default'))
