import logging.config
import os
import sys
from datetime import timedelta
from urllib.parse import urlsplit

import ldap
from django.conf import settings
from django.utils.log import DEFAULT_LOGGING
from django_auth_ldap.config import LDAPSearch
from environ import Env

from olki import env
from .environment import PROJECT_DIR, BASE_DIR

env()  # IMPORTANT: bootstraps all environment variables

Env.DB_SCHEMES['prompgsql'] = 'django_prometheus.db.backends.postgresql'
env = Env(
    # set casting, default value
    DEBUG=(bool, False)
)

DEBUG = env.bool('DEBUG') if os.getenv('DEBUG') else (len(sys.argv) > 1 and sys.argv[1] == 'runserver')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/
SECRET_KEY = env.str('SECRET', os.getenv('SECRET'))

# Application definition

DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.postgres',
]

THIRD_PARTY_APPS = [
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    'django_extensions',
    'dynamic_preferences',
    'dynamic_preferences.users.apps.UserPreferencesConfig',
    'rest_framework',
    'rest_framework_xml',
    'djoser',
    'corsheaders',
    'graphene_django',
    'django_filters',
    'django_dramatiq',
    'graphql_jwt',
    'django_cleanup',
    'easy_thumbnails',
    'rolepermissions',
    'rest_framework_tus',
    'actstream',
    'django_prometheus',
    'analytical'
]

if settings.DEBUG:
    THIRD_PARTY_APPS = [
        'debug_toolbar',
        'graphiql_debug_toolbar',
    ] + THIRD_PARTY_APPS

LOCAL_APPS = [
    'olki.apps.account',
    'olki.apps.core',
    'olki.apps.profile',
    'olki.apps.corpus',
    'olki.apps.note',
    'olki.apps.federation',
    'olki.apps.moderation',
    'olki.apps.oaipmh',
]

# putting local apps first allows to override django commands
INSTALLED_APPS = LOCAL_APPS + DJANGO_APPS + THIRD_PARTY_APPS

# LOCALE CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
TIME_ZONE = "UTC"

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = "en-us"

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# General settings
# ------------------------------------------------------------------------------
# Exposed host
try:
    INSTANCE_HOSTNAME = env("INSTANCE_HOSTNAME")
    INSTANCE_PROTOCOL = env("INSTANCE_PROTOCOL", default="https")
except Exception:
    INSTANCE_URL = env("INSTANCE_URL", default="http://localhost:5000")
    _parsed = urlsplit(INSTANCE_URL)
    INSTANCE_HOSTNAME = _parsed.netloc
    INSTANCE_PROTOCOL = _parsed.scheme

INSTANCE_PROTOCOL = INSTANCE_PROTOCOL.lower()
INSTANCE_HOSTNAME = INSTANCE_HOSTNAME.lower()
INSTANCE_URL = "{}://{}".format(INSTANCE_PROTOCOL, INSTANCE_HOSTNAME)

DOMAIN = INSTANCE_URL  # for backward-compatibility

# General settings (dynamic - can be changed in the web app - these are initial
# values only)
# ------------------------------------------------------------------------------
SITE_NAME = env('INSTANCE_NAME', default='olki')
INSTANCE_NAME = env('INSTANCE_NAME', default='')
INSTANCE_DESCRIPTION = env('INSTANCE_DESCRIPTION', default='')
INSTANCE_RULES = ''
INSTANCE_TERMS = ''
# Allow registration using the instance's own form on the about page.
# Users can still create accounts via the LDAP.
INSTANCE_REGISTRATIONS_OPEN = env.bool('INSTANCE_REGISTRATIONS_OPEN', True)
# Statistics are those made public (like active users/number of users)
# but no statistics are directly sent to a telemetry server.
INSTANCE_REDUCED_STATISTICS = env.bool('INSTANCE_REDUCED_STATISTICS', False)

# FEDERATION
#
FEDERATION_ENABLED = env('FEDERATION_ENABLED', default=True)
FEDERATION_COLLECTION_PAGE_SIZE = 15
FEDERATION_ACTOR_FETCH_DELAY = env.int("FEDERATION_ACTOR_FETCH_DELAY", default=60 * 12)  # in minutes

# Various settings (not sorted in a specific scope)
# ------------------------------------------------------------------------------
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

EMAIL_TIMEOUT = 5
DEFAULT_FROM_EMAIL = env("EMAIL_SENDER", default="OLKi <noreply@{}>".format(DOMAIN))
EMAIL_SUBJECT_PREFIX = env("EMAIL_SUBJECT_PREFIX", default="[OLKi] ")
SERVER_EMAIL = env("SERVER_EMAIL", default=DEFAULT_FROM_EMAIL)
EMAIL_CONFIG = env.email_url('EMAIL_URL', default='consolemail://')
vars().update(EMAIL_CONFIG)

# SHELL
# -------------------------------------------------------------------------------
SHELL_PLUS = 'ipython'
SHELL_PLUS_PRINT_SQL = True

# Truncate sql queries to this number of characters
SHELL_PLUS_PRINT_SQL_TRUNCATE = 1000

# Specify sqlparse configuration options when printing sql queries to the console
SHELL_PLUS_SQLPARSE_FORMAT_KWARGS = dict(
  reindent_aligned=True,
  truncate_strings=500,
)

# Specify Pygments formatter and configuration options when printing sql queries to the console
if DEBUG:
    import pygments.formatters
    SHELL_PLUS_PYGMENTS_FORMATTER = pygments.formatters.TerminalFormatter
    SHELL_PLUS_PYGMENTS_FORMATTER_KWARGS = {}

IPYTHON_ARGUMENTS = [
    '--ext', 'autoreload',
]

ROOT_URLCONF = 'olki.urls'

# WSGI
# -------------------------------------------------------------------------------
WSGI_APPLICATION = 'olki.wsgi.application'

# CHANNELS
# -------------------------------------------------------------------------------
CHANNELS_WS_PROTOCOLS = ["graphql-ws", ]
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels.layers.InMemoryChannelLayer",
    },
}

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': env.db('DATABASE_URL', default='prompgsql://olki:olki@localhost:5432/olki')
}
DATABASES['default']['PASSWORD'] = 'olki'

# CACHE CONFIGURATION
CACHES = {
    'default': env.cache('CACHE_URL', default='locmemcache://')
}

# PREFERENCES CACHE
#
# Can be enabled if you're NOT using an in-process cache backend, but a
# cross-process one like redis, db or memcached.
DYNAMIC_PREFERENCES = {
    'ENABLE_CACHE': False
}

# STATIC CONFIGURATION
# -------------------------------------------------------------------------------

# Simplifies the reverse-proxy configuration by serving the index.html
# automagically on non-API routes. See the middleware, this is just
# the storage backend :)
STATICFILES_STORAGE = 'spa.storage.SPAStaticFilesStorage'

# Defines directories to copy from
STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, '..', 'olki_front', 'dist'),
)

# Defines directories to store to
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_HOST = os.getenv('DJANGO_STATIC_HOST', '')
STATIC_URL = STATIC_HOST + '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_HOST = os.getenv('DJANGO_MEDIA_HOST', '')
MEDIA_URL = MEDIA_HOST + '/media/'

# This should be handled by the reverse-proxy
DATA_UPLOAD_MAX_MEMORY_SIZE = None

# FIELDS
# -------------------------------------------------------------------------------

# Image fields
"""
Note that avatar/profile picture image sizes vary. We are using Twitter
standards, which are generous enough
"""
THUMBNAIL_ALIASES = {
    '': {
        'avatar': {'size': (400, 400), 'crop': 'smart', 'upscale': True},
        'cover': {'size': (1500, 500), 'crop': 'scale', 'upscale': True}
    },
}


# DRAMATIQ WORKER CONFIGURATION
# ------------------------------------------------------------------------------
DRAMATIQ_BROKER = {
    "BROKER": "dramatiq.brokers.redis.RedisBroker",
    "OPTIONS": {
        "url": env.cache_url('CACHE_URL', default='redis://localhost:6379')['LOCATION'],
    },
    "MIDDLEWARE": [
        "dramatiq.middleware.Prometheus",
        "dramatiq.middleware.AgeLimit",
        "dramatiq.middleware.TimeLimit",
        "dramatiq.middleware.Callbacks",
        "dramatiq.middleware.Retries",
        "django_dramatiq.middleware.AdminMiddleware",
        "django_dramatiq.middleware.DbConnectionsMiddleware",
    ]
}

# Defines which database should be used to persist Task objects when the
# AdminMiddleware is enabled.  The default value is "default".
DRAMATIQ_TASKS_DATABASE = "default"

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        # 'rest_framework.authentication.TokenAuthentication',  # not working in concert with graphql_jwt through which users are identifying
        'olki.apps.core.graphql.backend.GraphqlJWTAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',  # all views using DRF will have these permissions classes applied
    ),
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle'
    ),
    'DEFAULT_THROTTLE_RATES': {
        'anon': '30/minute',
        'user': '100/minute'
    }
}

# LOGGING
# -------------------------------------------------------------------------------
# Disable Django's logging setup
LOGGING_CONFIG = None

LOGLEVEL = os.environ.get('LOGLEVEL', 'debug').upper()

if env.bool('SENTRY_ENABLE'):
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration
    sentry_sdk.init(
        dsn=env.str('SENTRY_DSN'),
        integrations=[DjangoIntegration()]
    )

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            # exact format is not important, this is the minimum information
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        },
        'django.server': DEFAULT_LOGGING['formatters']['django.server'],
    },
    'handlers': {
        # console logs to stderr
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'django.server': DEFAULT_LOGGING['handlers']['django.server'],
    },
    'loggers': {
        # default for all undefined Python modules
        '': {
            'level': 'INFO',  # extremely verbose when in 'debug' mode, beware
            'handlers': ['console'],
        },
        # Our application code
        'apps': {
            'level': LOGLEVEL,
            'handlers': ['console'],
            # Avoid double logging because of root logger
            'propagate': False,
        },
        'django_auth_ldap': {
            'level': 'ERROR',
            'handlers': ['console'],
        },
        'werkzeug': {
            'level': LOGLEVEL,
            'handlers': ['console'],
            'propagate': True,
        },
        'gunicorn': {
            'level': LOGLEVEL,
            'handlers': ['console'],
            'propagate': True,
        },
        # Default runserver request logging
        'django.server': DEFAULT_LOGGING['loggers']['django.server'],
    },
})

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
# SECURITY WARNING: don't run with debug turned on in production!

GRAPHENE = {
    'SCHEMA': 'olki.schema.schema',
    'SCHEMA_INDENT': 2,
    'MIDDLEWARE': [
        # 'olki.apps.core.graphql.middleware.DebugMiddleware',
        'graphql_jwt.middleware.JSONWebTokenMiddleware',
        'olki.apps.core.graphql.limit_complexity.DepthAnalysisMiddleware',
        'olki.apps.core.graphql.limit_complexity.CostAnalysisMiddleware',
    ],
    # Limit the complexity of queries, beginning with depth
    'QUERY_DEPTH_LIMIT': 8,
    # Limit the cost of queries, by summing the estimated cost of each field
    'QUERY_COST_LIMIT': 2000,
    # Set to True if the connection fields must have
    # either the first or last argument
    'RELAY_CONNECTION_MAX_LIMIT': 100,
    # Max items returned in ConnectionFields / FilterConnectionFields
    'RELAY_CONNECTION_ENFORCE_FIRST_OR_LAST': True
}

if DEBUG:
    GRAPHENE.get('MIDDLEWARE').append('graphene_django.debug.DjangoDebugMiddleware')

GRAPHQL_JWT = {
    'JWT_VERIFY_EXPIRATION': False,  # default is False
    'JWT_VERIFY': True,  # default is True
    'JWT_EXPIRATION_DELTA': timedelta(days=1),
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=7),
    'JWT_COOKIE_SECURE': False,
    # 'JWT_SECRET_KEY': SECRET_KEY,  # default is SECRET_KEY (which is defined above, so make sure it is the same across workers)
    'JWT_ALGORITHM': 'HS256',  # default is HS256
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,
}

# https://django-activity-stream.readthedocs.io/en/latest/configuration.html#manager

ACTSTREAM_SETTINGS = {
    'MANAGER': 'olki.apps.action.manager.ActivityStreamManager',
    'FETCH_RELATIONS': True,
}

# ROLES / PERMISSIONS
# -------------------------------------------------------------------------------
ROLEPERMISSIONS_MODULE = 'olki.apps.account.roles'

# Tell Django about the custom `User` model we created. The string
# `account.User` tells Django we are referring to the `User` model in
# the `account` module. This module is registered above in a setting
# called `INSTALLED_APPS`.
AUTH_USER_MODEL = 'account.User'

DJOSER = {
    'PASSWORD_RESET_CONFIRM_URL': 'account/reset?uid={uid}&token={token}',
    'ACTIVATION_URL': 'account/activate?uid={uid}&token={token}',
    'SEND_ACTIVATION_EMAIL': True,
}

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 9,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Password hashers
# https://docs.djangoproject.com/en/2.1/topics/auth/passwords/#auth-password-storage
#
# Argon2 is the winner of the 2015 Password Hashing Competition, a community
# organized open competition to select a next generation hashing algorithm.
# It’s designed not to be easier to compute on custom hardware than it is to
# compute on an ordinary CPU.
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher'
]

CSRF_COOKIE_HTTPONLY = True

# DJANGO MIDDLEWARES
# -------------------------------------------------------------------------------
MIDDLEWARE = [
    'django_prometheus.middleware.PrometheusBeforeMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'rest_framework_tus.middleware.TusMiddleware',
    'corsheaders.middleware.CorsMiddleware',  # Note that this needs to be placed above CommonMiddleware
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'graphql_jwt.middleware.JSONWebTokenMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_prometheus.middleware.PrometheusAfterMiddleware',
    'olki.apps.core.middleware.HeaderMiddleware',
    'spa.middleware.SPAMiddleware',
]

if settings.DEBUG:
    MIDDLEWARE = [
        'graphiql_debug_toolbar.middleware.DebugToolbarMiddleware',
    ] + MIDDLEWARE

# ALLOWED_HOSTS
# A list of strings representing the host/domain names that this Django site can
# serve. This is a security measure to prevent HTTP Host header attacks, which
# are possible even under many seemingly-safe web server configurations.
#
# see https://docs.djangoproject.com/en/2.2/ref/settings/#allowed-hosts
# -------------------------------------------------------------------------------
DOMAIN = os.getenv('INSTANCE_HOST', 'localhost:5000')
ALLOWED_HOSTS = [
    DOMAIN,
    'localhost',
    '127.0.0.1',
    '::1',
    '.ngrok.io',
]

INTERNAL_IPS = [
    '127.0.0.1',
    'localhost',
]

# CORS
# -------------------------------------------------------------------------------
CORS_ORIGIN_WHITELIST = [
    DOMAIN,
    '127.0.0.1:3000',
    '127.0.0.1:8000',
    'localhost:3000',
    'localhost:8000',
]

from corsheaders.defaults import default_headers
CORS_ALLOW_HEADERS = default_headers + (
    'Tus-Resumable',
    'Upload-Length',
    'Upload-Metadata',
    'Location',
    'Upload-Offset',
    'Content-Type',
)
CORS_EXPOSE_HEADERS = [
    'Tus-Resumable',
    'Upload-Length',
    'Upload-Metadata',
    'Location',
    'Upload-Offset',
    'Content-Type',
]

CORS_ALLOW_CREDENTIALS = True

# LDAP
# -------------------------------------------------------------------------------
# Baseline configuration.
AUTH_LDAP_ENABLE = env.bool('AUTH_LDAP_ENABLE')
AUTH_LDAP_SERVER_URI = os.getenv('AUTH_LDAP_SERVER_URI', 'ldap://localhost:389')

# Credentials
AUTH_LDAP_BIND_DN = 'cn=admin,dc=planetexpress,dc=com'
AUTH_LDAP_BIND_PASSWORD = 'GoodNewsEveryone'

# Search
AUTH_LDAP_USER_SEARCH = LDAPSearch("ou=people,dc=planetexpress,dc=com",
    ldap.SCOPE_SUBTREE, "(mail=%(user)s)")

# Populate the Django user from the LDAP directory.
AUTH_LDAP_USER_ATTR_MAP = {
    'email': 'mail',
}

AUTHENTICATION_BACKENDS = [
    'graphql_jwt.backends.JSONWebTokenBackend',

    # default authentication method modified to use e-mails intead of usernames
    "olki.apps.account.backends.OlkiAuthBackend",
]
if AUTH_LDAP_ENABLE:
    AUTHENTICATION_BACKENDS = ["olki.apps.account.backends.OlkiLDAPBackend"] + AUTHENTICATION_BACKENDS

# CRYPTOGRAPHY
# -------------------------------------------------------------------------------
EXTERNAL_REQUESTS_VERIFY_SSL = env.bool("EXTERNAL_REQUESTS_VERIFY_SSL", default=True)
RSA_KEY_SIZE = 2048
# ACTOR_KEY_ROTATION_DELAY = env.int("ACTOR_KEY_ROTATION_DELAY", default=3600 * 48)
