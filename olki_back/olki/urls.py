"""olki URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from typing import List
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.conf.urls import url
from django.urls import include, path
from django.conf.urls.static import static
from graphql_jwt.decorators import jwt_cookie

from .apps.core.graphql.upload.django import FileUploadGraphQLView

V1_PATTERNS: List[str] = []

urlpatterns: List[str] = [
    # '/' is serving staticfiles via django-spa/whitenoise automagically. Run 'collectstatic' after building the
    # client via `npm run build` in the 'olki_front' directory to update the assets in 'olki_back/olki/public', which
    # is the production directory where files are served.
    #
    # Note this is just a convenience, and efficient serving of files should be done via your reverse-proxy.

    # API
    url(r'^graphql', csrf_exempt(FileUploadGraphQLView.as_view(graphiql=True)), name='graphql'),
    url(r'^api/v1/', include((V1_PATTERNS, 'v1'), namespace='v1')),
    url('', include(('olki.apps.federation.urls', 'federation'), namespace='federation')),
    url('', include(('olki.apps.oaipmh.urls', 'oaipmh'), namespace='oaipmh')),

    # Heavy file handling
    url('', include('rest_framework_tus.urls', namespace='rest_framework_tus')),

    # Model-specific urls
    url('^corpus/', include(('olki.apps.corpus.urls', 'corpus'), namespace='corpus')),
    url('', include(('olki.apps.account.urls', 'account'), namespace='account')),

    # ActivityPub, WebFinger
    url('', include(('olki.apps.core.urls', 'core'), namespace='core')),

    # Prometheus metrics
    url('', include('django_prometheus.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)\
  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
