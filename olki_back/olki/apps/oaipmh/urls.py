from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(r'^corpus/oai$', csrf_exempt(views.CorpusOAIProvider.as_view())),
    url(r'^corpus/oai/xsl$', TemplateView.as_view(template_name='oaipmh/xsl.xml', content_type='application/xml')),
]
