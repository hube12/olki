# file oaipmh/views.py
#
#   Copyright 2013 Emory University Libraries
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from datetime import datetime
from functools import reduce
import operator

from django.apps import apps
from django.conf import settings
from django.db.models import Q
from django.views.generic import TemplateView

from olki.apps.core.renderers import OLKiXMLRenderer
from olki.apps.corpus.serializers import DublinCoreRecordSerializer
from olki.apps.core.datestamp import datetime_to_datestamp, tolerant_datestamp_to_datetime


class OAIProvider(TemplateView):
    """
    Astract OAI-PMH renderer, behaving much like the Django Sitemap
    class.
    """
    content_type = 'application/xml'  # switch to 'text/xml' once the xml template produces properly indented (and thus readable) xml.
    request = None

    # GET parameters
    oai_verb = None
    param_identifier = None
    param_metadataPrefix = None
    param_from = None
    param_until = None
    param_set = None

    # TO IMPLEMENT IN A SUBCLASS ##
    # modeling on sitemaps: these methods should be implemented
    # when extending OAIProvider
    _model = None
    metadata_formats = [
        {
            'metadataPrefix': 'oai_dc',
            'metadataNamespace': 'http://www.openarchives.org/OAI/2.0/oai_dc/',
            'schema': 'http://www.openarchives.org/OAI/2.0/oai_dc.xsd',
         }
    ]  # i.e.: oai_dc, oai_dcterms, etc.

    @property
    def model(self):
        return apps.get_model(*self._model.split('.', 1))

    def items(self):
        """
        list/generator/queryset of items to be made available via oai
        NOTE: this will probably have other optional parameters,
        e.g. filter by set or date modified
        - could possibly include find by id for GetRecord here also...

        :returns queryset
        """
        return self.model.objects.all()

    def item_by_id(self, id):
        return self.items().get(id=id)

    def last_modified(self, obj):
        # datetime object was last modified
        pass

    def oai_identifier(self, obj):
        # oai identifier for a given object
        # can potentially be left as is, if the identifier format fits your needs
        return 'oai:%s:%s:%s' % (settings.DOMAIN,
                                 obj.__class__.__name__.lower(),
                                 obj.id)

    def sets(self, obj):
        # list of set identifiers for a given object
        return []

    # END TO IMPLEMENT ##

    def render_to_response(self, context, **response_kwargs):
        # all OAI responses should be xml
        if 'content_type' not in response_kwargs:
            response_kwargs['content_type'] = self.content_type

        # add common context data needed for all responses
        context.update({
            'datestamp': datetime_to_datestamp(datetime.now()),
            'verb': self.oai_verb,
            'url': self.request.build_absolute_uri(self.request.path),
        })
        return super(TemplateView, self) \
            .render_to_response(context, **response_kwargs)

    def identify(self, identify_data={}):
        self.template_name = 'oaipmh/identify.xml'
        data = {
            'name': settings.SITE_NAME,
            # perhaps an oai_admins method with default logic settings.admins?
            'admins': (email for name, email in settings.ADMINS),
            'earliest_date': '1990-02-01T12:00:00Z',   # placeholder
            # should probably be a class variable/configuration
            'deleted': 'no',  # no, transient, persistent (?) - indicates whether the repository keeps track of deleted records
            # class-level variable/configuration (may affect templates also)
            'granularity': 'YYYY-MM-DDThh:mm:ssZ',  # or YYYY-MM-DD
            # class-level config?
            'compression': 'deflate',  # gzip?  - optional
            # description - optional
            # (place-holder values from OAI docs example)
            'identifier_scheme': 'oai',
            'repository_identifier': settings.INSTANCE_HOSTNAME,
            'identifier_delimiter': ':',
            'sample_identifier': 'oai:{}:/corpus/7FvEKnf340ht12o6'.format(settings.INSTANCE_HOSTNAME),
            **identify_data
        }
        return self.render_to_response(data)

    def list_entity(self, entity='identifiers'):
        if entity == 'identifiers':
            self.template_name = 'oaipmh/list_identifiers.xml'
        else:
            self.template_name = 'oaipmh/list_records.xml'
        items = []

        # TODO: eventually we will need pagination with oai resumption tokens
        # should be able to model similar to django.contrib.sitemap
        q = []
        if self.param_from:
            q.append(Q(updated_at__gte=self.param_from))
        if self.param_until:
            q.append(Q(updated_at__lte=self.param_until))

        for i in self.items().filter(reduce(operator.and_, q, Q())):
            item_info = self.get_record_info(i)
            if entity == 'records':
                if not self.param_metadataPrefix:
                    error_msg = "The required argument 'metadataPrefix' is missing in the request."
                    return self.error('badArgument', error_msg)
                metadata = OLKiXMLRenderer().render(DublinCoreRecordSerializer(i, context={'metadata_enclosed': True}).data, without_root=True)
                item_info.update({'metadata': metadata})
            items.append(item_info)

        return self.render_to_response({'items': items})


    def list_metadata_formats(self):
        self.template_name = 'oaipmh/list_metadata_formats.xml'
        if not self.metadata_formats:
            error_msg = "There are no metadata formats available for the specified item/collection."
            return self.error('noMetadataFormats', error_msg)
        else:
            return self.render_to_response({'formats': self.metadata_formats})

    def get_record(self):
        self.template_name = 'oaipmh/get_record.xml'
        error_msg = "The value of the `identifier` argument is unknown or illegal in this repository."
        item = None

        if not self.param_identifier:
            error_msg = "The required argument 'identifier' is missing in the request."
            return self.error('badArgument', error_msg)
        if not self.param_metadataPrefix:
            error_msg = "The required argument 'metadataPrefix' is missing in the request."
            return self.error('badArgument', error_msg)
        try:
            obj = self.item_by_id(self.param_identifier.split(':')[-1])
            item = {
                **self.get_record_info(obj),
                **self.get_record_metadata(obj),
            }
        except Exception as e:
            return self.error('Error', error_msg)
        return self.render_to_response({'item': item})

    def get_record_info(self, obj):
        return {
            'identifier': self.oai_identifier(obj),
            'last_modified': self.last_modified(obj),
            'sets': self.sets(obj)
        }

    def get_record_metadata(self, obj):
        if self.param_metadataPrefix == 'oai_dc':
            metadata = OLKiXMLRenderer().render(DublinCoreRecordSerializer(obj, context={'metadata_enclosed': True}).data, without_root=True)
        else:
            metadata = {}
        return {
            'metadata': metadata
        }

    def validate_boundaries(self):
        if self.param_from:
            self.param_from = tolerant_datestamp_to_datetime(self.param_from)
            if self.param_from.isoformat() > self.items().first().updated_at.isoformat():
                return False
        if self.param_until:
            self.param_until = tolerant_datestamp_to_datetime(self.param_until)
            if self.param_until.isoformat() < self.items().last().updated_at.isoformat():
                return False
        # See http://www.openarchives.org/OAI/openarchivesprotocol.html#SelectiveHarvestingandDatestamps
        if self.param_from and self.param_until:
            if self.param_from.isoformat() > self.param_until.isoformat():
                return False
        return True

    def error(self, code, text, http_code=200):
        # NOTE: per http://www.openarchives.org/pipermail/oai-implementers/2002-June/000552.html HTTP error codes are
        # to be used only in case of server error.
        self.template_name = 'oaipmh/error.xml'
        return self.render_to_response({
            'error_code': code,
            'error': text,
        }, status=http_code)

    def handle(self, request, *args, **kwargs):
        self.request = request  # store for access in other functions

        r = getattr(request, request.method, None)
        self.oai_verb = r.get('verb', None)
        self.param_identifier = r.get('identifier', None)
        self.param_metadataPrefix = r.get('metadataPrefix', None)
        self.param_from = r.get('from', None)
        self.param_until = r.get('until', None)
        self.param_set = r.get('set', None)

        if not self.validate_boundaries():
            error_msg = 'Selective harvesting markers (until and/or from) are improperly defined or cross the repository boundaries.'
            return self.error('badArgument', error_msg)

        if self.oai_verb == 'Identify':
            return self.identify()
        elif self.oai_verb == 'ListIdentifiers':
            return self.list_entity(entity="identifiers")
        elif self.oai_verb == 'ListMetadataFormats':
            return self.list_metadata_formats()
        elif self.oai_verb == 'GetRecord':
            return self.get_record()
        elif self.oai_verb == 'ListRecords':
            return self.list_entity(entity="records")

        # OAI verbs still TODO:
        #
        elif self.oai_verb == 'ListSets':
            error_msg = 'This repository does not support sets.'
            return self.error('noSetHierarchy', error_msg)

        else:
            # if no verb = bad request response
            if self.oai_verb is None:
                error_msg = 'The request did not provide any verb.'
            else:
                error_msg = 'The verb "%s" is illegal' % self.oai_verb
            return self.error('badVerb', error_msg)

    def get(self, request, *args, **kwargs):
        """
        HTTP GET request: determine OAI verb and hand off to appropriate method
        """
        return self.handle(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        HTTP POST request: determine OAI verb and hand off to appropriate method
        """
        return self.handle(request, *args, **kwargs)


class CorpusOAIProvider(OAIProvider):
    """
    Implementation of the OAIProvider, listing corpora
    """
    _model = "corpus.Corpus"

    def items(self):
        return super().items().order_by('-created_at')

    def last_modified(self, obj):
        return datetime_to_datestamp(obj.updated_at)

    def identify(self, identify_data={}):
        return super().identify(identify_data={
            'earliest_date': datetime_to_datestamp(self.items().first().created_at),
            'granularity': 'YYYY-MM-DDThh:mm:ssZ',
        })

    def sets(self, obj):
        return [
            'type:DATASET',
            'collection:{}'.format(settings.INSTANCE_HOSTNAME),
        ]
