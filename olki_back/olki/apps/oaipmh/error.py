class ErrorBase(Exception):
    def oainame(self):
        name = self.__class__.__name__
        # strip off 'Error' part
        name = name[:-5]
        # lowercase error name
        name = name[0].lower() + name[1:]
        return name


class BadResumptionTokenError(ErrorBase):
    pass


class DatestampError(ErrorBase):
    pass
