from django.contrib.postgres.fields import JSONField
from django.db import models
from django.core.validators import URLValidator
from model_utils.models import StatusModel, MonitorField
from model_utils import Choices

from olki.apps.core.models import TimestampedModel


def empty_dict():
    return {}


class Repository(
    TimestampedModel,
    StatusModel
):
    STATUS = Choices('enabled', 'disabled', 'blocked')

    url = models.CharField(
        primary_key=True,
        max_length=255,
        validators=[URLValidator()],
    )
    oaipmh_identify_fetch_date = MonitorField(monitor="oaipmh_identify")
    oaipmh_identify = JSONField(default=empty_dict, max_length=50000, blank=True)

    @property
    def name(self):
        return self.url
