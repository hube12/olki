import graphene
from graphene.types import List, String, Boolean

from .tasks import clone_record_oaipmh


class ImportCorpus(graphene.Mutation):
    """
    Mutation to import a corpus from an OAI-PMH repository
    """
    class Arguments:
        oai_parser = String(required=True)
        oai_repository = String(required=True)
        oai_identifier = String(required=True)

    success = Boolean()
    errors = List(String)

    def mutate(self, info, oai_parser, oai_repository, oai_identifier):
        is_authenticated = info.context.user.is_authenticated
        if not is_authenticated:
            errors = ['unauthenticated']
        else:
            try:
                clone_record_oaipmh.send_with_options(args=(oai_parser, oai_repository, oai_identifier), kwargs={'user_id': info.context.user.pk})
                return ImportCorpus(success=True)
            except Exception as e:
                errors = ['import failed: {}'.format(e)]
        return ImportCorpus(success=False, errors=errors)


class OaipmhMutations(graphene.AbstractType):
    importCorpus = ImportCorpus.Field()
