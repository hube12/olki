from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.text import Truncator
from django.contrib.postgres.fields import JSONField
from flax_id.django.fields import FlaxId

from olki.apps.core.models import TimestampedModel


class Note(TimestampedModel):
    """
    A Note is used to describe messages:
    - replies
    - reviews
    - direct messages to a set of users
    etc.

    # Create a Note
    >>> note = Note.objects.create(content="test")
    >>> note.content
    'test'
    """
    id = FlaxId(primary_key=True, help_text=_('A Flax ID that is used as a primary key for the model.'))
    fid = models.URLField(unique=True, max_length=500, null=True, blank=True)
    inReplyTo = models.URLField(max_length=500, null=True, blank=True)

    # Relations
    #
    actor = models.ForeignKey(
        "federation.Actor",
        null=True,
        on_delete=models.SET_NULL  # upon user deletion, sets the user reference to null
    )
    corpus = models.ForeignKey(
        "corpus.Corpus",
        null=True,
        on_delete=models.CASCADE  # upon corpus deletion, deletes the Note
    )

    # Attributes - Mandatory
    #
    # Mostly maps on https://www.w3.org/ns/activitystreams#Note
    name = models.TextField(blank=True, null=True, help_text=_('Subject of the Note.'))
    content = models.TextField(help_text=_('Text of the Note.'))
    summary = models.TextField(blank=True, null=True, default='')
    tags = JSONField(blank=True, null=True, help_text=_('A list of tags with a WikiData key and their english name as a value.'))
    language = models.CharField(default='en', null=True, max_length=10, help_text=_('Language in which the Note was written.'))

    # Visibility
    public = models.BooleanField(default=True, help_text=_('Moderation flag, where a Note that is not public is the same as deleted.'))
    approved = models.BooleanField(default=False, help_text=_('Moderation flag, where a Note has to be approved to be shown on the Corpus it answers.'))

    def __str__(self):
        return Truncator("{} - {}".format(self.name, self.content) if self.name else self.content).words(10)

    def to_json(self):
        from olki.apps.federation.serializers import NoteSerializer
        return NoteSerializer(self).data
