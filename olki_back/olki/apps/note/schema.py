from graphene import relay, JSONString, String
from graphene_django.types import DjangoObjectType

from olki.apps.core.graphql.types import CountableConnectionBase
from olki.apps.core.graphql.utils.mixins import PKMixin
from .models import Note as NoteModel


class Note(PKMixin, DjangoObjectType):
    """
    Action Node, following basic [concepts](https://django-activity-stream.readthedocs.io/en/latest/concepts.html).
    Verbs are relative to the https://www.w3.org/ns/activitystreams# namespace, and otherwise absolute IRIs.
    """

    class Meta:
        model = NoteModel
        filter_fields = {
            'language': ['exact'],
        }
        only_fields = (
            'name',
            'content',
            'summary',
            'language',
        )
        description = "This is a Note as defined in the ActivityStreams Vocabulary."
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase

    fid = String()
    tags = JSONString()

    def resolve_fid(self, info):
        return "/note/{}".format(self.id)

    def resolve_tags(self, info):
        return self.tags

    @classmethod
    def get_queryset(cls, queryset, info):
        return queryset.filter(public=True)
