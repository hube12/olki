from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'olki.apps.note'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Note'))
