import graphene
from actstream import action
from django import forms
from graphene.types import String
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import ErrorType
from graphql_jwt.decorators import login_required

from olki.apps.corpus.models import Corpus as CorpusModel
from olki.apps.federation.routes import outbox
from .models import Note as NoteModel


class NoteForm(forms.ModelForm):
    class Meta:
        model = NoteModel
        fields = (
            'name',
            'content',
            'tags',
        )

    language = forms.CharField(required=False)


class CreateNote(DjangoModelFormMutation):
    class Input:
        pk = String(required=True, description='A Flax ID that is used as a primary key for the Corpus model the Note references.')
        # replyTo = graphene.String()

    class Meta:
        form_class = NoteForm
        exclude_fields = ('id', )

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        form = cls.get_form(root, info, **input)
        pk = input.get('pk')

        if form.is_valid():
            # get corpus
            corpus = CorpusModel.objects.get(id=pk)

            # create Note
            obj = NoteModel(
                **form.cleaned_data,
                actor=info.context.user.actor,
                corpus=corpus,
            )
            obj.save()

            # create Action holding the note
            action.send(info.context.user.actor, verb='Add', action_object=obj, target=corpus)
            # create Activity holding the note
            outbox.dispatch({"type": "Create", "object": {"type": "Note"}}, context={"note": obj})

            return cls(errors=[])
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(errors=errors)


class DeleteNote(graphene.Mutation):
    """
    Mutation to delete a Note. It doesn't delete the Note object, but
    rather switches its visibility to False
    """
    class Arguments:
        pk = graphene.String(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate(self, info, pk):
        obj = NoteModel.objects.get(id=pk)
        obj.public = False
        obj.save(update_fields=['public'])
        return DeleteNote(
            success=True,
            errors=None
        )


class NoteMutations(graphene.AbstractType):
    createNote = CreateNote.Field()
    deleteNote = DeleteNote.Field()
