# pylint: skip-file

import uuid

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from model_utils.managers import QueryManager

from olki.apps.core.fields import AddFlagOneToOneField
from olki.apps.core.models import FederationMixin
from olki.apps.core.validators import DomainValidator
from . import utils as federation_utils

TYPE_CHOICES = [
    ("Person", "Person"),
    ("Tombstone", "Tombstone"),
    ("Application", "Application"),
    ("Group", "Group"),
    ("Organization", "Organization"),
    ("Service", "Service"),
]


def empty_dict():
    return {}


def get_shared_inbox_url():
    return federation_utils.full_url(reverse("federation:shared-inbox"))


class ActorQuerySet(models.QuerySet):
    def local(self, include=True):
        return self.exclude(user__isnull=include)


class DomainQuerySet(models.QuerySet):
    def external(self):
        return self.exclude(pk=settings.INSTANCE_HOSTNAME)

    def with_actors_count(self):
        return self.annotate(actors_count=models.Count("actors", distinct=True))

    def with_outbox_activities_count(self):
        return self.annotate(
            outbox_activities_count=models.Count(
                "actors__outbox_activities", distinct=True
            )
        )


class Domain(models.Model):
    name = models.CharField(
        primary_key=True,
        max_length=255,
        validators=[DomainValidator()],
    )
    creation_date = models.DateTimeField(default=timezone.now)
    nodeinfo_fetch_date = models.DateTimeField(default=None, null=True, blank=True)
    nodeinfo = JSONField(default=empty_dict, max_length=50000, blank=True)
    service_actor = models.ForeignKey(
        "Actor",
        related_name="managed_domains",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    # are interactions with this domain allowed (only applies when allow-listing is on)
    allowed = models.BooleanField(default=None, null=True)

    objects = DomainQuerySet.as_manager()

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        lowercase_fields = ["name"]
        for field in lowercase_fields:
            v = getattr(self, field, None)
            if v:
                setattr(self, field, v.lower())

        super().save(**kwargs)

    @property
    def is_local(self):
        return self.name == settings.INSTANCE_HOSTNAME


class Actor(
    FederationMixin
):
    ap_type = "Actor"

    user = AddFlagOneToOneField(
        "account.User",
        related_name='actor',
        flag_name='has_actor',
        null=True,
        on_delete=models.CASCADE
    )
    outbox_url = models.URLField(max_length=500, null=True, blank=True)
    inbox_url = models.URLField(max_length=500)
    following_url = models.URLField(max_length=500, null=True, blank=True)
    followers_url = models.URLField(max_length=500, null=True, blank=True)
    shared_inbox_url = models.URLField(max_length=500, null=True, blank=True)
    type = models.CharField(choices=TYPE_CHOICES, default="Person", max_length=50)
    name = models.CharField(max_length=200, null=True, blank=True)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, related_name="actors")
    summary = models.CharField(max_length=500, null=True, blank=True)
    preferred_username = models.CharField(max_length=200, null=True, blank=True)
    public_key = models.TextField(max_length=5000, null=True, blank=True)
    private_key = models.TextField(max_length=5000, null=True, blank=True)
    creation_date = models.DateTimeField(default=timezone.now)
    last_fetch_date = models.DateTimeField(default=timezone.now)
    manually_approves_followers = models.NullBooleanField(default=None)
    followers = models.ManyToManyField(
        to="self",
        symmetrical=False,
        through="Follow",
        through_fields=("target", "actor"),
        related_name="following",
    )
    icon = models.URLField(max_length=500, null=True, blank=True)
    image = models.URLField(max_length=500, null=True, blank=True)

    objects = ActorQuerySet.as_manager()

    class Meta:
        unique_together = ["domain", "preferred_username"]

    @property
    def webfinger_subject(self):
        return "{}@{}".format(self.preferred_username, settings.INSTANCE_HOSTNAME)

    @property
    def private_key_id(self):
        return "{}#main-key".format(self.fid)

    @property
    def profile_url(self):
        if self.is_local:
            return "{}/@{}".format(settings.INSTANCE_URL, self.username)
        else:
            return self.url or self.fid

    @property
    def username(self):
        return self.preferred_username

    @property
    def full_username(self):
        return "{}@{}".format(self.preferred_username, self.domain_id)

    def __str__(self):
        return "{}@{}".format(self.preferred_username, self.domain_id)

    def get_approved_followers(self):
        follows = self.received_follows.filter(approved=True)
        return self.followers.filter(pk__in=follows.values_list("actor", flat=True))

    def should_autoapprove_follow(self, actor):
        return False

    def get_user(self):
        try:
            return self.user
        except ObjectDoesNotExist:
            return None

    @property
    def keys(self):
        return self.private_key, self.public_key

    @keys.setter
    def keys(self, v):
        self.private_key = v[0].decode("utf-8")
        self.public_key = v[1].decode("utf-8")

    def migrate_domain(self, domain=None):
        actor_data = self.get_actor_data(self.username)
        for attr, value in actor_data.items():
            setattr(self, attr, value)
        self.domain = domain or Domain.objects.get_or_create(
            name=settings.INSTANCE_HOSTNAME
        )[0]
        self.save()
        return self

    @staticmethod
    def get_actor_data(username):
        slugified_username = federation_utils.slugify_username(username)
        return {
            "preferred_username": slugified_username,
            "domain": Domain.objects.get_or_create(
                name=settings.INSTANCE_HOSTNAME
            )[0],
            "type": "Person",
            "name": username,
            "manually_approves_followers": False,
            "fid": federation_utils.full_url(
                reverse(
                    "federation:actor-detail",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
            "url": federation_utils.full_url(
                reverse(
                    "profile",
                    kwargs={"user_id": slugified_username},
                    urlconf="olki.apps.account.spa_urls",
                )
            ),
            "shared_inbox_url": get_shared_inbox_url(),
            "inbox_url": federation_utils.full_url(
                reverse(
                    "federation:actor-inbox",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
            "outbox_url": federation_utils.full_url(
                reverse(
                    "federation:actor-outbox",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
            "followers_url": federation_utils.full_url(
                reverse(
                    "federation:actor-followers",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
            "following_url": federation_utils.full_url(
                reverse(
                    "federation:actor-following",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
        }


FETCH_STATUSES = [
    ("pending", "Pending"),
    ("errored", "Errored"),
    ("finished", "Finished"),
    ("skipped", "Skipped"),
]


class FetchQuerySet(models.QuerySet):
    def get_for_object(self, object):
        content_type = ContentType.objects.get_for_model(object)
        return self.filter(object_content_type=content_type, object_id=object.pk)


class Fetch(models.Model):
    url = models.URLField(max_length=500, db_index=True)
    creation_date = models.DateTimeField(default=timezone.now)
    fetch_date = models.DateTimeField(null=True, blank=True)
    object_id = models.IntegerField(null=True)
    object_content_type = models.ForeignKey(
        ContentType, null=True, on_delete=models.CASCADE
    )
    object = GenericForeignKey("object_content_type", "object_id")
    status = models.CharField(default="pending", choices=FETCH_STATUSES, max_length=20)
    detail = JSONField(
        default=empty_dict, max_length=50000, encoder=DjangoJSONEncoder, blank=True
    )
    actor = models.ForeignKey(Actor, related_name="fetches", on_delete=models.CASCADE)

    objects = FetchQuerySet.as_manager()

    def save(self, **kwargs):
        if not self.url and self.object:
            self.url = self.object.fid

        super().save(**kwargs)

    @property
    def serializers(self):
        return {
            # contexts.AS.Note: serializers.NoteSerializer,
            # contexts.AS.Audio: serializers.UploadSerializer,
        }


class InboxItem(models.Model):
    """
    Store activities binding to local actors, with read/unread status.
    """

    actor = models.ForeignKey(
        Actor, related_name="inbox_items", on_delete=models.CASCADE
    )
    activity = models.ForeignKey(
        "Activity", related_name="inbox_items", on_delete=models.CASCADE
    )
    type = models.CharField(max_length=10, choices=[("to", "to"), ("cc", "cc")])
    is_read = models.BooleanField(default=False)


class Delivery(models.Model):
    """
    Store deliveries attempt to remote inboxes
    """

    is_delivered = models.BooleanField(default=False)
    last_attempt_date = models.DateTimeField(null=True, blank=True)
    attempts = models.PositiveIntegerField(default=0)
    inbox_url = models.URLField(max_length=500)

    activity = models.ForeignKey(
        "Activity", related_name="deliveries", on_delete=models.CASCADE
    )


class Activity(
    FederationMixin
):
    """
    Activities store references to the objects and targets. Their payload
    is what gets sent on the network and is the result of processing other
    attributes through the dispatched route that created the Activity.
    """
    actor = models.ForeignKey(
        Actor, related_name="outbox_activities", on_delete=models.CASCADE
    )
    recipients = models.ManyToManyField(
        Actor, related_name="inbox_activities", through=InboxItem
    )
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    payload = JSONField(default=empty_dict, max_length=50000, encoder=DjangoJSONEncoder)
    creation_date = models.DateTimeField(default=timezone.now, db_index=True)
    type = models.CharField(db_index=True, null=True, max_length=100)

    # generic relations
    object_id = models.CharField(null=True, max_length=16)
    object_content_type = models.ForeignKey(
        ContentType,
        null=True,
        on_delete=models.SET_NULL,
        related_name="objecting_activities",
    )
    object = GenericForeignKey("object_content_type", "object_id")
    target_id = models.CharField(null=True, max_length=16)
    target_content_type = models.ForeignKey(
        ContentType,
        null=True,
        on_delete=models.SET_NULL,
        related_name="targeting_activities",
    )
    target = GenericForeignKey("target_content_type", "target_id")
    related_object_id = models.CharField(null=True, max_length=16)
    related_object_content_type = models.ForeignKey(
        ContentType,
        null=True,
        on_delete=models.SET_NULL,
        related_name="related_objecting_activities",
    )
    related_object = GenericForeignKey(
        "related_object_content_type", "related_object_id"
    )

    objects = models.Manager()
    # Some notable shortcuts to common filtering cases:
    Corpus = QueryManager(Q(payload__object__type='Corpus') & Q(object_id__isnull=False))
    Note = QueryManager(Q(payload__object__type='Note') & Q(object_id__isnull=False))
    Local = QueryManager(Q(actor__domain=Domain.objects.get_or_create(name=settings.INSTANCE_HOSTNAME)[0]))


class AbstractFollow(models.Model):
    ap_type = "Follow"

    fid = models.URLField(unique=True, max_length=500, null=True, blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    creation_date = models.DateTimeField(default=timezone.now)
    modification_date = models.DateTimeField(auto_now=True)
    approved = models.NullBooleanField(default=None)

    class Meta:
        abstract = True

    def get_federation_id(self):
        return federation_utils.full_url(
            "{}#follows/{}".format(self.actor.fid, self.uuid)
        )


class Follow(AbstractFollow):
    actor = models.ForeignKey(
        Actor, related_name="emitted_follows", on_delete=models.CASCADE
    )
    target = models.ForeignKey(
        Actor, related_name="received_follows", on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ["actor", "target"]
