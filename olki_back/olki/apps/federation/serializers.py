# pylint: skip-file

"""py
    Author: Eliot Berriot
    Project of origin: Funkwhale
    Commit of origin: 4a412d36a993f97c69b57ecd4d75372769185caf
    Project licence: AGPLv3.0 or later
"""
import json
import logging
import mimetypes
import urllib.parse
from urllib.parse import urlencode

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.urls import reverse
from rest_framework import serializers

from olki.apps.core import utils as olki_utils
from olki.apps.note.models import Note
from . import activity, models, utils, jsonld, contexts

AP_CONTEXT = jsonld.get_default_context()

logger = logging.getLogger(__name__)


class LinkSerializer(jsonld.JsonLdSerializer):
    type = serializers.ChoiceField(choices=[contexts.AS.Link])
    href = serializers.URLField(max_length=500)
    mediaType = serializers.CharField()

    class Meta:
        jsonld_mapping = {
            "href": jsonld.first_id(contexts.AS.href),
            "mediaType": jsonld.first_val(contexts.AS.mediaType),
        }

    def __init__(self, *args, **kwargs):
        self.allowed_mimetypes = kwargs.pop("allowed_mimetypes", [])
        super().__init__(*args, **kwargs)

    def validate_mediaType(self, v):
        if not self.allowed_mimetypes:
            # no restrictions
            return v
        for mt in self.allowed_mimetypes:
            if mt.endswith("/*"):
                if v.startswith(mt.replace("*", "")):
                    return v
            else:
                if v == mt:
                    return v
        raise serializers.ValidationError(
            "Invalid mimetype {}. Allowed: {}".format(v, self.allowed_mimetypes)
        )


class EndpointsSerializer(jsonld.JsonLdSerializer):
    sharedInbox = serializers.URLField(max_length=500, required=False)

    class Meta:
        jsonld_mapping = {"sharedInbox": jsonld.first_id(contexts.AS.sharedInbox)}


class PublicKeySerializer(jsonld.JsonLdSerializer):
    publicKeyPem = serializers.CharField(trim_whitespace=False)

    class Meta:
        jsonld_mapping = {"publicKeyPem": jsonld.first_val(contexts.SEC.publicKeyPem)}


class ActorSerializer(jsonld.JsonLdSerializer):
    id = serializers.URLField(max_length=500)
    outbox = serializers.URLField(max_length=500, required=False)  # the only case when it is not required is for instance actors
    inbox = serializers.URLField(max_length=500)
    type = serializers.ChoiceField(
        choices=[getattr(contexts.AS, c[0]) for c in models.TYPE_CHOICES]
    )
    preferredUsername = serializers.CharField()
    manuallyApprovesFollowers = serializers.NullBooleanField(required=False)
    name = serializers.CharField(required=False, max_length=200)
    summary = serializers.CharField(max_length=None, required=False)
    followers = serializers.URLField(max_length=500, required=False, allow_null=True)  # the only case when it is not required is for instance actors
    following = serializers.URLField(max_length=500, required=False, allow_null=True)
    publicKey = PublicKeySerializer(required=False)
    endpoints = EndpointsSerializer(required=False)

    class Meta:
        jsonld_mapping = {
            "outbox": jsonld.first_id(contexts.AS.outbox),
            "inbox": jsonld.first_id(contexts.LDP.inbox),
            "following": jsonld.first_id(contexts.AS.following),
            "followers": jsonld.first_id(contexts.AS.followers),
            "preferredUsername": jsonld.first_val(contexts.AS.preferredUsername),
            "summary": jsonld.first_val(contexts.AS.summary),
            "name": jsonld.first_val(contexts.AS.name),
            "publicKey": jsonld.first_obj(contexts.SEC.publicKey),
            "manuallyApprovesFollowers": jsonld.first_val(
                contexts.AS.manuallyApprovesFollowers
            ),
            "mediaType": jsonld.first_val(contexts.AS.mediaType),
            "endpoints": jsonld.first_obj(contexts.AS.endpoints),
        }

    def to_representation(self, instance):
        ret = {
            "id": instance.fid,
            "url": instance.url,
            "outbox": instance.outbox_url,
            "inbox": instance.inbox_url,
            "preferredUsername": instance.preferred_username,
            "type": "Person",
        }
        if instance.name:
            ret["name"] = instance.name
        if instance.followers_url:
            ret["followers"] = instance.followers_url
        if instance.following_url:
            ret["following"] = instance.following_url
        if instance.summary:
            ret["summary"] = instance.summary
        if instance.manually_approves_followers is not None:
            ret["manuallyApprovesFollowers"] = instance.manually_approves_followers

        ret["@context"] = AP_CONTEXT
        if instance.public_key:
            ret["publicKey"] = {
                "owner": instance.fid,
                "publicKeyPem": instance.public_key,
                "id": "{}#main-key".format(instance.fid),
            }
        ret["endpoints"] = {}
        if instance.shared_inbox_url:
            ret["endpoints"]["sharedInbox"] = instance.shared_inbox_url
        try:
            if instance.user.profile and instance.user.profile.image:
                ret["icon"] = {
                    "type": "Image",
                    "mediaType": mimetypes.guess_type(instance.user.profile.image.path)[0],
                    "url": utils.full_url(instance.user.profile.avatar),
                }
        except ObjectDoesNotExist:
            pass
        return ret

    def prepare_missing_fields(self):
        kwargs = {
            "fid": self.validated_data["id"],
            "url": self.validated_data.get("url", None),
            "outbox_url": self.validated_data.get("outbox", None),
            "inbox_url": self.validated_data["inbox"],
            "following_url": self.validated_data.get("following", None),
            "followers_url": self.validated_data.get("followers"),
            "summary": self.validated_data.get("summary"),
            "type": self.validated_data["type"],
            "name": self.validated_data.get("name"),
            "preferred_username": self.validated_data["preferredUsername"],
        }
        maf = self.initial_data.get("manuallyApprovesFollowers", None)
        if maf is not None:
            kwargs["manually_approves_followers"] = maf
        domain = urllib.parse.urlparse(kwargs["fid"]).netloc
        kwargs["domain"] = models.Domain.objects.get_or_create(pk=domain)[0]
        for endpoint, url in self.initial_data.get("endpoints", {}).items():
            if endpoint == "sharedInbox":
                kwargs["shared_inbox_url"] = url
                break
        try:
            kwargs["public_key"] = self.initial_data["publicKey"]["publicKeyPem"]
        except KeyError:
            pass
        return kwargs

    def build(self):
        d = self.prepare_missing_fields()
        return models.Actor(**d)

    def save(self, **kwargs):
        d = self.prepare_missing_fields()
        d.update(kwargs)
        return models.Actor.objects.update_or_create(fid=d["fid"], defaults=d)[0]

    def validate_summary(self, value):
        if value:
            return value[:500]


class APIActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Actor
        fields = [
            "id",
            "fid",
            "url",
            "creation_date",
            "summary",
            "preferred_username",
            "name",
            "last_fetch_date",
            "domain",
            "type",
            "manually_approves_followers",
            "full_username",
        ]


class BaseActivitySerializer(serializers.Serializer):
    id = serializers.URLField(max_length=500, required=False)
    type = serializers.CharField(max_length=100)
    actor = serializers.URLField(max_length=500)

    def validate_actor(self, v):
        expected = self.context.get("actor")
        if expected and expected.fid != v:
            raise serializers.ValidationError("Invalid actor")
        if expected:
            # avoid a DB lookup
            return expected
        try:
            return models.Actor.objects.get(fid=v)
        except models.Actor.DoesNotExist:
            raise serializers.ValidationError("Actor not found")

    def create(self, validated_data):
        return models.Activity.objects.create(
            fid=validated_data.get("id"),
            actor=validated_data["actor"],
            payload=self.initial_data,
            type=validated_data["type"],
        )

    def validate(self, data):
        data["recipients"] = self.validate_recipients(self.initial_data)
        return super().validate(data)

    def validate_recipients(self, payload):
        """
        Ensure we have at least a to/cc field with valid actors
        """
        to = payload.get("to", [])
        cc = payload.get("cc", [])

        if not to and not cc:
            raise serializers.ValidationError(
                "We cannot handle an activity with no recipient"
            )


class FollowSerializer(serializers.Serializer):
    id = serializers.URLField(max_length=500)
    object = serializers.URLField(max_length=500)
    actor = serializers.URLField(max_length=500)
    type = serializers.ChoiceField(choices=["Follow"])

    def validate_object(self, v):
        expected = self.context.get("follow_target")
        if self.parent:
            # it's probably an accept, so everything is inverted, the actor
            # the recipient does not matter
            recipient = None
        else:
            recipient = self.context.get("recipient")
        if expected and expected.fid != v:
            raise serializers.ValidationError("Invalid target")
        try:
            obj = models.Actor.objects.get(fid=v)
            if recipient and recipient.fid != obj.fid:
                raise serializers.ValidationError("Invalid target")
            return obj
        except models.Actor.DoesNotExist:
            pass

        raise serializers.ValidationError("Target not found")

    def validate_actor(self, v):
        expected = self.context.get("follow_actor")
        if expected and expected.fid != v:
            raise serializers.ValidationError("Invalid actor")
        try:
            return models.Actor.objects.get(fid=v)
        except models.Actor.DoesNotExist:
            raise serializers.ValidationError("Actor not found")

    def save(self, **kwargs):
        target = self.validated_data["object"]

        # if target._meta.label == "music.Library":
        #     follow_class = models.LibraryFollow
        # else:
        #     follow_class = models.Follow
        follow_class = models.Follow
        defaults = kwargs
        defaults["fid"] = self.validated_data["id"]
        return follow_class.objects.update_or_create(
            actor=self.validated_data["actor"],
            target=self.validated_data["object"],
            defaults=defaults,
        )[0]

    def to_representation(self, instance):
        return {
            "@context": AP_CONTEXT,
            "actor": instance.actor.fid,
            "id": instance.get_federation_id(),
            "object": instance.target.fid,
            "type": "Follow",
        }


class TagSerializer(jsonld.JsonLdSerializer):
    type = serializers.ChoiceField(choices=[
        contexts.AS.Hashtag,
        # TODO: put contexts.SCIFED.Hashtag
    ])
    name = serializers.CharField(max_length=100)

    class Meta:
        jsonld_mapping = {
            "name": jsonld.first_val(contexts.AS.name)
        }

    def validate_name(self, value):
        if value.startswith("#"):
            # remove trailing #
            value = value[1:]
        return value


class APIFollowSerializer(serializers.ModelSerializer):
    actor = APIActorSerializer()
    target = APIActorSerializer()

    class Meta:
        model = models.Follow
        fields = [
            "uuid",
            "id",
            "approved",
            "creation_date",
            "modification_date",
            "actor",
            "target",
        ]


class AcceptFollowSerializer(serializers.Serializer):
    id = serializers.URLField(max_length=500, required=False)
    actor = serializers.URLField(max_length=500)
    object = FollowSerializer()
    type = serializers.ChoiceField(choices=["Accept"])

    def validate_actor(self, v):
        expected = self.context.get("actor")
        if expected and expected.fid != v:
            raise serializers.ValidationError("Invalid actor")
        try:
            return models.Actor.objects.get(fid=v)
        except models.Actor.DoesNotExist:
            raise serializers.ValidationError("Actor not found")

    def validate(self, validated_data):
        # we ensure the accept actor actually match the follow target / library owner
        target = validated_data["object"]["object"]

        if target._meta.label == "music.Library":
            expected = target.actor
            follow_class = models.LibraryFollow
        else:
            expected = target
            follow_class = models.Follow
        if validated_data["actor"] != expected:
            raise serializers.ValidationError("Actor mismatch")
        try:
            validated_data["follow"] = (
                follow_class.objects.filter(
                    target=target, actor=validated_data["object"]["actor"]
                )
                .exclude(approved=True)
                .select_related()
                .get()
            )
        except follow_class.DoesNotExist:
            raise serializers.ValidationError("No follow to accept")
        return validated_data

    def to_representation(self, instance):
        if instance.target._meta.label == "music.Library":
            actor = instance.target.actor
        else:
            actor = instance.target

        return {
            "@context": AP_CONTEXT,
            "id": instance.get_federation_id() + "/accept",
            "type": "Accept",
            "actor": actor.fid,
            "object": FollowSerializer(instance).data,
        }

    def save(self):
        follow = self.validated_data["follow"]
        follow.approved = True
        follow.save()
        if follow.target._meta.label == "music.Library":
            follow.target.schedule_scan(actor=follow.actor)
        return follow


class UndoFollowSerializer(serializers.Serializer):
    id = serializers.URLField(max_length=500)
    actor = serializers.URLField(max_length=500)
    object = FollowSerializer()
    type = serializers.ChoiceField(choices=["Undo"])

    def validate_actor(self, v):
        expected = self.context.get("actor")

        if expected and expected.fid != v:
            raise serializers.ValidationError("Invalid actor")
        try:
            return models.Actor.objects.get(fid=v)
        except models.Actor.DoesNotExist:
            raise serializers.ValidationError("Actor not found")

    def validate(self, validated_data):
        # we ensure the accept actor actually match the follow actor
        if validated_data["actor"] != validated_data["object"]["actor"]:
            raise serializers.ValidationError("Actor mismatch")

        target = validated_data["object"]["object"]

        if target._meta.label == "music.Library":
            follow_class = models.LibraryFollow
        else:
            follow_class = models.Follow

        try:
            validated_data["follow"] = follow_class.objects.filter(
                actor=validated_data["actor"], target=target
            ).get()
        except follow_class.DoesNotExist:
            raise serializers.ValidationError("No follow to remove")
        return validated_data

    def to_representation(self, instance):
        return {
            "@context": AP_CONTEXT,
            "id": instance.get_federation_id() + "/undo",
            "type": "Undo",
            "actor": instance.actor.fid,
            "object": FollowSerializer(instance).data,
        }

    def save(self):
        return self.validated_data["follow"].delete()


class ActorWebfingerSerializer(serializers.Serializer):
    subject = serializers.CharField()
    aliases = serializers.ListField(child=serializers.URLField(max_length=500))
    links = serializers.ListField()
    actor_url = serializers.URLField(max_length=500, required=False)

    def validate(self, validated_data):
        validated_data["actor_url"] = None
        for l in validated_data["links"]:
            try:
                if not l["rel"] == "self":
                    continue
                if not l["type"] == "application/activity+json":
                    continue
                validated_data["actor_url"] = l["href"]
                break
            except KeyError:
                pass
        if validated_data["actor_url"] is None:
            raise serializers.ValidationError("No valid actor url found")
        return validated_data

    def to_representation(self, instance):
        data = {
            "subject": "acct:{}".format(instance.webfinger_subject),
            "links": [
                {"rel": "self", "href": instance.fid, "type": "application/activity+json"},
                {"rel": "http://webfinger.net/rel/profile-page", "href": instance.profile_url, "type": "text/html"},
            ],
            "aliases": [instance.fid, instance.profile_url]
        }
        return data


class ActivitySerializer(serializers.Serializer):
    actor = serializers.URLField(max_length=500)
    id = serializers.URLField(max_length=500, required=False)
    type = serializers.ChoiceField(choices=[(c, c) for c in activity.ACTIVITY_TYPES])
    object = serializers.JSONField(required=False)
    target = serializers.JSONField(required=False)

    def validate_object(self, value):
        try:
            type = value["type"]
        except KeyError:
            raise serializers.ValidationError("Missing object type")
        except TypeError:
            # probably a URL
            return value

        try:
            object_serializer = OBJECT_SERIALIZERS[type]
        except KeyError:
            raise serializers.ValidationError("Unsupported type {}".format(type))

        serializer = object_serializer(data=value)
        serializer.is_valid(raise_exception=True)
        return serializer.data

    def validate_actor(self, value):
        request_actor = self.context.get("actor")
        if request_actor and request_actor.fid != value:
            raise serializers.ValidationError(
                "The actor making the request do not match" " the activity actor"
            )
        return value

    def to_representation(self, conf):
        try:
            d = super(ActivitySerializer, self).to_representation(conf)
            d['id'] = conf.fid

            if self.context.get("include_ap_context", True):
                d["@context"] = AP_CONTEXT
            d["audience"] = (
                contexts.AS.Public  # TODO: make audience vary depending on profile setting
            )
        except Exception as e:
            logger.info(e)
        return d


class ObjectSerializer(serializers.Serializer):
    id = serializers.URLField(max_length=500)
    url = serializers.URLField(max_length=500, required=False, allow_null=True)
    type = serializers.ChoiceField(choices=[(c, c) for c in activity.OBJECT_TYPES])
    content = serializers.CharField(required=False, allow_null=True)
    summary = serializers.CharField(required=False, allow_null=True)
    name = serializers.CharField(required=False, allow_null=True)
    published = serializers.DateTimeField(required=False, allow_null=True)
    updated = serializers.DateTimeField(required=False, allow_null=True)
    to = serializers.ListField(
        child=serializers.URLField(max_length=500), required=False, allow_null=True
    )
    cc = serializers.ListField(
        child=serializers.URLField(max_length=500), required=False, allow_null=True
    )
    bto = serializers.ListField(
        child=serializers.URLField(max_length=500), required=False, allow_null=True
    )
    bcc = serializers.ListField(
        child=serializers.URLField(max_length=500), required=False, allow_null=True
    )

    def to_representation(self, conf):
        from olki.apps.core.datestamp import datetime_to_datestamp
        conf['published'] = datetime_to_datestamp(conf['published'])
        return super().to_representation(conf)


OBJECT_SERIALIZERS = {t: ObjectSerializer for t in activity.OBJECT_TYPES}


def get_additional_fields(data):
    UNSET = object()
    additional_fields = {}
    for field in ["name", "summary"]:
        v = data.get(field, UNSET)
        if v == UNSET:
            continue
        additional_fields[field] = v

    return additional_fields


PAGINATED_COLLECTION_JSONLD_MAPPING = {
    "totalItems": jsonld.first_val(contexts.AS.totalItems),
    "first": jsonld.first_id(contexts.AS.first),
    "last": jsonld.first_id(contexts.AS.last),
    "partOf": jsonld.first_id(contexts.AS.partOf),
}


class PaginatedOrderedCollectionSerializer(jsonld.JsonLdSerializer):
    type = serializers.ChoiceField(choices=[contexts.AS.OrderedCollection])
    totalItems = serializers.IntegerField(min_value=0)
    id = serializers.URLField(max_length=500)
    first = serializers.URLField(max_length=500)
    last = serializers.URLField(max_length=500)

    class Meta:
        jsonld_mapping = PAGINATED_COLLECTION_JSONLD_MAPPING

    def to_representation(self, conf):
        paginator = Paginator(conf["orderedItems"], conf.get("page_size", 20))
        first = olki_utils.set_query_parameter(conf["id"], page=1)
        last = olki_utils.set_query_parameter(conf["id"], page=paginator.num_pages)
        d = {
            "id": conf["id"],
            "totalItems": paginator.count,
            "type": conf.get("type", "OrderedCollection"),
            "first": first,
            "last": last,
        }
        d.update(get_additional_fields(conf))
        if self.context.get("include_ap_context", True):
            d["@context"] = "https://www.w3.org/ns/activitystreams"
        return d


class OrderedCollectionPageSerializer(jsonld.JsonLdSerializer):
    type = serializers.ChoiceField(choices=[contexts.AS.OrderedCollectionPage])
    totalItems = serializers.IntegerField(min_value=0)
    orderedItems = serializers.ListField()
    id = serializers.URLField(max_length=500)
    first = serializers.URLField(max_length=500)
    last = serializers.URLField(max_length=500)
    next = serializers.URLField(max_length=500, required=False)
    prev = serializers.URLField(max_length=500, required=False)
    partOf = serializers.URLField(max_length=500)

    class Meta:
        jsonld_mapping = {
            "totalItems": jsonld.first_val(contexts.AS.totalItems),
            "orderedItems": jsonld.raw(contexts.AS.orderedItems),
            "first": jsonld.first_id(contexts.AS.first),
            "last": jsonld.first_id(contexts.AS.last),
            "next": jsonld.first_id(contexts.AS.next),
            "prev": jsonld.first_id(contexts.AS.prev),
            "partOf": jsonld.first_id(contexts.AS.partOf),
        }

    def validate_items(self, v):
        item_serializer = self.context.get("item_serializer")
        if not item_serializer:
            return v
        raw_items = [item_serializer(data=i, context=self.context) for i in v]
        valid_items = []
        for i in raw_items:
            try:
                i.is_valid(raise_exception=True)
                valid_items.append(i)
            except serializers.ValidationError:
                logger.debug("Invalid item %s: %s", i.data, i.errors)

        return valid_items

    def to_representation(self, conf):
        page = conf["page"]
        first = olki_utils.set_query_parameter(conf["id"], page=1)
        last = olki_utils.set_query_parameter(
            conf["id"], page=page.paginator.num_pages
        )
        id = olki_utils.set_query_parameter(conf["id"], page=page.number)
        d = {
            "id": id,
            "partOf": conf["id"],
            "totalItems": page.paginator.count,
            "type": "OrderedCollectionPage",
            "first": first,
            "last": last,
            "orderedItems": [
                conf["item_serializer"](
                    i, context={"include_ap_context": False}
                ).data
                for i in page.object_list
            ],
        }

        if page.has_previous():
            d["prev"] = olki_utils.set_query_parameter(
                conf["id"], page=page.previous_page_number()
            )

        if page.has_next():
            d["next"] = olki_utils.set_query_parameter(
                conf["id"], page=page.next_page_number()
            )
        d.update(get_additional_fields(conf))
        if self.context.get("include_ap_context", True):
            d["@context"] = "https://www.w3.org/ns/activitystreams"
        return d


class NoteSerializer(jsonld.JsonLdSerializer, ObjectSerializer):
    type = serializers.ChoiceField(choices=[contexts.AS.Note])
    content = serializers.CharField()

    sensitive = serializers.BooleanField(required=False, default=False)
    attributedTo = serializers.URLField(max_length=500, allow_null=True, required=False)
    inReplyTo = serializers.URLField(max_length=500, allow_null=True, required=False)
    audience = serializers.ChoiceField(
        choices=["", "./", None, "https://www.w3.org/ns/activitystreams#Public"],
        required=False,
        allow_null=True,
        allow_blank=True,
    )
    tag = serializers.ListField(child=TagSerializer(), min_length=0, required=False, allow_null=True)

    class Meta:
        model = Note
        jsonld_mapping = olki_utils.concat_dicts(
            {
                "content": jsonld.first_val(contexts.AS.content),
                "audience": jsonld.first_id(contexts.AS.audience),
                "published": jsonld.first_val(contexts.AS.published),
                "updated": jsonld.first_val(contexts.AS.updated),
                "name": jsonld.first_id(contexts.AS.name),
                "url": jsonld.first_id(contexts.AS.url),
                "tag": jsonld.first_id(contexts.AS.tag),
                "attributedTo": jsonld.first_id(contexts.AS.attributedTo),
                "inReplyTo": jsonld.first_id(contexts.AS.inReplyTo),
            },
        )

    def to_representation(self, note):
        r = {}
        try:
            # ObjectSerializer-specific properties
            conf = {
                "id": note.fid or "{}/note/{}".format(settings.INSTANCE_URL, note.id),
                "url": "{}/note/{}".format(settings.INSTANCE_URL, note.id),
                "name": note.name,
                "content": note.content,
                "summary": note.summary or "",
                "published": note.created_at,
                "type": "Note",
            }
            r = super().to_representation(conf)

            # NoteSerializer-specific properties
            r["attributedTo"] = {
                "type": "Person",
                "id": note.actor.fid
            }
            r["inReplyTo"] = note.inReplyTo
            r["tag"] = []

            # Audience
            r["audience"] = (
                contexts.AS.Public if note.public else ""
            )
            r["to"] = [
                activity.PUBLIC_ADDRESS
            ]
            r["cc"] = [
                utils.full_url(
                    reverse(
                        "federation:actor-followers",
                        kwargs={"preferred_username": note.actor.preferred_username},
                    )
                )
            ]
            r["sensitive"] = False

            # Context
            r["@context"] = AP_CONTEXT
        except Exception as e:
            logger.info(e)
        return r

    def create(self, validated_data):
        actor = utils.retrieve_ap_object(
            validated_data.get("attributedTo"),
            actor=self.context.get("fetch_actor"),
            queryset=models.Actor,
            serializer_class=ActorSerializer,
        )
        note, created = Note.objects.update_or_create(
            fid=validated_data.get("id"),
            actor=actor,
            corpus=models.Activity.objects.filter(payload__object__id=validated_data.get("inReplyTo")).first().target,
            defaults={**{
                key: value
                for key, value in list(validated_data.items())
                if key in [f.name for f in Note._meta.get_fields()] and key not in ['id', 'actor']
            }}
        )
        # no need to update the activity's properties, activity.InboxRouter.dispatch handles that for us
        return note

    def update(self, instance, validated_data):
        pass


class CorpusSerializer(jsonld.JsonLdSerializer, ObjectSerializer):
    type = serializers.ChoiceField(choices=[contexts.SCIFED.Corpus])  # FIXME: switch to Corpus type
    content = serializers.CharField()

    sensitive = serializers.BooleanField(required=False, default=False)
    attributedTo = serializers.URLField(max_length=500, allow_null=True, required=False)
    inReplyTo = serializers.URLField(max_length=500, allow_null=True, required=False)
    audience = serializers.ChoiceField(
        choices=["", "./", None, "https://www.w3.org/ns/activitystreams#Public"],
        required=False,
        allow_null=True,
        allow_blank=True,
    )
    tag = serializers.ListField(child=TagSerializer(), min_length=0, required=False, allow_null=True)
    attachment = serializers.ListField(child=LinkSerializer(), min_length=0, required=False, allow_null=True)

    class Meta:
        jsonld_mapping = olki_utils.concat_dicts(
            {
                "content": jsonld.first_val(contexts.AS.content),
                "audience": jsonld.first_id(contexts.AS.audience),
                "published": jsonld.first_val(contexts.AS.published),
                "updated": jsonld.first_val(contexts.AS.updated),
                "name": jsonld.first_id(contexts.AS.name),
                "url": jsonld.first_id(contexts.AS.url),
                "tag": jsonld.first_id(contexts.AS.tag),
                "attributedTo": jsonld.first_id(contexts.AS.attributedTo),
                "inReplyTo": jsonld.first_id(contexts.AS.inReplyTo),
                "followers": jsonld.first_id(contexts.AS.followers),
                "attachment": jsonld.first_id(contexts.AS.attachment),
            },
        )

    def to_representation(self, corpus):
        r = {}
        try:
            # ObjectSerializer-specific properties
            conf = {
                "id": corpus.fid or "{}/corpus/{}".format(settings.INSTANCE_URL, corpus.id),
                "url": "{}/corpus/{}".format(settings.INSTANCE_URL, corpus.id),
                "name": corpus.title,
                "content": corpus.description,
                "summary": '',
                "published": corpus.created_at,
                "type": "Corpus",  # FIXME: switch to Corpus type
            }
            r = super().to_representation(conf)

            r["attributedTo"] = [
                {
                    "id": corpus.actor.fid,
                    "type": "Person"
                }
            ]

            # Audience
            r["audience"] = (
                contexts.AS.Public
            )
            r["to"] = [
                activity.PUBLIC_ADDRESS
            ]
            r["cc"] = [
                utils.full_url(
                    reverse(
                        "federation:actor-followers",
                        kwargs={"preferred_username": corpus.actor.preferred_username},
                    )
                )
            ]

            r["mediaType"] = "text/markdown"
            r["commentsEnabled"] = corpus.comments_allowed
            from olki.apps.corpus.views import FederatedRepresentationsOfACorpus
            r["url"] = [
               {
                   "type": "Link",
                   "mimeType": "text/html",
                   "mediaType": "text/html",
                   "href": "{}/corpus/{}".format(settings.INSTANCE_URL, corpus.id)
               }
            ] + [
                {
                    "href": "{domain}{}".format(
                        reverse("corpus:{}".format(representation.get('named_url')), kwargs={"corpus_id": corpus.id}),
                        domain=settings.INSTANCE_URL
                    ),
                    "type": "Link",
                    **{k: v for k, v in representation.items() if not k.startswith('named_url')}
                }
                for representation in FederatedRepresentationsOfACorpus
            ]
            r["tag"] = [
                {
                    "type": "Hashtag",
                    "href": "{domain}/?{tag}".format(
                        domain=settings.INSTANCE_URL,
                        tag=urlencode({"tag": json.dumps({
                            "name": tag.name,
                            "type": 'tag',
                            "filtervalue": tag.code
                        })}, quote_via=urllib.parse.quote)
                    ),
                    "name": tag.name
                }
                for tag in corpus.tags.all()
            ]
            r["attachment"] = [
                {
                    "type": "Link",
                    "size": file.filesize,
                    "mimeType": file.filetype,
                    "href": file.file
                }
                for file in corpus.files.all()
            ]

            # Context
            r["@context"] = AP_CONTEXT
        except Exception as e:
            logger.info(e)
        return r

    def create(self, validated_data):
        pass


class ActorDeleteSerializer(jsonld.JsonLdSerializer):
    fid = serializers.URLField(max_length=500)

    class Meta:
        jsonld_mapping = {"fid": jsonld.first_id(contexts.AS.object)}


class NodeInfoLinkSerializer(serializers.Serializer):
    href = serializers.URLField()
    rel = serializers.URLField()


class NodeInfoSerializer(serializers.Serializer):
    links = serializers.ListField(child=NodeInfoLinkSerializer(), min_length=1)
