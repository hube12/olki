# pylint: skip-file

"""py
    Original author: Eliot Berriot
    Project of origin: Funkwhale
    Commit of origin: 49769819265170a771e747b8696de6a27a100379
    Project licence: AGPLv3.0 or later

The module registers "routes" in the form of object matchings
that are triggered when a dispatched message matches the type
and potential 'object.type'. See the Route.register function
in `olki.apps.federation.activity`.

Don't forget a route is usually can be registered twice: once
for the inbox router, and once for the outbox (corresponding
to deserializing/serializing messages, resp.).

--- How/what to serialize?
You need to yield a dict that the dispatch can later transform
into an Activity. It will hold the links to actors, objects,
targets, etc. in the database, but that's not what is sent on
the wire! Essentially what is sent is the `payload` property of
that dict. You need to serialize the payload, but what is the
payload? It contains:
- a serialized verb (Follow, Create, etc.) with a context:
    - a serialized object, for instance (Note, Corpus, etc.)

--- What gets sent on the network?
In the end, just the payload.
"""
import logging

from django.conf import settings
from django.urls import reverse

from olki.apps.core import utils
from . import activity
from . import models
from . import serializers

logger = logging.getLogger(__name__)
inbox = activity.InboxRouter()
outbox = activity.OutboxRouter()


def with_recipients(payload, to=[], cc=[]):
    if to:
        payload["to"] = to
    if cc:
        payload["cc"] = cc
    print("with_recipients:", payload)
    return payload


@inbox.register({"type": "Follow"})
def inbox_follow(payload, context):
    context["recipient"] = [
        ii.actor for ii in context["inbox_items"] if ii.type == "to"
    ][0]
    serializer = serializers.FollowSerializer(data=payload, context=context)
    if not serializer.is_valid(raise_exception=context.get("raise_exception", False)):
        logger.debug(
            "Discarding invalid follow from {}: %s",
            context["actor"].fid,
            serializer.errors,
        )
        return

    autoapprove = serializer.validated_data["object"].should_autoapprove_follow(
        context["actor"]
    )
    follow = serializer.save(approved=True if autoapprove else None)
    if follow.approved:
        outbox.dispatch({"type": "Accept"}, context={"follow": follow})
    return {"object": follow.target, "related_object": follow}


@inbox.register({"type": "Accept"})
def inbox_accept(payload, context):
    context["recipient"] = [
        ii.actor for ii in context["inbox_items"] if ii.type == "to"
    ][0]
    serializer = serializers.AcceptFollowSerializer(data=payload, context=context)
    if not serializer.is_valid(raise_exception=context.get("raise_exception", False)):
        logger.debug(
            "Discarding invalid accept from {}: %s",
            context["actor"].fid,
            serializer.errors,
        )
        return

    serializer.save()
    obj = serializer.validated_data["follow"]
    return {"object": obj, "related_object": obj.target}


@outbox.register({"type": "Accept"})
def outbox_accept(context):
    follow = context["follow"]
    # if follow._meta.label == "federation.DatasetFollow":
    #     actor = follow.target.actor
    # else:
    #     actor = follow.target
    actor = follow.target
    payload = serializers.AcceptFollowSerializer(follow, context={"actor": actor}).data
    yield {
        "actor": actor,
        "type": "Accept",
        "payload": with_recipients(payload, to=[follow.actor]),
        "object": follow,
        "related_object": follow.target,
    }


@inbox.register({"type": "Undo", "object.type": "Follow"})
def inbox_undo_follow(payload, context):
    serializer = serializers.UndoFollowSerializer(data=payload, context=context)
    if not serializer.is_valid(raise_exception=context.get("raise_exception", False)):
        logger.debug(
            "Discarding invalid follow undo from %s: %s",
            context["actor"].fid,
            serializer.errors,
        )
        return

    serializer.save()


@outbox.register({"type": "Undo", "object.type": "Follow"})
def outbox_undo_follow(context):
    follow = context["follow"]
    actor = follow.actor
    # if follow._meta.label == "federation.DatasetFollow":
    #     recipient = follow.target.actor
    # else:
    #     recipient = follow.target
    recipient = follow.target
    payload = serializers.UndoFollowSerializer(follow, context={"actor": actor}).data
    yield {
        "actor": actor,
        "type": "Undo",
        "payload": with_recipients(payload, to=[recipient]),
        "object": follow,
        "related_object": follow.target,
    }


@outbox.register({"type": "Follow"})
def outbox_follow(context):
    follow = context["follow"]
    # if follow._meta.label == "federation.DatasetFollow":
    #     target = follow.target.actor
    # else:
    #     target = follow.target
    target = follow.target
    payload = serializers.FollowSerializer(follow, context={"actor": follow.actor}).data
    yield {
        "type": "Follow",
        "actor": follow.actor,
        "payload": with_recipients(payload, to=[target]),
        "object": follow.target,
        "related_object": follow,
    }


##
# `Create` types below
##


@outbox.register({"type": "Create", "object.type": "Corpus"})
def outbox_create_corpus(context):
    corpus = context["corpus"]
    corpus_data = serializers.Serializer(corpus).data
    a = serializers.ActivitySerializer(
        {"type": "Create", "object": corpus_data}
    )  # we do not call .data, as ActivitySerializer looks for context.actor, which it will find in create_data
    yield {
        "type": "Create",
        "actor": corpus.actor,
        "payload": with_recipients(
            a.data,
            # TODO: notify all authors' followers
            cc=[
                utils.full_url(
                    reverse(
                        "federation:actor-followers",
                        kwargs={"preferred_username": corpus.actor.preferred_username},
                    )
                )
            ],
            to=[activity.PUBLIC_ADDRESS]
        ),
        "object": corpus
    }

# works with i.e. `{"type": "Create", "object": {"type": "Note"}}, context={"note": note}`
# in for example activity.OutboxRouter.dispatch
# i.e. `outbox.dispatch({"type": "Create", "object": {"type": "Note"}}, context={"note": note})`
@outbox.register({"type": "Create", "object.type": "Note"})
def outbox_create_note(context):
    note = context["note"]
    note_data = serializers.NoteSerializer(note).data
    a = serializers.ActivitySerializer(
        {"type": "Create", "object": note_data, "actor": note.actor},
        fid="{}/note/{}/activity".format(settings.INSTANCE_URL, note.id)
    )  # we do not call .data, as ActivitySerializer looks for context.actor, which it will find in create_data
    yield {
        "type": "Create",
        "actor": note.actor,
        "payload": with_recipients(
            a.data,
            # TODO: notify more than just the corpus author, but also the mentioned and replied-to actors.
            to=[note.corpus.actor]
        ),
        "object": note,
        "target": note.corpus
    }


@inbox.register({"type": "Create", "object.type": "Note"})
def inbox_create_note(payload, context):
    serializer = serializers.NoteSerializer(
        data=payload["object"],
        context={"activity": context.get("activity"), "actor": context["actor"]},
    )

    if not serializer.is_valid(raise_exception=context.get("raise_exception", True)):
        logger.warning("Discarding invalid note create")
        return

    note = serializer.save()

    return {"object": note, "target": note.corpus}


@outbox.register({"type": "Update", "object.type": "Actor"})
def outbox_update_actor(context):
    actor = context["actor"]
    actor_data = serializers.ActorSerializer(actor).data
    a = serializers.ActivitySerializer(
        {"type": "Update", "object": actor_data, "actor": actor}
    )  # we do not call .data, as ActivitySerializer looks for context.actor, which it will find in create_data
    yield {
        "type": "Update",
        "actor": actor,
        "payload": with_recipients(
            a.data,
            # TODO: notify followers, and people who interacted with the actor's corpora
            cc=[
                utils.full_url(
                    reverse(
                        "federation:actor-followers",
                        kwargs={"preferred_username": actor.preferred_username},
                    )
                )
            ]
        ),
        "object": actor,
        "target": actor
    }

##
# `Delete` types below
##

@outbox.register(
    {
        "type": "Delete",
        "object.type": [
            "Tombstone",
            "Actor",
            "Person",
            "Application",
            "Organization",
            "Service",
            "Group",
        ],
    }
)
def outbox_delete_actor(context):
    actor = context["actor"]
    serializer = serializers.ActivitySerializer(
        {"type": "Delete", "object": {"type": actor.type, "id": actor.fid}}
    )
    yield {
        "type": "Delete",
        "actor": actor,
        "payload": with_recipients(
            serializer.data,
            to=[activity.PUBLIC_ADDRESS, {"type": "instances_with_followers"}],
        ),
    }


@inbox.register(
    {
        "type": "Delete",
        "object.type": [
            "Tombstone",
            "Actor",
            "Person",
            "Application",
            "Organization",
            "Service",
            "Group",
        ],
    }
)
def inbox_delete_actor(payload, context):
    actor = context["actor"]
    serializer = serializers.ActorDeleteSerializer(data=payload)
    if not serializer.is_valid():
        logger.info("Skipped actor %s deletion, invalid payload", actor.fid)
        return

    deleted_fid = serializer.validated_data["fid"]
    try:
        # ensure the actor only can delete itself, and is a remote one
        actor = models.Actor.objects.local(False).get(fid=deleted_fid, pk=actor.pk)
    except models.Actor.DoesNotExist:
        logger.warn("Cannot delete actor %s, no matching object found", actor.fid)
        return
