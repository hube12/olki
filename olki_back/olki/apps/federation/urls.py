from rest_framework import routers

from . import views

router = routers.SimpleRouter(trailing_slash=False)

"""The ActorViewSet defines the shared inbox"""
router.register(r"federation/shared", views.SharedViewSet, "shared")

"""
The ActorViewSet defines:
- an actor's inbox
- an actor's outbox
- an actor's AS representation @id
- an actor's followers list
- an actor's following list
"""
router.register(r"federation/actor", views.ActorViewSet, "actor")

"""
The WellKnownViewSet defines:
- nodeinfo
- webfinger
"""
router.register(r".well-known", views.WellKnownViewSet, "well-known")

from . import api_views

router.register(r"fetches", api_views.FetchViewSet, "fetches")
router.register(r"inbox", api_views.InboxItemViewSet, "inbox")

urlpatterns = router.urls
