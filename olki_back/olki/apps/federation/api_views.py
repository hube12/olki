from django.db import transaction
from rest_framework import decorators
from rest_framework import mixins
from rest_framework import permissions
from rest_framework import response
from rest_framework import viewsets

from . import activity
from . import filters
from . import models
from . import routes
from . import serializers_api


@transaction.atomic
def update_follow(follow, approved):
    follow.approved = approved
    follow.save(update_fields=["approved"])
    if approved:
        routes.outbox.dispatch({"type": "Accept"}, context={"follow": follow})


class InboxItemViewSet(
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):

    queryset = (
        models.InboxItem.objects.select_related("activity__actor")
        .prefetch_related("activity__object", "activity__target")
        .filter(activity__type__in=activity.BROADCAST_TO_USER_ACTIVITIES, type="to")
        .order_by("-activity__creation_date")
    )
    serializer_class = serializers_api.InboxItemSerializer
    # permission_classes = [oauth_permissions.ScopePermission]
    permission_classes = [permissions.IsAuthenticated]
    required_scope = "notifications"
    filterset_class = filters.InboxItemFilter
    ordering_fields = ("activity__creation_date",)

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(actor=self.request.user.actor)

    @decorators.action(methods=["post"], detail=False)
    def action(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = serializers_api.InboxItemActionSerializer(
            request.data, queryset=queryset
        )
        serializer.is_valid(raise_exception=True)
        result = serializer.save()
        return response.Response(result, status=200)


class FetchViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = models.Fetch.objects.select_related("actor")
    serializer_class = serializers_api.FetchSerializer
    permission_classes = [permissions.IsAuthenticated]
