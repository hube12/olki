from unittest import skip
from django.test import TestCase

import pytest
import requests_mock

from . import jsonld, models, serializers, keys


class TestFederationViews(TestCase):
    @pytest.mark.usefixtures('fixtures')
    def test_note_serializer(self):
        note = self.factories['note.Note']()
        serializer = serializers.NoteSerializer(note)

        assert serializer.data
        assert serializer.data.get("id")
        assert serializer.data.get("type")
        assert "Note" in serializer.data.get("type")
        assert serializer.data.get("published")
        assert serializer.data.get("url")
        assert "http" in serializer.data.get("url")


@skip("Actors are not mocked and .save() tries to retrieve them by their url")
def test_note_serializer_from_ap(db, r_mock):
    r_mock.register_uri(requests_mock.ANY, requests_mock.ANY, text='{}')
    actor_url = "https://test.federation/actor"
    payload = {
        "@context": jsonld.get_default_context(),
        'cc': [],
        'id': 'https://test.federation/users/rigelk/statuses/102865587793760562',
        'to': [actor_url],
        'tag': [
            {
                'href': 'https://olki.federation/federation/actor/rigelk',
                'name': '@rigelk@olki.federation',
                'type': 'Mention'
            }
        ],
        'url': 'https://test.federation/@rigelk/102865587793760562',
        'type': 'Note',
        'atomUri': 'https://test.federation/users/rigelk/statuses/102865587793760562',
        'content': '<p><span class="h-card"><a href="https://olki.federation/@rigelk" class="u-url mention" rel="nofollow noopener tag" target="_blank">@<span>rigelk</span></a></span> test 8</p>',
        'replies': {
            'id': 'https://test.federation/users/rigelk/statuses/102865587793760562/replies',
            'type': 'Collection', 'first': {
                'next': 'https://test.federation/users/rigelk/statuses/102865587793760562/replies?only_other_accounts=true&page=true',
                'type': 'CollectionPage',
                'items': [],
                'partOf': 'https://test.federation/users/rigelk/statuses/102865587793760562/replies'
            }
        },
        'summary': None,
        'inReplyTo': 'https://olki.federation/note/7_xE1nEUOC29-T4V',
        'published': '2019-09-27T17:11:45Z',
        'sensitive': False,
        'attachment': [],
        'contentMap': {
            'fr': '<p><span class="h-card"><a href="https://olki.federation/@rigelk" class="u-url mention" rel="nofollow noopener tag" target="_blank">@<span>rigelk</span></a></span> test 8</p>'},
        'attributedTo': 'https://test.federation/users/rigelk',
        'conversation': 'tag:test.federation,2019-09-18:objectId=4250098:objectType=Conversation',
        'inReplyToAtomUri': 'https://olki.federation/note/7_xE1nEUOC29-T4V'
    }

    serializer = serializers.NoteSerializer(data=payload)
    assert serializer.is_valid(raise_exception=True)
    note = serializer.save()


def test_actor_serializer_from_ap(db):
    private, public = keys.get_key_pair()
    actor_url = "https://test.federation/actor"
    payload = {
        "@context": jsonld.get_default_context(),
        "id": actor_url,
        "type": "Person",
        "outbox": "https://test.com/outbox",
        "inbox": "https://test.com/inbox",
        "following": "https://test.com/following",
        "followers": "https://test.com/followers",
        "preferredUsername": "test",
        "name": "Test",
        "summary": "Hello world",
        "manuallyApprovesFollowers": True,
        "publicKey": {
            "publicKeyPem": public.decode("utf-8"),
            "owner": actor_url,
            "id": actor_url + "#main-key",
        },
        "endpoints": {"sharedInbox": "https://noop.url/federation/shared/inbox"},
    }

    serializer = serializers.ActorSerializer(data=payload)
    assert serializer.is_valid(raise_exception=True)
    actor = serializer.save()

    assert actor.fid == actor_url
    assert actor.url is None
    assert actor.inbox_url == payload["inbox"]
    assert actor.shared_inbox_url == payload["endpoints"]["sharedInbox"]
    assert actor.outbox_url == payload["outbox"]
    assert actor.following_url == payload["following"]
    assert actor.followers_url == payload["followers"]
    assert actor.followers_url == payload["followers"]
    assert actor.type == "https://www.w3.org/ns/activitystreams#Person"
    assert actor.preferred_username == payload["preferredUsername"]
    assert actor.name == payload["name"]
    assert actor.summary == payload["summary"]
    assert actor.fid == actor_url
    assert actor.manually_approves_followers is True
    assert actor.private_key is None
    assert actor.public_key == payload["publicKey"]["publicKeyPem"]
    assert actor.domain_id == "test.federation"


def test_actor_serializer_only_mandatory_field_from_ap(db):
    payload = {
        "@context": jsonld.get_default_context(),
        "id": "https://test.federation/user",
        "type": "Person",
        "following": "https://test.federation/user/following",
        "followers": "https://test.federation/user/followers",
        "inbox": "https://test.federation/user/inbox",
        "outbox": "https://test.federation/user/outbox",
        "preferredUsername": "user",
    }

    serializer = serializers.ActorSerializer(data=payload)
    assert serializer.is_valid(raise_exception=True)

    actor = serializer.build()

    assert actor.fid == payload["id"]
    assert actor.inbox_url == payload["inbox"]
    assert actor.outbox_url == payload["outbox"]
    assert actor.followers_url == payload["followers"]
    assert actor.following_url == payload["following"]
    assert actor.preferred_username == payload["preferredUsername"]
    assert actor.domain.pk == "test.federation"
    assert actor.type == "https://www.w3.org/ns/activitystreams#Person"
    assert actor.manually_approves_followers is None


def test_webfinger_serializer():
    expected = {
        "subject": "acct:service@localhost:5000",
        "links": [
            {
                "rel": "self",
                "href": "https://test.federation/federation/instance/actor",
                "type": "application/activity+json",
            },
            {
                'rel': 'http://webfinger.net/rel/profile-page',
                'href': "https://test.federation/federation/instance/actor",
                'type': 'text/html'
            }
        ],
        "aliases": ["https://test.federation/federation/instance/actor", "https://test.federation/federation/instance/actor"],
    }
    actor = models.Actor(
        fid=expected["links"][0]["href"],
        preferred_username="service",
        domain=models.Domain(pk="test.federation"),
    )
    serializer = serializers.ActorWebfingerSerializer(actor)

    assert serializer.data == expected


def test_activity_serializer_validate_recipients_empty(db):
    s = serializers.BaseActivitySerializer()

    with pytest.raises(serializers.serializers.ValidationError):
        s.validate_recipients({})

    with pytest.raises(serializers.serializers.ValidationError):
        s.validate_recipients({"to": []})

    with pytest.raises(serializers.serializers.ValidationError):
        s.validate_recipients({"cc": []})
