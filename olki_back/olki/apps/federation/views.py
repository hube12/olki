# pylint: skip-file

"""py
    Author: Eliot Berriot
    Project of origin: Funkwhale
    Commit of origin: 49769819265170a771e747b8696de6a27a100379
    Project licence: AGPLv3.0 or later
"""
import logging

from django import forms
from django.core import paginator
from django.http import HttpResponse
from django.urls import reverse
from rest_framework import exceptions, mixins, response, viewsets
from rest_framework.decorators import action
from rest_framework.parsers import JSONParser

from olki.apps.core import preferences
from . import utils, activity, models, webfinger, serializers, renderers, authentication

logger = logging.getLogger(__name__)


class FederationMixin:
    def dispatch(self, request, *args, **kwargs):
        if not preferences.get("federation__enabled"):
            return HttpResponse(status=405)
        return super().dispatch(request, *args, **kwargs)


class ActivityParser(JSONParser):
    media_type = "application/activity+json"


class SharedViewSet(FederationMixin, viewsets.GenericViewSet):
    permission_classes = []
    authentication_classes = [authentication.SignatureAuthentication]
    renderer_classes = renderers.get_ap_renderers()

    @action(methods=["post"], detail=False)
    def inbox(self, request, *args, **kwargs):
        if request.method.lower() == "post" and request.actor is None:
            raise exceptions.AuthenticationFailed(
                "You need a valid signature to send an activity"
            )
        if request.method.lower() == "post":
            activity.receive(activity=request.data, on_behalf_of=request.actor)
        return response.Response({}, status=200)


class ActorViewSet(FederationMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    lookup_field = "preferred_username"
    authentication_classes = [authentication.SignatureAuthentication]
    permission_classes = []
    parser_classes = [JSONParser, ActivityParser]
    renderer_classes = renderers.get_ap_renderers()
    queryset = models.Actor.objects.local().select_related("user")
    serializer_class = serializers.ActorSerializer

    @action(methods=["get", "post"], detail=True)
    def inbox(self, request, *args, **kwargs):
        actor = self.get_object()
        data = {}

        if request.method.lower() == "get":  # and request.actor and request.actor == actor:
            # TODO implement get: https://www.w3.org/TR/activitypub/#inbox
            conf = {
                "id": utils.full_url(
                    reverse(
                        "federation:actor-inbox",
                        kwargs={"preferred_username": actor.preferred_username},
                    )
                ),
                "orderedItems": actor.inbox_activities.order_by("-creation_date").values_list('payload', flat=True),
                "item_serializer": serializers.ActivitySerializer,
            }
            page = request.GET.get("page")
            if page is None:
                serializer = serializers.PaginatedOrderedCollectionSerializer(conf)
                data = serializer.data
            else:
                try:
                    page_number = int(page)
                except Exception:
                    return response.Response({"page": ["Invalid page number"]}, status=400)
                conf["page_size"] = preferences.get("federation__collection_page_size")
                p = paginator.Paginator(conf["items"], conf["page_size"])
                try:
                    page = p.page(page_number)
                    conf["page"] = page
                    serializer = serializers.OrderedCollectionPageSerializer(conf)
                    data = serializer.data
                except paginator.EmptyPage:
                    return response.Response(status=404)
            pass
        elif request.method.lower() == "post" and request.actor is None:
            raise exceptions.AuthenticationFailed(
                "You need a valid signature to send an activity"
            )
        elif request.method.lower() == "post":
            activity.receive(activity=request.data, on_behalf_of=request.actor)
        return response.Response(data, status=200)

    @action(methods=["get", "post"], detail=True)
    def outbox(self, request, *args, **kwargs):
        data = {}

        if request.method.lower() == "get":
            actor = self.get_object()

            conf = {
                "id": utils.full_url(
                    reverse(
                        "federation:actor-outbox",
                        kwargs={"preferred_username": actor.preferred_username},
                    )
                ),
                "orderedItems": actor.outbox_activities.order_by("-creation_date").values_list('payload', flat=True),
                "item_serializer": serializers.ActivitySerializer,
            }
            page = request.GET.get("page")
            if page is None:
                serializer = serializers.PaginatedOrderedCollectionSerializer(conf)
                data = serializer.data
            else:
                # if actor is requesting a specific page, we ensure library is public
                # or readable by the actor
                # if not has_library_access(request, actor):
                #     raise exceptions.AuthenticationFailed(
                #         "You do not have access to this library"
                #     )
                try:
                    page_number = int(page)
                except Exception:
                    return response.Response({"page": ["Invalid page number"]}, status=400)
                conf["page_size"] = preferences.get("federation__collection_page_size")
                p = paginator.Paginator(conf["orderedItems"], conf["page_size"])
                try:
                    page = p.page(page_number)
                    conf["page"] = page
                    serializer = serializers.OrderedCollectionPageSerializer(conf)
                    data = serializer.data
                except Exception as e:
                    logger.debug(e)
                except paginator.EmptyPage:
                    return response.Response(status=404)
        # TODO implement post
        return response.Response(data, status=200)

    @action(methods=["get"], detail=True)
    def followers(self, request, *args, **kwargs):
        actor = self.get_object()
        conf = {
            "id": utils.full_url(
                reverse(
                    "federation:actor-followers",
                    kwargs={"preferred_username": actor.preferred_username},
                )
            ),
            "orderedItems": actor.received_follows.filter(approved=True).order_by("-creation_date"),
            "item_serializer": serializers.FollowSerializer,
        }
        page = request.GET.get("page")

        if page is None:
            serializer = serializers.PaginatedOrderedCollectionSerializer(conf)
            data = serializer.data
        else:
            try:
                page_number = int(page)
            except Exception:
                return response.Response({"page": ["Invalid page number"]}, status=400)
            conf["page_size"] = preferences.get("federation__collection_page_size")
            p = paginator.Paginator(conf["items"], conf["page_size"])
            try:
                page = p.page(page_number)
                conf["page"] = page
                serializer = serializers.OrderedCollectionPageSerializer(conf)
                data = serializer.data
            except paginator.EmptyPage:
                return response.Response(status=404)
        return response.Response(data)

    @action(methods=["get"], detail=True)
    def following(self, request, *args, **kwargs):
        actor = self.get_object()
        conf = {
            "id": utils.full_url(
                reverse(
                    "federation:actor-following",
                    kwargs={"preferred_username": actor.preferred_username},
                )
            ),
            "orderedItems": actor.emitted_follows.filter(approved=True).order_by("-creation_date"),
            "item_serializer": serializers.FollowSerializer,
        }
        page = request.GET.get("page")

        if page is None:
            serializer = serializers.PaginatedOrderedCollectionSerializer(conf)
            data = serializer.data
        else:
            try:
                page_number = int(page)
            except Exception:
                return response.Response({"page": ["Invalid page number"]}, status=400)
            conf["page_size"] = preferences.get("federation__collection_page_size")
            p = paginator.Paginator(conf["items"], conf["page_size"])
            try:
                page = p.page(page_number)
                conf["page"] = page
                serializer = serializers.OrderedCollectionPageSerializer(conf)
                data = serializer.data
            except paginator.EmptyPage:
                return response.Response(status=404)
        return response.Response(data)


class WellKnownViewSet(viewsets.GenericViewSet):
    authentication_classes = []
    permission_classes = []
    renderer_classes = [renderers.JSONRenderer, renderers.WebfingerRenderer]

    @action(methods=["get"], detail=False)
    def nodeinfo(self, request, *args, **kwargs):
        # if not preferences.get("instance__nodeinfo_enabled"):
        #     return HttpResponse(status=404)
        data = {
            "links": [
                {
                    "rel": "http://nodeinfo.diaspora.software/ns/schema/1.0",
                    "href": utils.full_url(reverse("core:nodeinfo-1.0")),
                },
                {
                    "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
                    "href": utils.full_url(reverse("core:nodeinfo-2.0")),
                },
                {
                    "rel": "http://nodeinfo.diaspora.software/ns/schema/2.1",
                    "href": utils.full_url(reverse("core:nodeinfo-2.1")),
                }
            ]
        }
        return response.Response(data)

    @action(methods=["get"], detail=False)
    def webfinger(self, request, *args, **kwargs):
        if not preferences.get("federation__enabled"):
            return HttpResponse(status=405)
        try:
            resource_type, resource = webfinger.clean_resource(request.GET["resource"])
            cleaner = getattr(webfinger, "clean_{}".format(resource_type))
            result = cleaner(resource)
            handler = getattr(self, "handler_{}".format(resource_type))
            data = handler(result)
        except forms.ValidationError as e:
            return response.Response({"errors": {"resource": e.message}}, status=400)
        except KeyError:
            return response.Response(
                {"errors": {"resource": "This field is required"}}, status=400
            )

        return response.Response(data)

    def handler_acct(self, clean_result):
        username, hostname = clean_result

        try:
            actor = models.Actor.objects.local().get(preferred_username=username)
        except models.Actor.DoesNotExist:
            raise forms.ValidationError("Invalid username")

        return serializers.ActorWebfingerSerializer(actor).data
