### Federation module

`federation` module that allows *local and distant* ActivityStreams 
representation of actions on object (i.e.: creation, edition, comments 
on a corpus).

It defines generic `Domain`, `Activity` and `Actor` models, all needed
to represent activities on distant hosts in the ActivityPub realm (see
`models.py`). Views defined in this module allow to represent all parts 
of an actor's content, as well as Webfinger queries essential to discover
actors (see `views.py`).

Most of the logic related to AP is found in `routes.py`, `activity.py`
`tasks.py` and `serializers.py`. They define what activities and objects
are supported (mostly `routes.py`), and how they are handled inbound
and outbound (`activity.py` and of course `tasks.py` which is called by
the former for the final interactions with the outside).

#### How to use this module

Since urls are defined, include them in your Django app. After that, you
can link the actor model to your user model and generate its keys upon
creation. Don't forget to rotate them!

Models that you want to map to activities should define a
serializer inheriting from `federation.serializers.ObjectSerializer`.
After that, you need to register in `routes.py` the inbox and outbox
matcher functions for activity types related to that object.

Use activity.OutboxRouter.dispatch to send the activity and its object
as defined in the matcher (see examples in `routes.py`).
