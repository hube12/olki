from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from graphene import relay, String, Field, Union, Int
from graphene.types.generic import GenericScalar
from graphene_django.filter.fields import DjangoConnectionField
from graphene_django.types import DjangoObjectType

from olki.apps.core.graphql.types import CountableConnectionBase
from olki.apps.core.graphql.utils.mixins import PKMixin
from .models import Activity as ActivityModel
from .serializers import ActorSerializer
from olki.apps.corpus.models import Corpus


class Activity(PKMixin, DjangoObjectType):
    class Meta:
        model = ActivityModel
        filter_fields = {
            'type': ['exact'],
        }
        exclude_fields = [
            'payload',
        ]
        description = "This is an Activity as defined in the ActivityStreams Vocabulary."
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase

    class TargetUnion(Union):
        """
        A list of possible models that can be considered as targets.
        """
        class Meta:
            types = [Corpus]

    payload = GenericScalar()
    inReplyTo = GenericScalar()
    actor = Field('olki.apps.account.schema.Actor')

    def resolve_inReplyTo(self, info):
        inReplyTo = ActivityModel.objects.filter(payload__object__id=self.payload['object'].get('inReplyTo')).first()
        return inReplyTo and {
            'id': inReplyTo.id,
            'actor': ActorSerializer(inReplyTo.actor).data
        } or None

    def resolve_payload(self, info):
        return self.payload

    @classmethod
    def get_queryset(cls, queryset, info):
        return queryset


class ActivityQuery(object):
    # Activities related to a user
    # activities_user = DjangoFilterConnectionField(
    #     Activity,
    #     username=String(required=True),
    #     verbs=Argument(List(String), required=False,
    #                    description=_('A list of public Activity verbs you wish to limit your query to.')),
    #     description=_('List the Activities realated to a given User.')
    # )
    # Activities related to a corpus
    activities_corpus = DjangoConnectionField(
        Activity,
        pk=String(required=True, description=_('A Flax ID that is used as a primary key for the model.')),
        description=_('List the Activities related to a given Corpus.')
    )

    def resolve_activities_corpus(self, info, pk, **kwargs):
        return ActivityModel.objects.filter(target_id=pk).order_by('-creation_date')
