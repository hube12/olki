import datetime
from enum import Enum

from django.conf import settings
from django.contrib.auth.models import AbstractUser, BaseUserManager, models
from django.utils.translation import ugettext_lazy as _
from flax_id.django.fields import FlaxId
from rolepermissions.roles import assign_role, get_user_roles

from olki.apps.core.models import TimestampedModel
from olki.apps.federation import keys
from olki.apps.federation.models import Actor


class InactiveReasonChoice(Enum):
    pendingApproval = "PA"
    pendingSelfVerification = "PV"
    selfDisabled = "SD"
    quarantined = "QU"
    banned = "BA"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class LocalityBackendChoice(Enum):
    LOCAL = "LO"
    LDAP = "LD"
    SAML = "SA"  # not yet supported
    CAS = "CA"  # not yet supported

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class UserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        actorname = extra_fields.pop('actorname', None)
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        if actorname:
            user.set_actor(actorname)

        if extra_fields.get('is_staff'):
            assign_role(user, 'moderator')
        if extra_fields.get('is_superuser'):
            assign_role(user, 'administrator')

        if not extra_fields.get('is_staff'):
            assign_role(user, 'untrusted_user')
        else:
            assign_role(user, 'trusted_user')
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)

        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """
        Creates and saves a superuser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser, TimestampedModel):
    """
    A User is the main entity representing an account on OLKi.
    It is not meant to be federated, as federation.Actor is meant
    for this.

    # Create a User
    >>> user = User.objects.create_user(email="test@example.com", password="test", actorname="test")
    >>> user.actor.username
    'test'
    >>> user.is_setup
    True
    >>> user.roles
    ['untrusted_user']

    # Create a User with superuser attributions (administrator)
    >>> superuser = User.objects.create_superuser(email="superuser@example.com", password="superuser", actorname="superuser")
    >>> superuser.is_superuser
    True
    >>> superuser.roles
    ['administrator', 'moderator', 'trusted_user']
    """
    # Flax ID field
    # Generates unambiguous, unique distributed ids, with a canonical representation Base64-encoded,
    # URL safe string (16 characters).
    #
    # An ID has 96 bits, of which first 40 bits are the timestamp, and the next 56 bits are random.
    # This gives 2^56 possible unique ids per millisecond.
    #
    # The number 96 was chosen on the following grounds:
    #
    #    It is divisible by 6, so can be rendered as base64-encoded string without padding
    #    It is divisible by 8, so it can be represented as a byte array
    #    It leaves enough room for the random part
    #
    # See http://yellerapp.com/posts/2015-02-09-flake-ids.html for a more thorough explanation.
    #
    # Based on `flax-id`: https://github.com/ergeon/python-flax-id
    id = FlaxId(primary_key=True, help_text=_('A Flax ID that is used as a primary key for the model.'))

    locality = models.CharField(
        _('locality'),
        editable=False,
        choices=LocalityBackendChoice.choices(),
        default=LocalityBackendChoice.LOCAL.value,  # local by default
        max_length=2,
        help_text=_(
            'Designates whether a user is local or coming from LDAP,'
            'CAS, SAML, or any other authorization backend.'
        )
    )
    is_inactive_reason = models.CharField(
        _('inactive reason'),
        default=None,
        null=True,
        choices=InactiveReasonChoice.choices(),
        max_length=2,
        help_text=_(
            'Designates the reason(s) a user account is inactive'
            'Some reasons are naturally occuring on account creation,'
            'some are decided by the moderation - they are then cumulative.'
        ),
    )
    email = models.EmailField(
        _('email address'),
        unique=True
    )
    last_activity = models.DateTimeField(default=None, null=True, blank=True)


    USERNAME_FIELD = 'email'  # extremely important to note. transforms authenticate(username=… in authenticate(email=email, password=password)
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        # these permissions get added to the Django default_permissions
        permissions = [
            ("list_users", _("Can list all users")),
            ("change_is_active", _("Can change activity statuses and their reasons")),
            ("change_moderation_status", _("Can moderate user activities")),
        ]

    @property
    def is_setup(self):
        return self.has_actor

    def set_actor(self, actorname):
        self.username = actorname
        self.save()
        args = Actor.get_actor_data(actorname)
        private, public = keys.get_key_pair()
        args["private_key"] = private.decode("utf-8")
        args["public_key"] = public.decode("utf-8")
        return Actor.objects.create(user=self, **args)

    @property
    def roles(self):
        return [role.get_name() for role in get_user_roles(self)]

    def get_activity_url(self):
        return settings.INSTANCE_HOSTNAME + "/@{}".format(self.username)

    def full_username(self):
        return "{}@{}".format(self.username, settings.INSTANCE_HOSTNAME)

    def record_activity(self):
        """
        Simply update the last_activity field if current value is too old
        than a threshold. This is useful to keep a track of inactive accounts.
        """
        current = self.last_activity
        delay = 60 * 15  # fifteen minutes
        now = datetime.timezone.now()

        if current is None or current < now - datetime.timedelta(seconds=delay):
            self.last_activity = now
            self.save(update_fields=["last_activity"])

    def get_absolute_url(self):
        return "/@{}".format(self.actor.preferred_username)

    def __str__(self):
        if self.profile.name:
            return self.profile.name
        elif hasattr(self, 'actor'):
            return self.actor.username
        else:
            return ''


User._meta.get_field('email')._unique = True
User._meta.get_field('username')._unique = False
