from django.contrib.auth.tokens import default_token_generator
from django import forms
from djoser.conf import settings as djoser_settings
from djoser.utils import decode_uid
from django.utils.translation import ugettext_lazy as _

import graphene
import graphql_jwt
from graphql_jwt.decorators import login_required
from graphene_django.forms.mutation import DjangoFormMutation, ErrorType

from rest_framework_jwt.serializers import (
    JSONWebTokenSerializer,
    RefreshJSONWebTokenSerializer,
    VerifyJSONWebTokenSerializer
)

from .models import User as UserModel
from .schema import User
from .serializers import PasswordResetConfirmRetypeSerializer
from .utils import send_activation_email, send_password_reset_email


class Register(graphene.Mutation):
    """
    Mutation to register a user
    """
    class Arguments:
        email = graphene.String(required=True)
        username = graphene.String()  # a user can register with no actor/username defined yet
        password = graphene.String(required=True)
        password_repeat = graphene.String(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    def mutate(self, info, email, password, password_repeat, **kwargs):
        if password == password_repeat:
            try:
                user = UserModel.objects.create(
                    email=email,
                    is_active=False
                    )
                user.set_password(password)
                user.save()

                if len(kwargs.get('username')) > 3:
                    user.set_actor(kwargs.get('username'))
                if djoser_settings.get('SEND_ACTIVATION_EMAIL'):
                    send_activation_email(user, info.context)

                return Register(success=bool(user.id))
            # TODO: specify exception
            except Exception:
                errors = ["email", "Email already registered."]
                return Register(success=False, errors=errors)
        errors = ["password", "Passwords don't match."]
        return Register(success=False, errors=errors)


class Activate(graphene.Mutation):
    """
    Mutation to activate a user's registration
    """
    class Arguments:
        token = graphene.String(required=True)
        uid = graphene.String(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    def mutate(self, info, token, uid):

        try:
            uid = decode_uid(uid)
            user = UserModel.objects.get(pk=uid)
            if not default_token_generator.check_token(user, token):
                return Activate(success=False, errors=['stale token'])
                pass
            user.is_active = True
            user.save()
            return Activate(success=True, errors=None)

        except Exception:
            return Activate(success=False, errors=['unknown user'])


class Login(graphene.Mutation):
    """
    Mutation to login a user
    """
    class Arguments:
        email = graphene.String(required=True)
        password = graphene.String(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    token = graphene.String()
    user = graphene.Field(User)

    def mutate(self, info, email, password):
        user = {'email': email, 'password': password}
        serializer = JSONWebTokenSerializer(data=user)
        if serializer.is_valid():
            token = serializer.object['token']
            user = serializer.object['user']
            return Login(success=True, user=user, token=token, errors=None)
        else:
            return Login(
                success=False,
                token=None,
                errors=['email', 'Unable to login with provided credentials.']
                )


class VerifyToken(graphene.Mutation):
    """
    Mutation to verify a user is authenticated
    """
    class Arguments:
        token = graphene.String(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    def mutate(self, info, token):
        serializer = VerifyJSONWebTokenSerializer(data={'token': token})
        if serializer.is_valid():
            return VerifyToken(
                success=True,
                errors=None
                )
        else:
            return RefreshToken(
                success=False,
                errors=['token', 'Unable to verify the provided token.']
                )


class RefreshToken(graphene.Mutation):
    """
    Mutation to reauthenticate a user
    """
    class Arguments:
        token = graphene.String(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    token = graphene.String()

    def mutate(self, info, token):
        serializer = RefreshJSONWebTokenSerializer(data={'token': token})
        if serializer.is_valid():
            return RefreshToken(
                success=True,
                token=serializer.object['token'],
                errors=None
                )
        else:
            return RefreshToken(
                success=False,
                token=None,
                errors=['email', 'Unable to login with provided credentials.']
                )


class ChangePassword(graphene.Mutation):
    """
    Mutation for changing password
    """

    class Arguments:
        new_password = graphene.String(required=True)
        re_new_password = graphene.String(required=True)

    success = graphene.Boolean()

    @login_required
    def mutate(self, info, new_password, re_new_password):
        try:
            if new_password != re_new_password:
                raise Exception
            user = info.context.user
            user.set_password(new_password)
            user.save()
            return ChangePassword(success=True)
        except Exception:
            return ChangePassword(success=False)


class ResetPassword(graphene.Mutation):
    """
    Mutation for requesting a password reset email
    """

    class Arguments:
        email = graphene.String(required=True)

    success = graphene.Boolean()

    def mutate(self, info, email):
        try:
            user = UserModel.objects.get(email=email)
            send_password_reset_email(user, info.context)
            return ResetPassword(success=True)
        except Exception:
            return ResetPassword(success=True)


class ResetPasswordConfirm(graphene.Mutation):
    """
    Mutation for requesting a password reset email
    """

    class Arguments:
        uid = graphene.String(required=True)
        token = graphene.String(required=True)
        email = graphene.String(required=True)
        new_password = graphene.String(required=True)
        re_new_password = graphene.String(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    def mutate(self, info, uid, token, email, new_password, re_new_password):
        serializer = PasswordResetConfirmRetypeSerializer(data={
            'uid': uid,
            'token': token,
            'email': email,
            'new_password': new_password,
            're_new_password': re_new_password,
        })
        if serializer.is_valid():
            serializer.user.set_password(serializer.data['new_password'])
            serializer.user.save()
            return ResetPasswordConfirm(success=True, errors=None)
        else:
            return ResetPasswordConfirm(
                success=False, errors=[serializer.errors])


class DeleteAccount(graphene.Mutation):
    """
    Mutation to delete an account
    """
    class Arguments:
        password = graphene.String(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    def mutate(self, info, password):
        is_authenticated = info.context.user.is_authenticated
        if not is_authenticated:
            errors = ['unauthenticated']
        elif not info.context.user.check_password(password):
            errors = ['wrong password']
        else:
            info.context.user.delete()
            return DeleteAccount(success=True)
        return DeleteAccount(success=False, errors=errors)


class ActorForm(forms.Form):
    username = forms.CharField(label="Actor username", max_length=30)


class CreateActor(DjangoFormMutation):
    """
    Mutation to initialize an account's actor
    """
    class Meta:
        form_class = ActorForm

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        user = info.context.user

        form = cls.get_form(root, info, **input)

        if form.is_valid() and not user.is_setup:
            user.set_actor(input.get('username'))
            return cls(errors=[])
        else:
            if user.is_setup:
                form.add_error(field='username', error=_('Actor already setup.'))

            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(errors=errors)


class UserMutations(graphene.AbstractType):
    # account-related
    activateAccount = Activate.Field()
    registerAccount = Register.Field()
    deleteAccount = DeleteAccount.Field()
    changePassword = ChangePassword.Field()
    resetPassword = ResetPassword.Field()
    resetPasswordConfirm = ResetPasswordConfirm.Field()
    createActor = CreateActor.Field()
    # auth-related
    login = Login.Field()
    verifyToken = graphql_jwt.Verify.Field()
    refreshToken = graphql_jwt.Refresh.Field()
    revokeToken = graphql_jwt.Revoke.Field()
