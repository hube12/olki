from graphene import relay, ObjectType, Field, Boolean, String
from graphene_django.filter.fields import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
from rolepermissions.checkers import has_role

from olki.apps.core.graphql.interfaces import AuthorInterface
from olki.apps.federation.models import Actor as ActorModel
from olki.apps.profile.schema import Profile
from .models import User as UserModel
from .roles import Administrator, Moderator


class Actor(DjangoObjectType):
    """
    Actor Node
    """
    cost = 2

    class Meta:
        model = ActorModel
        description = "This is an Actor."
        except_fields = []
        interfaces = (relay.Node,)

    profile_url = String()
    full_username = String()

    @classmethod
    def get_node(cls, id, info):
        try:
            actor = cls._meta.model.objects.get(id=id)
        except cls._meta.model.DoesNotExist:
            return None
        return actor


class User(DjangoObjectType):
    """
    User Node
    """
    cost = 7

    class Meta:
        model = UserModel
        filter_fields = {
            'email': ['exact'],
            'profile__name': ['exact', 'icontains', 'istartswith'],
            'actor__preferred_username': ['exact', 'icontains', 'istartswith'],
        }
        exclude_fields = (
            'password',
            'locality',
            'is_inactive_reason',
        )
        description = "This is a User. It is meant to hold basic authentication information and delegate the rest to Profile and Actor models."
        interfaces = (relay.Node, AuthorInterface, )

    profile = Field(Profile)
    actor = Field('olki.apps.account.schema.Actor')
    is_setup = Boolean()

    def resolve_email(self, info):
        if info.context.user.is_staff or (info.context.user.id == self.id):
            return self.email
        return ''

    def resolve_name(self, info):
        return str(self)


class UserQuery(object):
    """
    what is an abstract type?
    http://docs.graphene-python.org/en/latest/types/abstracttypes/
    """
    user = Field(User, username=String(required=True))
    users = DjangoFilterConnectionField(User)

    def resolve_user(self, info, username, **kwargs):
        qs = UserModel.objects.filter(actor__preferred_username__exact=username)
        if info.context.user == qs.get():
            return qs.get()
        return qs.exclude(profile__private=True).get()

    def resolve_users(self, info, **kwargs):
        if info.context.user.is_authenticated and has_role(info.context.user, [Administrator, Moderator]):
            return UserModel.objects.all()
        else:
            return UserModel.objects.none()


class ActorQuery(ObjectType):
    has_actor = Field(Boolean, username=String(required=True))

    def resolve_has_actor(self, info, username):
        return ActorModel.objects.filter(username=username).exists()


class Viewer(ObjectType):
    user = Field(User, )

    def resolve_user(self, info, **kwargs):
        if info.context.user.is_authenticated:
            return info.context.user
        return None
