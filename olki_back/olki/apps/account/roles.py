from rolepermissions.roles import AbstractUserRole


class Administrator(AbstractUserRole):
    available_permissions = {
        'list_users': True,
        'add_corpus_freely': True,
        'comment_corpus': True,
        "moderate_corpus": True,
    }


class Moderator(AbstractUserRole):
    available_permissions = {
        'list_users': True,
        'add_corpus_freely': True,
        'comment_corpus': True,
        "moderate_corpus": True,
    }


class TrustedUser(AbstractUserRole):
    available_permissions = {
        'list_users': False,
        'add_corpus_freely': True,
        'comment_corpus': True,
    }


class UntrustedUser(AbstractUserRole):
    available_permissions = {
        'list_users': False,
        'add_corpus_freely': False,
        'comment_corpus': True
    }
