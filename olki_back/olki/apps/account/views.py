from django.conf import settings
from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Atom1Feed

from . import models


class rss(Feed):
    description = "Updates on changes and additions to the user's corpora."

    def get_object(self, request, user_id):
        return models.User.objects.get(username=user_id)

    def title(self, obj):
        return "{} on {} - Corpora".format(obj.__str__, settings.SITE_NAME)

    def link(self, obj):
        return "{}/@{}.rss".format(settings.INSTANCE_URL, obj.username)

    def items(self, obj):
        return obj.actor.corpora_owned.public.all()[:40]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description


class atom(rss):
    feed_type = Atom1Feed
    subtitle = rss.description

    def link(self, obj):
        return "{}/@{}.atom".format(settings.INSTANCE_URL, obj.username)
