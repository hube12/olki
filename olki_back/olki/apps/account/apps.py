from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'olki.apps.account'

    def ready(self):
        pass