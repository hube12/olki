from django.core.management.base import BaseCommand
from rolepermissions.roles import remove_role, assign_role

from olki.apps.account.models import User as UserModel


class Command(BaseCommand):
    help = 'Modify a local user.'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('email', type=str)

        # Named (optional) arguments
        parser.add_argument(
            '--role',
            type=str,
            help='Modify the role of the user to the given role, stripping the user of rights if the new role is below the current role.',
        )

    def handle(self, *args, **options):
        user = UserModel.objects.get(
            email=options['email']
        )

        if options['role'] and options['role'] in ['admin', 'administrator', 'mod', 'moderator', 'user']:
            if options['role'] == 'admin' or options['role'] == 'administrator':
                user.is_staff = True
                user.is_superuser = True
                assign_role(user, 'administrator')
            if options['role'] == 'mod' or options['role'] == 'moderator':
                user.is_staff = True
                user.is_superuser = False
                remove_role(user, 'administrator')
                assign_role(user, 'moderator')
            if options['role'] == 'user':
                user.is_staff = False
                user.is_superuser = False
                remove_role(user, 'moderator')
                remove_role(user, 'administrator')
            user.save()
        else:
            return

        self.stdout.write(self.style.SUCCESS(
            'Successfully modified user {} to be {}'.format(
                options['email'],
                options['role']
            )
        ))
