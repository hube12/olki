from django.core.management.base import BaseCommand

from olki.apps.account.models import User as UserModel


class Command(BaseCommand):
    help = 'Add a new local user. By default sends a command to activate the account.'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('email', type=str)
        parser.add_argument('password', type=str)

    def handle(self, *args, **options):
        user = UserModel.objects.create(
            email=options['email']
        )
        user.set_password(options['password'])
        user.save()
        self.stdout.write(self.style.SUCCESS('Successfully created user {}'.format(options['email'])))
