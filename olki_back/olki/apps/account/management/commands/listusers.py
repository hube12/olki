from tabulate import tabulate
from django.utils import formats
from django.core.management.base import BaseCommand
from django.template.defaultfilters import filesizeformat

from olki.apps.account.models import User as UserModel


class Command(BaseCommand):
    help = 'Lists local user accounts.'

    def add_arguments(self, parser):
        # Optional argument
        pass

    def handle(self, *args, **options):
        users = [
            [
                user.email,
                formats.date_format(user.created_at, "SHORT_DATETIME_FORMAT"),
                user.is_active,
                user.roles,
                (user.actor.username if hasattr(user, 'actor') else ''),
                len(user.actor.corpora_owned.all()),
                filesizeformat(sum([corpus.filesize for corpus in user.actor.corpora_owned.all()]))
            ]
            for user in UserModel.objects.all()
        ]
        print(tabulate(users, headers=['Email', 'Created At', 'Activated', 'Roles', 'Actor', 'Corpora', 'Size on disk']))
