from actstream.models import Action as ActionModel
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django_filters import FilterSet, BooleanFilter
from graphene import relay, String, Union, Field, Argument, List, JSONString, Boolean
from graphene_django.filter.fields import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from olki.apps.core.graphql.types import CountableConnectionBase
from olki.apps.corpus.schema import Corpus, CorpusModel
from olki.apps.federation.models import Actor as ActorModel
from olki.apps.note.schema import Note, NoteModel

NoteContentType = ContentType.objects.get_for_model(NoteModel)


class ActionTargetUnion(Union):
    """
    A list of possible models that can be considered as targets.
    """
    class Meta:
        types = [Corpus]


class ActionObjectUnion(Union):
    """
    A list of possible models that can be considered as objects.
    """
    class Meta:
        types = [Note]


class Action(DjangoObjectType):
    """
    Action Node, following basic [concepts](https://django-activity-stream.readthedocs.io/en/latest/concepts.html).
    Verbs are relative to the https://www.w3.org/ns/activitystreams# namespace, and otherwise absolute IRIs.
    """
    cost = 12

    class Meta:
        model = ActionModel
        filter_fields = {
            'verb': ['exact'],
        }
        exclude_fields = [
            'public'
        ]
        description = "This is an Action as defined in Django Activity Streams."
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase

    actor = Field('olki.apps.account.schema.Actor')
    actor_id = String()
    target = Field(ActionTargetUnion)
    object = Field(ActionObjectUnion)
    object_deleted = Boolean()
    description = Field(JSONString, description=_('A field used to store annex data, for instance describing the fields changed on the target during an Update.'))

    def resolve_actor_id(self, info):
        return self.actor.username

    def resolve_object(self, info):
        return self.action_object if self.action_object and self.action_object.public else None

    def resolve_object_deleted(self, info):
        return True if self.action_object and not self.action_object.public else False

    @classmethod
    def get_queryset(cls, queryset, info):
        return queryset.filter(public=True)


class ActionFilter(FilterSet):
    class Meta:
        model = ActionModel
        fields = ()

    # restriction booleans
    only_comments = BooleanFilter(field_name='comments', exclude=False, method='filter_only_comments')
    only_modifications = BooleanFilter(field_name='modifications', exclude=False, method='filter_only_modifications')

    def filter_only_comments(self, queryset, name, value):
        return queryset.filter(Q(action_object_content_type=NoteContentType)) if value is True else queryset

    def filter_only_modifications(self, queryset, name, value):
        return queryset.filter(~Q(action_object_content_type=NoteContentType)) if value is True else queryset

    @property
    def qs(self):
        # The query context can be found in self.request.
        return super(ActionFilter, self).qs  # default value but better be explicit


class ActionQuery(object):
    # Actions related to a user
    actions_user = DjangoFilterConnectionField(
        Action,
        username=String(required=True),
        verbs=Argument(List(String), required=False,
                       description=_('A list of public Action verbs you wish to limit your query to.')),
        filterset_class=ActionFilter,
        description=_('List the Actions taken related to a given User.')
    )
    # Actions related to a corpus
    actions_corpus = DjangoFilterConnectionField(
        Action,
        pk=String(required=True, description=_('A Flax ID that is used as a primary key for the model.')),
        verbs=Argument(List(String), required=False,
                       description=_('A list of public Action verbs you wish to limit your query to.')),
        filterset_class=ActionFilter,
        description=_('List the Actions taken related to a given Corpus.')
    )

    def resolve_actions_user(self, info, username, **kwargs):
        actor = ActorModel.objects.filter(user__isnull=False).filter(preferred_username__exact=username).get()
        public_user_verbs = ['Create', 'Import', 'Add', 'Update']
        ## Non-aggregated implementation
        from actstream.models import any_stream
        return any_stream(actor)\
            .filter(verb__in=public_user_verbs)\
            .filter(public=True)

    def resolve_actions_corpus(self, info, pk, **kwargs):
        corpus = CorpusModel.objects.filter(pk__exact=pk).get()
        public_corpus_verbs = ['Create', 'Import', 'Add', 'Update']
        ## Non-aggregated implementation
        from actstream.models import any_stream
        return any_stream(corpus)\
            .filter(verb__in=public_corpus_verbs)\
            .filter(
                ~Q(action_object_content_type=NoteContentType)  # ~ is to negate the Q filter
                |  # union
                Q(actions_with_note_note_as_action_object__public=True)
            )
