### Action module

`action` module that allows *local* ActivityStreams representation of
actions on object (i.e.: creation, edition, comments on a corpus).

This is a temporary implementation meant to be replaced by the federated
Activity framework contained in the `federation` module.