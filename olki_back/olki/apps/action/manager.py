from typing import List
from functools import reduce
import actstream
from actstream.managers import ActionManager, stream

# https://www.w3.org/TR/activitystreams-vocabulary/#activity-types
ACTIVITY_TYPES: List[str] = [
    "Accept",
    "Add",
    "Announce",
    "Arrive",
    "Block",
    "Create",
    "Delete",
    "Dislike",
    "Flag",
    "Follow",
    "Ignore",
    "Invite",
    "Join",
    "Leave",
    "Like",
    "Listen",
    "Move",
    "Offer",
    "Question",
    "Reject",
    "Read",
    "Remove",
    "TentativeReject",
    "TentativeAccept",
    "Travel",
    "Undo",
    "Update",
    "View",
]


class ActivityStreamManager(ActionManager):
    def __aggregated(self, obj, stream_type, verbs=None):
        if verbs is None:
            verbs = ACTIVITY_TYPES
        qs = getattr(actstream.models, "%s" % stream_type)(obj)\
            .filter(verb__in=verbs)\
            .filter(public=True)

        def aggregate(prev, curr):
            if not prev:
                return [curr]
            if prev[-1].verb == curr.verb and curr.verb == 'Update':
                if (not hasattr(prev[-1], 'object') or not hasattr(curr, 'object')) and hasattr(prev[-1], 'target') and hasattr(curr, 'target') and prev[-1].target.pk is curr.target.pk:
                    return prev
                if (not hasattr(prev[-1], 'target') or not hasattr(curr, 'target')) and hasattr(prev[-1], 'object') and hasattr(curr, 'object') and prev[-1].object.pk is curr.object.pk:
                    return prev
            else:
                return prev + [curr]

        return reduce(aggregate, qs, [])

    def any_aggregated(self, obj, **kwargs):
        return self.__aggregated(obj, 'any_stream', **kwargs)

    def actor_aggregated(self, obj, **kwargs):
        return self.__aggregated(obj, 'actor_stream', **kwargs)

    def target_aggregated(self, obj, **kwargs):
        return self.__aggregated(obj, 'target_stream', **kwargs)

    def action_object_aggregated(self, obj, **kwargs):
        return self.__aggregated(obj, 'action_object_stream', **kwargs)
