"""
This is a module expected by olki.apps.core.middleware.HeaderMiddleware

It registers pattern matchings not served by Django but by the frontend
SPA. This allows modifying the response in case a headless client queries
a route normally served by the SPA with no processing by Django.

Note: instead of views, routes in urlpatterns register functions that
take a request and a response in argument, to modify the response.
"""

from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import path
from django.shortcuts import render

from .models import Corpus
from olki.apps.federation.utils import is_federation_accept
from olki.apps.federation.serializers import CorpusSerializer


def none(request, response):
    pass


def index(request, response):
    response = render(request, 'pages/_index.html')
    response['Link'] = ','.join([
        '<{}/corpus/rss.xml>; rel="alternate"; type="application/rss+xml"'.format(settings.INSTANCE_URL),
        '<{}/corpus/atom.xml>; rel="alternate"; type="application/atom+xml"'.format(settings.INSTANCE_URL)
    ])
    return response


def corpus(request, response, corpus_id):
    # we have an activitypub object request
    if is_federation_accept(request.headers['Accept']):
        c = get_object_or_404(Corpus, pk=corpus_id)
        return JsonResponse(c.to_json(), content_type="application/activity+json; charset=utf-8")  # FIXME: return Corpus representation in JSON-LD

    # we have a normal web page
    if '.' not in corpus_id:
        c = get_object_or_404(Corpus, pk=corpus_id)
        response = render(request, 'pages/corpus.html', {
            'corpus': c,
            'corpus_fid': CorpusSerializer(c).data.get('id')
        })
        response['Link'] = ','.join([
            '<{}/corpus/{}.bib>; rel="alternate"; type="application/x-bibtex"'.format(settings.INSTANCE_URL, corpus_id),
            '<{}/corpus/{}.ris>; rel="alternate"; type="application/x-research-info-systems"'.format(settings.INSTANCE_URL, corpus_id),
            '<{}/corpus/{}>; rel="alternate"; type="application/activity+json"'.format(settings.INSTANCE_URL, corpus_id)
        ])

    return response


urlpatterns = [
    path('', index),
    path('corpus/<str:corpus_id>', corpus, name="corpus"),
]
