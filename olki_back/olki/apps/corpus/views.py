import os
import tarfile
import zipfile
from io import BytesIO
from wsgiref.util import FileWrapper

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.http import StreamingHttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.utils.feedgenerator import Atom1Feed
from django.utils.text import slugify
from rest_framework import mixins, viewsets
from rest_framework.decorators import action

from olki.apps.core.renderers import OLKiXMLRenderer
from . import models, serializers

"""
Corpus views
"""


def archive(request, corpus_id, archive_format):
    corpus = get_object_or_404(models.Corpus, pk=corpus_id)
    corpus_media_directory = '{0}/corpus/{1}/'.format(settings.MEDIA_ROOT, corpus_id)

    # note: the zip only exist in memory as you add to it
    buffer = BytesIO()
    ext = 'zip'
    content_type = 'application/zip'

    if archive_format == 'tar':
        ext = 'tar'
        content_type = 'application/x-tar'
        with tarfile.open(mode="w", fileobj=buffer) as _archive:
            for fname in os.listdir(corpus_media_directory):
                _archive.add(os.path.join(corpus_media_directory, fname), fname)
    else:
        with zipfile.ZipFile(buffer, "w", zipfile.ZIP_STORED) as _archive:
            for fname in os.listdir(corpus_media_directory):
                _archive.write(os.path.join(corpus_media_directory, fname), fname)

    response = StreamingHttpResponse(FileWrapper(buffer), content_type=content_type)
    response['Content-Disposition'] = 'attachment; filename={}.{}'.format(slugify(corpus.title), ext)
    buffer.seek(0)
    return response


def zip(request, corpus_id):
    return archive(request, corpus_id, archive_format='zip')


def tar(request, corpus_id):
    return archive(request, corpus_id, archive_format='tar')


class CorpusViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    lookup_field = "id"
    lookup_url_kwarg = "corpus_id"
    permission_classes = []
    queryset = models.Corpus.public

    @action(methods=["get"], detail=True)
    def bib(self, request, *args, **kwargs):
        corpus = self.get_object()
        return render(
            request,
            'corpus/citation.bib',
            {
                "id": corpus.id,
                "data": serializers.BibTeXSerializer(corpus).data
            },
            content_type='application/x-bibtex'
        )

    @action(methods=["get"], detail=True)
    def ris(self, request, *args, **kwargs):
        corpus = self.get_object()
        return render(
            request,
            'corpus/citation.ris',
            {
                "data": serializers.RISSerializer(corpus).data
            },
            content_type='application/x-research-info-systems'
        )

    @action(methods=["get"], detail=True)
    def endnote(self, request, *args, **kwargs):
        corpus = self.get_object()
        return render(
            request,
            'corpus/citation.endnote',
            {
                "data": serializers.EndNoteSerializer(corpus).data
            },
            content_type='application/x-bibliographic'
        )

    @action(methods=["get"], detail=True)
    def dublin_core(self, request, *args, **kwargs):
        corpus = self.get_object()
        return render(
            request,
            'corpus/citation.dc',
            {
                "id": corpus.id,
                "data": OLKiXMLRenderer().render(serializers.DublinCoreRecordSerializer(corpus).data)
            },
            content_type='application/dc+xml'
        )


bib = CorpusViewSet.as_view(actions={'get': 'bib'})
ris = CorpusViewSet.as_view(actions={'get': 'ris'})
endnote = CorpusViewSet.as_view(actions={'get': 'endnote'})
dublin_core = CorpusViewSet.as_view(actions={'get': 'dublin_core'})


class rss(Feed):
    title = "{} - Corpora".format(settings.SITE_NAME)
    link = "{}/corpus/rss/".format(settings.INSTANCE_URL)
    description = "Updates on changes and additions to the repository corpora."

    def items(self):
        return models.Corpus.public.all()[:40]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description


class atom(rss):
    feed_type = Atom1Feed
    subtitle = rss.description


"""
Federated views from the Corpus views

They are used in the 'url' field of a Corpus federated object.
They are alternative representations of that object.
"""
FederatedRepresentationsOfACorpus = [
    {
        "named_url": 'archive-zip',
        "mediaType": "application/zip",
    },
    {
        "named_url": 'archive-tar',
        "mediaType": "application/x-tar",
    },
    {
        "named_url": 'citation-bib',
        "mediaType": "application/x-bibtex",
    },
    {
        "named_url": 'citation-ris',
        "mediaType": "application/x-research-info-systems",
    },
    {
        "named_url": 'citation-endnote',
        "mediaType": "application/x-bibliographic",
    },
    {
        "named_url": 'citation-dublin_core',
        "type": "Metadata",
        "mediaType": "application/xml, application/dc+xml",
        "metadataPrefix": "oai_dc",
        "metadataNamespace": "http://www.openarchives.org/OAI/2.0/oai_dc/",
        "schema": "http://www.openarchives.org/OAI/2.0/oai_dc.xsd"
    },
]
