from collections import defaultdict

from django.apps import apps
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django_filters import FilterSet, OrderingFilter, BooleanFilter, CharFilter, BaseInFilter
from graphene import relay, String, Field, List, Int, Boolean
from graphene_django.filter.fields import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
from promise import Promise
from promise.dataloader import DataLoader
from rolepermissions.checkers import has_role

from olki.apps.account.roles import Administrator, Moderator
from olki.apps.core.graphql.interfaces import AuthorInterface, ActionTargetInterface
from olki.apps.core.graphql.types import CountableConnectionBase
from olki.apps.core.graphql.utils.mixins import PKMixin
from olki.apps.federation.models import Actor as ActorModel
from olki.apps.note.models import Note as NoteModel
from .models import Corpus as CorpusModel, CorpusAuthor as CorpusAuthorModel, \
    CorpusAuthorMembership as CorpusAuthorMembershipModel, \
    CorpusTag as CorpusTagModel, \
    CorpusFile as CorpusFileModel


class CorpusAuthor(DjangoObjectType):
    cost = 3

    class Meta:
        interfaces = (AuthorInterface, )
        model = CorpusAuthorMembershipModel
        filter_fields = {
            'author__name': ['exact', 'icontains', 'istartswith'],
            'author__orcid': ['exact'],
            'affiliation': ['exact', 'icontains', 'istartswith'],
            'role': ['exact']
        }
        only_fields = [
            'email',
            'affiliation',
            'role',
        ]
        interfaces = (relay.Node,)

    name = String()
    orcid = String()

    def resolve_name(self, info):
        return self.author.name

    def resolve_orcid(self, info):
        return self.author.orcid


class CorpusTag(DjangoObjectType):
    cost = 2

    class Meta:
        name = 'Tag'
        model = CorpusTagModel
        description = 'A tag of a Corpus.'
        only_fields = (
            'code',
            'name',
            'description',
        )


class CorpusFile(DjangoObjectType):
    url = String()
    size = Int()

    class Meta:
        name = 'File'
        model = CorpusFileModel
        description = 'A file of a Corpus.'
        only_fields = (
            'name',
            'filetype',
            'hash',
            'description',
        )

    def resolve_url(self, info):
        return self.file

    def resolve_size(self, info):
        return self.filesize


class CommentsByCorpusIdLoader(DataLoader):
    """
    Optimized way to retrieve instances of foreign models having
    a ForeignKey to the Corpus instance.
    Current implementation for the Note model (comments to the Corpus).
    """
    @staticmethod
    def batch_load_fn(keys):
        comments_by_corpus_ids = defaultdict(list)
        for comment in NoteModel.objects.filter(corpus_id__in=keys).iterator():
            comments_by_corpus_ids[comment.corpus.id].append(comment)
        return Promise.resolve([
            comments_by_corpus_ids.get(corpus_id, [])
            for corpus_id in keys
        ])


class TagsByCorpusIdLoader(DataLoader):
    """
    Optimized way to retrieve instances of foreign models having
    a ForeignKey to the Corpus instance.
    Current implementation for the through CorpusTag model (tags of the Corpus).
    """
    def batch_load_fn(self, keys):
        tags_by_corpus_ids = defaultdict(list)
        for m2m in CorpusModel.tags.through.objects.filter(corpus_id__in=keys):
            tags_by_corpus_ids[m2m.corpus_id].append(m2m.corpustag)
        return Promise.resolve([
            tags_by_corpus_ids.get(corpus_id, [])
            for corpus_id in keys
        ])


tags_by_corpus_ids_loader = TagsByCorpusIdLoader()  # outside the context frame of one Corpus, since this should be used in a Connection (multiple corpora)


class AuthorsByCorpusIdLoader(DataLoader):
    """
    Optimized way to retrieve instances of foreign models having
    a ForeignKey to the Corpus instance.
    Current implementation for the through CorpusAuthor model (authors of the Corpus).
    """
    def batch_load_fn(self, keys):
        authors_by_corpus_ids = defaultdict(list)
        for m2m in CorpusModel.authors.through.objects.filter(corpus_id__in=keys).select_related('author'):
            authors_by_corpus_ids[m2m.corpus_id].append(m2m)
        return Promise.resolve([
            authors_by_corpus_ids.get(corpus_id, [])
            for corpus_id in keys
        ])


authors_by_corpus_ids_loader = AuthorsByCorpusIdLoader()  # outside the context frame of one Corpus, since this should be used in a Connection (multiple corpora)


class Corpus(PKMixin, DjangoObjectType):
    """
    Corpus Node
    """
    cost = 20

    class Meta:
        model = CorpusModel
        only_fields = (
            'cloned_from',
            'digital_object_identifier',

            'title',
            'description',
            'license',
            'language',
            'draft',
            'related',

            'comments_allowed',
            'comments_only_local',
            'comments_only_visible_if_approved',

            'created_at',
            'updated_at',
            'original_publication_at',
        )
        description = "This is a Corpus. It contains files, metadata describing them, and discussions around them."
        interfaces = (relay.Node, ActionTargetInterface,)
        connection_class = CountableConnectionBase

    actor = Field('olki.apps.account.schema.Actor')
    authors = Field(List(CorpusAuthor))
    tags = Field(List(CorpusTag))
    files = Field(List(CorpusFile), description=_('Files of a Corpus which are publicly available, and their metadata.'))
    comments_count = Field(Int)

    def resolve_files(self, info):
        return self.files.all()

    def resolve_authors(self, info):
        return authors_by_corpus_ids_loader.load(self.id)

    def resolve_tags(self, info):
        return tags_by_corpus_ids_loader.load(self.id)

    def resolve_comments_count(self, info):
        ## First naive implementation
        # return target_stream(self)\
        #     .filter(verb__in=['Add'])\
        #     .filter(public=True)\
        #     .filter(action_object_content_type=NoteContentType)\
        #     .filter(actions_with_note_note_as_action_object__public=True)\
        #     .count()
        ## Second naive implementation
        # return NoteModel.objects\
        #     .filter(corpus=self)\
        #     .filter(public=True)\
        #     .count()
        return CommentsByCorpusIdLoader().load(self.id).then(lambda x: len(x))


class CharInFilter(BaseInFilter, CharFilter):
    """See https://django-filter.readthedocs.io/en/master/ref/filters.html#base-in-filter"""
    pass


class CorpusFilter(FilterSet):
    class Meta:
        model = CorpusModel
        fields = ('order_by', )

    # ordering
    order_by = OrderingFilter(
        fields=(
            ('created_at', 'created_at'),
            ('updated_at', 'updated_at')
        ),
        # automatically creates 'created_at', '-created_at' orders
    )

    # exclusion booleans
    exclude_drafts = BooleanFilter(field_name='draft', exclude=True)
    exclude_quarantined = BooleanFilter(field_name='quarantined', exclude=True)
    # restriction booleans
    only_drafts = BooleanFilter(field_name='draft', exclude=False)
    only_quarantined = BooleanFilter(field_name='quarantined', exclude=False)

    # regular text search
    title = CharFilter(lookup_expr='icontains')
    language = CharFilter(lookup_expr='exact')

    # tag filtering
    tags_name__in = CharInFilter(field_name='tags__name', lookup_expr='in')
    tags_code__in = CharInFilter(field_name='tags__code', lookup_expr='in')

    def filter_all_in(qs, name, value):
        for v in value:
            qs = qs.filter(**{name: v})
        return qs

    tags_name__all_in = CharInFilter(field_name='tags__name', method=filter_all_in)
    tags_code__all_in = CharInFilter(field_name='tags__code', method=filter_all_in)

    # author filtering
    authors_name__all_in = CharInFilter(field_name='authors__name', method=filter_all_in)
    authors_orcid__all_in = CharInFilter(field_name='authors__orcid', method=filter_all_in)

    # organization filetring
    organizations__all_in = CharInFilter(field_name='authors_memberships__affiliation', method=filter_all_in)

    # full-text search
    full_text = CharFilter(field_name='full_text', method='full_text_search')

    def full_text_search(self, queryset, name, value):
        return self._meta.model.objects.search(value) & queryset

    @property
    def qs(self):
        # The query context can be found in self.request.
        return super(CorpusFilter, self).qs  # default value but better be explicit


class CorpusQuery(object):
    corpus = Field(
        Corpus,
        pk=String(required=True, description=_('A Flax ID that is used as a primary key for the model.')),
        description=_('Gets a single Corpus.')
    )
    # A connection is a vitaminized version of a List that provides ways of slicing and paginating through it.
    corpora = DjangoFilterConnectionField(Corpus, filterset_class=CorpusFilter)
    # For users to get their own corpora
    own_corpora = DjangoFilterConnectionField(Corpus, filterset_class=CorpusFilter)
    # For third-parties to get a user's corpora
    user_corpora = DjangoFilterConnectionField(
        Corpus,
        filterset_class=CorpusFilter,
        username=String(required=True)
    )
    # For typeahead
    tags = Field(
        List(CorpusTag),
        name=String(required=True),
        description=_('List of tags involved in corpora')
    )
    authors = Field(
        List(String),
        name=String(required=True),
        description=_('List of authors involved in corpora')
    )
    organizations = Field(
        List(String),
        name=String(required=True),
        description=_('List of organizations involved in corpora')
    )
    # For import OAI existence check
    already_cloned = Field(
        Boolean,
        cloned_from__contains=String(required=True),
        description=_('Checks if corpora cloned from a URI containing a given string are already known')
    )

    def resolve_corpus(self, info, pk, **kwargs):
        qs = CorpusModel.objects.filter(pk__exact=pk)
        # owner can see it
        if info.context.user == qs.get():
            return qs.get()
        # admins, moderators can see it
        if info.context.user.is_authenticated and has_role(info.context.user, [Administrator, Moderator]):
            return qs.get()
        # anon can only see a public corpus
        return qs.exclude(draft=True)\
                 .exclude(quarantined=True)\
                 .get()

    def resolve_corpora(self, info, **kwargs):
        # admins, moderators can see all corpora
        if info.context.user.is_authenticated and has_role(info.context.user, [Administrator, Moderator]):
            return CorpusFilter(kwargs).qs
        # owners can see their own corpora regardless of status in addition to what anon can
        if info.context.user.is_authenticated:
            return (
                CorpusFilter(kwargs).qs
                &  # queryset intersection
                info.context.user.actor.corpora_owned.public.all()
            )
        # anon can only see public corpora
        else:
            return CorpusFilter(kwargs).qs\
                .exclude(draft=True)\
                .exclude(quarantined=True)

    def resolve_own_corpora(self, info, **kwargs):
        if info.context.user.is_authenticated:
            return CorpusFilter(kwargs).qs & info.context.user.actor.corpora_owned.all()
        else:
            return CorpusModel.objects.none()

    def resolve_user_corpora(self, info, username, **kwargs):
        corpora = CorpusFilter(kwargs).qs\
                .exclude(draft=True)\
                .exclude(quarantined=True)
        actor = ActorModel.objects.filter(preferred_username__exact=username).first()
        return corpora & actor.corpora_owned.all()

    def resolve_tags(self, info, name, **kwargs):
        return CorpusTagModel.objects\
            .filter(name__icontains=name)\
            .order_by()[:3]

    def resolve_authors(self, info, name, **kwargs):
        return CorpusAuthorModel.objects\
            .filter(name__icontains=name)\
            .values_list('name', flat=True)\
            .distinct()\
            .order_by()[:3]

    def resolve_organizations(self, info, name, **kwargs):
        return CorpusAuthorMembershipModel.objects\
            .filter(affiliation__icontains=name)\
            .values_list('affiliation', flat=True)\
            .distinct()\
            .order_by()[:3]

    def resolve_already_cloned(self, info, cloned_from__contains, **kwargs):
        return CorpusModel.harvested\
            .filter(cloned_from__contains=cloned_from__contains)\
            .exists()