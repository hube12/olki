import json

import graphene
from actstream import action
from graphene.types import List, UUID, String, Boolean
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import ErrorType
from graphql_jwt.decorators import login_required
from rolepermissions.checkers import has_object_permission
from rolepermissions.checkers import has_role

from olki.apps.account.roles import Administrator
from .forms import CorpusForm
from .models import Corpus as CorpusModel


class CreateCorpus(DjangoModelFormMutation):
    """
    Mutation to initialize a Corpus
    """
    class Input:
        """
        List of file GUID as they are stored on DRF-TUS
        (objects available at from rest_framework_tus.models import get_upload_model)
        """
        files = List(UUID, description='A list of file GUIDs that have already been uploaded to the TUS server.')

    class Meta:
        form_class = CorpusForm
        exclude_fields = ('id', )

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        form = cls.get_form(root, info, **input)

        if form.is_valid():
            # create Corpus
            obj = CorpusModel(actor=info.context.user.actor, **form.cleaned_data)
            obj.save()

            # create actions for the actor involved
            action.send(info.context.user.actor, verb='Create', target=obj)

            # deal with files
            files_guid = input.get('files')
            obj.add_tus_files(files_guid)

            kwargs = {cls._meta.return_field_name: obj}
            return cls(errors=[], **kwargs)
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(errors=errors)


class EditCorpus(DjangoModelFormMutation):
    """
    Mutation to edit an existing corpus
    """
    class Input:
        """
        List of file GUID as they are stored on DRF-TUS
        (objects available at from rest_framework_tus.models import get_upload_model)
        """
        files = List(UUID, description='A list of file GUIDs that have already been uploaded to the TUS server.')
        pk = String(required=True, description='A Flax ID that is used as a primary key for the model.')

    class Meta:
        form_class = CorpusForm
        exclude_fields = ('id', )

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        # get corpus
        obj = CorpusModel.objects.get(id=input.get('pk'))
        if not info.context.user.actor.corpora_owned.filter(pk=obj.pk).exists() and not has_role(info.context.user, [Administrator]):
            errors = [ErrorType(field='pk', messages=['unauthorized to edit corpus'])]
            return cls(errors=errors)

        obj.save()

        # set target corpus for get_form (same id as 'get corpus' step)
        input['id'] = input.get('pk')

        form = cls.get_form(root, info, **input)

        if form.is_valid():
            # deal with new files
            files_guid = input.get('files')
            obj.add_tus_files(files_guid)

            mutation = cls.perform_mutate(form, info)

            # create action for the actor involved
            if form.has_changed and [i for i in form.changed_data if i not in obj.no_update_fields]:
                action.send(info.context.user.actor, verb='Update', target=obj, description=json.dumps(form.changed_data))

            return mutation
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(errors=errors)


class DeleteCorpus(graphene.Mutation):
    """
    Mutation to delete a corpus
    """
    class Arguments:
        pk = String(required=True, description='A Flax ID that is used as a primary key for the model.')

    success = Boolean()
    errors = List(String)

    def mutate(self, info, pk):
        is_authenticated = info.context.user.is_authenticated
        if not is_authenticated:
            errors = ['unauthenticated']
        else:
            try:
                corpus = CorpusModel.objects.get(id=pk)
                if not has_object_permission('delete_corpus', info.context.user, corpus) and not info.context.user.actor.corpora_owned.filter(pk=corpus.pk).exists():
                    errors = ['unauthorized to delete corpus']
                    return DeleteCorpus(success=False, errors=errors)
                corpus.delete()
                return DeleteCorpus(success=True)
            except CorpusModel.DoesNotExist:
                errors = ['corpus not found']
        return DeleteCorpus(success=False, errors=errors)


class CorpusMutations(graphene.AbstractType):
    createCorpus = CreateCorpus.Field()
    editCorpus = EditCorpus.Field()
    deleteCorpus = DeleteCorpus.Field()
