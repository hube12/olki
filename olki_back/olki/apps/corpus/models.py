# pylint: disable=no-member
import json
from os.path import splitext

import django.contrib.postgres.search as pg_search
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.indexes import GinIndex
from django.core.files import File
from django.db import models
from django.db.models import Q
from django.utils.text import Truncator
from django.utils.translation import ugettext_lazy as _
from django_prometheus.models import ExportModelOperationsMixin
from flax_id.django.fields import FlaxId
from model_utils.managers import QueryManager
from rest_framework_tus.models import get_upload_model

from olki.apps.core.models import TimestampedModel, FederationMixin
from olki.apps.core.utils import hash_file
from .managers import CorpusManager


def corpus_directory_path(instance, filename):
    # Get new file name/upload path
    base, ext = splitext(filename)
    return 'corpus/{0}/{1}{2}'.format(instance.corpus.id, base, ext)


class CorpusAuthor(models.Model):
    """
    An author can be present on multiple corpora.
    Defaults to a 'First Name Last Name' conventional format.
    """
    name = models.CharField(max_length=200, blank=False, default=None)
    orcid = models.CharField(blank=True, null=True, unique=True, max_length=200)

    class Meta:
        # while we can have two homonyms, we cannot have them with
        # the same ORCID, nor have the ORCID appear twice.
        unique_together = (("name", "orcid"),)
        ordering = ['name']

    def __str__(self):
        return self.name


class CorpusAuthorMembership(models.Model):
    """
    Metadata associated with an author, local to the corpus.
    """
    email = models.EmailField(blank=True, null=True)
    affiliation = models.CharField(blank=True, null=True, max_length=200)
    role = models.CharField(blank=True, null=True, max_length=200)

    author = models.ForeignKey('corpus.CorpusAuthor', related_name='author_memberships', on_delete=models.CASCADE)
    corpus = models.ForeignKey('corpus.Corpus', related_name='authors_memberships', on_delete=models.CASCADE)
    order = models.IntegerField(
        verbose_name=_('Order'),
        help_text=_('What order to display this person within the corpus.')
    )

    class Meta:
        ordering = ['order']

    def __str__(self):
        return "{}:{}:{}".format(self.email, self.affiliation, self.role)


class CorpusTag(models.Model):
    code = models.CharField(max_length=200, unique=True, help_text=_('WikiData id'), blank=False, default=None)
    name = models.CharField(max_length=200, help_text=_('English name corresponding to the id'), blank=False, default=None)
    description = models.CharField(max_length=500, null=True, blank=True, help_text=_('English description corresponding to the id'))

    class Meta:
        ordering = ['name']
        unique_together = (("code", "name", "description"),)

    def __str__(self):
        return self.name


class Corpus(
    TimestampedModel,
    FederationMixin,
    ExportModelOperationsMixin('corpus')
):
    id = FlaxId(primary_key=True, help_text=_('A Flax ID that is used as a primary key for the model.'))

    # authors
    actor = models.ForeignKey(
        'federation.Actor',
        related_name='corpora_owned',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text=_('The actor that deposited the corpus.')
    )  # note: the actor is null when repository is set
    authors = models.ManyToManyField(
        CorpusAuthor,
        through=CorpusAuthorMembership,
        related_name='corpora_authored',
        help_text=_('A list of authors that cannot be given ownership over the corpus (i.e.: because they are remote users).')
    )

    # origin
    cloned_from = models.URLField(blank=True, null=True, help_text=_('The id or URL of the resource used as a base for this Corpus.'))
    repository = models.ForeignKey(
        'oaipmh.Repository',
        related_name='corpora',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text=_('The repository that contains the corpus.')
    )  # note: the actor is null when repository is set

    # generic metadata
    title = models.TextField()
    description = models.TextField(blank=True, null=True)
    license = models.TextField(blank=True, null=True, help_text=_('An [SDPX license](https://spdx.org/licenses/) identifier.'))
    language = models.TextField(help_text=_('An [iso639-1](https://en.wikipedia.org/wiki/ISO_639-1) code.'))
    digital_object_identifier = models.URLField(blank=True, null=True, help_text=_('The DOI or DOI URL of the Corpus.'))
    original_publication_at = models.DateTimeField(blank=True, null=True, default=None, help_text=_('Optional original date of publication, if any.'))
    tags = models.ManyToManyField(
        CorpusTag,
        related_name='corpora_tagged',
        help_text=_('A list of tags and categories describing the Corpus.')
    )
    related = JSONField(blank=True, null=True, help_text=_('A list of related URI/identifiers with their DataCite Metadata Scheme as a value.'))

    # visibility
    draft = models.BooleanField(default=True)
    quarantined = models.BooleanField(default=False)

    # comments
    comments_allowed = models.BooleanField(default=True)
    comments_only_local = models.BooleanField(default=False)
    comments_only_visible_if_approved = models.BooleanField(default=False)

    # these fields shouldn't be triggering updates nor show in update actions
    no_update_fields = [
        'draft',
        'quarantined',
        'comments_allowed',
        'comments_only_local',
        'comments_only_visible_if_approved',
    ]

    # indexation-related, allowing FTS search
    search_vector = pg_search.SearchVectorField(blank=True, null=True)

    objects = CorpusManager()
    # Some notable shortcuts to common filtering cases:
    public = QueryManager(Q(draft=False) & Q(quarantined=False))
    local = QueryManager(Q(cloned_from__exact='') & Q(actor__domain_id=settings.INSTANCE_HOSTNAME))
    harvested = QueryManager(~Q(cloned_from__exact=''))

    def __str__(self):
        return Truncator(self.title).words(10)

    @property
    def filesize(self):
        if hasattr(self, 'files'):
            return sum([corpusfile.file.size for corpusfile in self.files.all()])
        else:
            return 0

    def get_absolute_url(self):
        return "/corpus/{}".format(self.id)

    def to_json(self):
        from olki.apps.federation.serializers import CorpusSerializer
        return CorpusSerializer(self).data

    def add_tus_files(self, files_guid):
        """
        add files stored by TUS (drf-tus) from the files_guid array
        :param files_guid:
        """
        for guid in files_guid:
            try:
                tmp_file = get_upload_model().objects.get(guid=guid)
                tmp_file_metadata = json.loads(tmp_file.upload_metadata)
                CorpusFile.objects.create(
                    corpus=self,
                    file=File(open(tmp_file.temporary_file_path, 'rb'),
                              name=tmp_file_metadata['filename']),
                    filetype=tmp_file_metadata['filetype'],
                    name=tmp_file_metadata['name'],
                    description=tmp_file_metadata['description'] if 'description' in tmp_file_metadata else None,
                )
                tmp_file.delete()
            except get_upload_model().DoesNotExist:
                pass

    class Meta:
        # these permissions get added to the Django default_permissions
        permissions = [
            ("add_corpus_freely", _("Can upload corpus without going through the quarantine system")),
            ("comment_corpus", _("Can comment corpus")),
            ("moderate_corpus", _("Can moderate corpus"))
        ]
        # indexation-related, allowing FTS search
        indexes = [GinIndex(fields=['search_vector'])]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if 'update_fields' not in kwargs or 'search_vector' not in kwargs['update_fields']:
            instance = self._meta.default_manager\
                .annotate(sv=self._meta.default_manager.search_vectors)\
                .get(pk=self.pk)
            instance.search_vector = instance.sv
            instance.save(update_fields=['search_vector'])


class CorpusFile(TimestampedModel):
    """
    A file in a corpus
    """
    corpus = models.ForeignKey(Corpus, on_delete=models.CASCADE, related_name='files', help_text=_('Files of a corpus'), blank=True, null=True)
    file = models.FileField(upload_to=corpus_directory_path)
    filetype = models.CharField(blank=True, null=True, max_length=255)
    hash = models.CharField(blank=True, null=True, max_length=255)
    name = models.CharField(blank=True, null=True, max_length=255)
    description = models.TextField(blank=True, null=True, max_length=2000)

    @property
    def filesize(self):
        return self.file.size

    class Meta:
        ordering = ['name']

    def save(self, *args, **kwargs):
        if not self.pk:
            # TODO: might want to delay that in a task, since large files could be slow to hash
            self.hash = hash_file(self.file)
        super(CorpusFile, self).save(*args, **kwargs)
