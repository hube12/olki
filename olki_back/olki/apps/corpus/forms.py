import json
from django import forms

from .models import Corpus as CorpusModel, CorpusAuthor, CorpusTag


class CorpusForm(forms.ModelForm):
    class Meta:
        model = CorpusModel
        fields = (
            'title',
            'description',
            'digital_object_identifier',
            'cloned_from',
            'license',
            'language',
            'draft',

            'related',

            'comments_allowed',
            'comments_only_local',
            'comments_only_visible_if_approved',

            'original_publication_at',
        )

    tags = forms.CharField(required=False)
    authors = forms.CharField(required=False)

    def clean_title(self):
        return ' '.join(self.cleaned_data.get('title').split()).replace('\n', ' ')

    def clean_tags(self):
        return json.loads(self.cleaned_data.get('tags') or '[]')

    def clean_authors(self):
        return json.loads(self.cleaned_data.get('authors') or '[]')

    def save(self, commit=True):
        corpus = super(CorpusForm, self).save()  # Save the child so we have an ID for the m2m

        authors = self.cleaned_data.get('authors')
        if authors:
            # update/create
            corpus_authors = []
            for index, value in enumerate(authors):
                author, _ = corpus.authors.update_or_create(
                    name=value.pop('name'),
                    orcid=value.pop('orcid', None),
                    through_defaults={'order': index}
                )
                corpus_authors.append(author)
                corpus.authors_memberships.filter(author=author).update(**value)
            # delete/cleanup
            [corpus.authors.remove(a) for a in corpus.authors.all() if a.name not in [i.name for i in corpus_authors]]  # authors not in the form
            CorpusAuthor.objects.filter(author_memberships__isnull=True).delete()  # authors that have no link to any corpus anymore

        tags = self.cleaned_data.get('tags')
        if tags:
            # update/create
            corpus_tags = []
            for index, value in enumerate(tags):
                try:
                    tag, _ = CorpusTag.objects.get_or_create(
                        code=value.get('code', value.get('name')),
                        name=value.get('name'),
                        description=value.get('description', '')
                    )
                    corpus.tags.add(tag)
                    corpus_tags.append(tag)
                except Exception:
                    pass
            # delete/cleanup
            [corpus.tags.remove(a) for a in corpus.tags.all() if a.code not in [i.code for i in corpus_tags]]  # tags not in the form
            CorpusTag.objects.filter(corpora_tagged__isnull=True).delete()  # tags that have no link to any corpus anymore

        return corpus
