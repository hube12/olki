from datetime import timezone

import factory
import random
from faker import Faker

from olki.fixtures.factories import registry, NoUpdateOnCreate
from .models import CorpusTag

fake = Faker('la')  # latin provider, to avoid weird-looking sentences


def entity():
    possible_occupations = [
        'Contact person',
        'Data collector',
        'Data curator',
        'Data manager',
        'Supervisor',
        'Sponsor',
        'Project manager',
        'Project leader',
        'Project member',
        'Researcher',
        'Rights holder',
        'Work package leader',
        'Distributor',
        'Editor',
        'Producer',
    ]
    eventual_affiliations = [
        'Université de Nantes',
        'Freie Universität Berlin',
        'Université de Nice Sophia Antipolis',
        'Université Paris-Saclay',
        'Université de Lorraine',
    ]
    return [
        {
            'name': fake.name(),
            'affiliation': fake.random_element(elements=eventual_affiliations),
            'role': fake.random_element(elements=possible_occupations)
        }
        for _ in range(random.randint(1, 5))
    ]


def tags():
    possible_categories = [
        {'code': 'wikidata.org/entity/Q10870555', 'name': 'Report', 'description': 'Formal, detailed text'},
        {'code': 'wikidata.org/entity/Q3284399', 'name': 'Statistical Model', 'description': 'Type of mathematical model'},
        {'code': 'wikidata.org/entity/Q187685', 'name': 'Thesis',
         'description': 'Document submitted in support of candidature for a doctorate'},
        {'code': 'wikidata.org/entity/Q580922', 'name': 'Preprint',
         'description': 'Version of a scholarly or scientific paper that precedes publication in a peer-reviewed scholarly or scientific journal'},
        {'code': 'wikidata.org/entity/Q23927052', 'name': 'Conference paper',
         'description': 'Document published as part of the proceedings of an academic conference'},
        {'code': 'wikidata.org/entity/Q571', 'name': 'Book',
         'description': 'Medium for a collection of words and/or pictures to represent knowledge or a fictional story'},
        {'code': 'wikidata.org/entity/Q324254', 'name': 'Ontology', 'description': 'Specification of a conceptualization'},
        {'code': 'wikidata.org/entity/Q603773', 'name': 'Lecture/Presentation',
         'description': 'Oral presentation intended to present information or teach people about a particular subject'},
        {'code': 'wikidata.org/entity/Q128751', 'name': 'Source Code',
         'description': 'Collection of computer instructions written using some human-readable computer language'},
        {'code': 'wikidata.org/entity/Q379833', 'name': 'Lesson',
         'description': 'Section of learning or teaching into which a wider learning content is divided'},
        {'code': 'wikidata.org/entity/Q478798', 'name': 'Image(s)',
         'description': 'Artifact that depicts or records visual perception'},
        {'code': 'wikidata.org/entity/Q26987250', 'name': 'Audio',
         'description': 'Relating to sound waves that have been electronically recorded, transmitted, or reproduced'}
    ]
    return [
        {
            'name': 'Science',
            'code': 'wikidata.org/entity/Q336',
            'description': 'Study and knowledge of the natural world'
        },
        *[
            fake.random_element(elements=possible_categories)
            for _ in range(random.randint(1, 2))
        ]
    ]


# Extract from the SPDX license list
LICENSE_IDS = [
    '0BSD',
    'AGPL-3.0-or-later',
    'AML',
    'CECILL-C',
    'EUPL-1.2',
]

# Extract from the language list
LANGUAGE_IDS = [
    'en',
    'fr',
    'ga',
    'it',
    'ja',
]


@registry.register
class CorpusFactory(NoUpdateOnCreate, factory.django.DjangoModelFactory):
    class Meta:
        model = "corpus.Corpus"

    title = factory.Faker('sentence', locale='la')
    description = factory.Sequence(lambda n: "\n".join(fake.paragraphs(nb=5)))

    license = factory.Faker('random_element', elements=LICENSE_IDS)
    language = factory.Faker('random_element', elements=LANGUAGE_IDS)
    original_publication_at = factory.Faker("past_datetime", start_date="-10y", tzinfo=timezone.utc) if factory.Faker("boolean") else None

    draft = False
    quarantined = False

    comments_allowed = factory.Faker("boolean")
    comments_only_local = factory.Faker("boolean")
    comments_only_visible_if_approved = factory.Faker("boolean")

    """
    Dealing with M2M relationships: https://factoryboy.readthedocs.io/en/latest/recipes.html#simple-many-to-many-relationship
    """
    @factory.post_generation
    def authors(self, create, extracted, **kwargs):
        if create:
            # Called when no 'authors' param is given
            for index, a in enumerate(entity()):
                author, _ = self.authors.update_or_create(
                    name=a.pop('name'),
                    orcid=a.pop('orcid', None),
                    through_defaults={'order': index}
                )
                self.authors_memberships.filter(author=author).update(**a)

        if extracted:
            # Called when a tuple/list of users were passed to a 'authors' param
            for author in extracted:
                self.authors.add(author)

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if create:
            # Called when no 'authors' param is given
            for index, a in enumerate(tags()):
                tag, _ = CorpusTag.objects.get_or_create(
                    code=a.get('code'),
                    name=a.get('name'),
                    description=a.get('description', None)
                )
                self.tags.add(tag)

        if extracted:
            # Called when a tuple/list of users were passed to a 'authors' param
            for tag in extracted:
                self.tags.add(tag)

    @factory.post_generation
    def creation_date(self, create, extracted, **kwargs):
        self.created_at = fake.past_datetime(start_date="-10y", tzinfo=timezone.utc)
        self.updated_at = fake.past_datetime(start_date="-10d", tzinfo=timezone.utc)
