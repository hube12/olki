import pytest
from django.test import TestCase
from django.urls import reverse


class TestCorpusViews(TestCase):
    def test_viewset_with_no_matching_corpus_errors(self):
        response = self.client.get(reverse('corpus:citation-bib', kwargs={'corpus_id': 'unknown_corpus_id'}))
        self.assertEqual(response.status_code, 404)

    @pytest.mark.usefixtures('fixtures')
    def test_viewset_with_matching_corpus_succeeds(self):
        user = self.factories['account.User']()
        corpus = self.factories['corpus.Corpus'](actor=user.actor)
        response = self.client.get(reverse('corpus:citation-bib', kwargs={'corpus_id': corpus.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'corpus/citation.bib')

    @pytest.mark.with_fe
    @pytest.mark.usefixtures('fixtures')
    def test_corpus_link(self):
        user = self.factories['account.User']()
        corpus = self.factories['corpus.Corpus'](actor=user.actor)
        response = self.client.get(reverse('corpus', urlconf='olki.apps.corpus.spa_urls', kwargs={'corpus_id': corpus.id}))
        self.assertEqual(response.status_code, 200)
        # assert response.has_header('Link')
