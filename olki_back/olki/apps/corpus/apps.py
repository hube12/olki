from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'olki.apps.corpus'

    def ready(self):
        # register Corpus as a model for actstream
        from actstream import registry as actstream_registry
        actstream_registry.register(self.get_model('Corpus'))
