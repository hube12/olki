# pylint: disable=no-member
from os.path import splitext
from uuid import uuid4

from django.conf import settings
from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from easy_thumbnails.fields import ThumbnailerImageField

from olki.apps.core.models import TimestampedModel


def user_directory_path(instance, filename):
    # Get new file name/upload path
    base, ext = splitext(filename)
    return 'user/{0}/{1}{2}'.format(instance.user.id, uuid4(), ext)


class Profile(TimestampedModel):
    # Relations
    #
    # There is an inherent relationship between the Profile and
    # User models. By creating a one-to-one relationship between the two, we
    # are formalizing this relationship. Every user will have one -- and only
    # one -- related Profile model.
    user = models.OneToOneField(
        get_user_model(),
        on_delete=models.CASCADE  # deletes the referenced object upon deletion
    )

    # Attributes - Mandatory
    #
    # Each user has a name. A name cannot be empty
    name = models.TextField(blank=True)

    # Each user profile will have a field where they can tell other users
    # something about themselves. This field will be empty when the user
    # creates their account, so we specify `blank=True`.
    bio = models.TextField(blank=True)

    # Each user may have a profile image or avatar.
    # Similar to `bio`, this field is not required. It may be blank.
    image = ThumbnailerImageField(
        upload_to=user_directory_path,
        blank=True,
        resize_source=settings.THUMBNAIL_ALIASES['']['avatar']
    )

    # A profile is by default telling web scrapers not to index it
    # (noindex, nofollow).
    # Note: we could have used dynamic-preferences to store it,
    # but this way we have easier access to it for filtering.
    indexed = models.BooleanField(default=False)

    # A profile can be set to private to only show minimal information
    # Note: we could have used dynamic-preferences to store it,
    # but this way we have easier access to it for filtering.
    private = models.BooleanField(default=False)

    # This is an example of a Many-To-Many relationship where both sides of the
    # relationship are of the same model. In this case, the model is `Profile`.
    # As mentioned in the text, this relationship will be one-way. Just because
    # you are following mean does not mean that I am following you. This is
    # what `symmetrical=False` does for us.
    follows = models.ManyToManyField(
        'self',
        related_name='followed_by',
        symmetrical=False
    )

    # Attributes - Optional
    @property
    def avatar(self):
        return self.image.url if self.image else ''

    # Object Manager
    # Custom Properties

    # Methods
    def follow(self, profile):
        """Follow `profile` if we're not already following `profile`."""
        self.follows.add(profile)

    def unfollow(self, profile):
        """Unfollow `profile` if we're already following `profile`."""
        self.follows.remove(profile)

    def is_following(self, profile):
        """Returns True if we're following `profile`; False otherwise."""
        return self.follows.filter(pk=profile.pk).exists()

    def is_followed_by(self, profile):
        """Returns True if `profile` is following us; False otherwise."""
        return self.followed_by.filter(pk=profile.pk).exists()

    # Meta and String
    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


@receiver(post_save, sender=get_user_model(), dispatch_uid="create_user_profile")
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
