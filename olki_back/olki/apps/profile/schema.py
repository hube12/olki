from graphene import relay, ObjectType, Field, String
from graphene_django.types import DjangoObjectType
from graphene_django.filter.fields import DjangoFilterConnectionField

from .models import Profile as ProfileModel


class Profile(DjangoObjectType):
    """
    Profile Node
    """
    cost = 10

    class Meta:
        model = ProfileModel
        filter_fields = {
            'name': ['exact'],
            'bio': ['exact', 'icontains', 'istartswith'],
        }
        only_fields = (
            'name',
            'indexed',
            'private',
            'bio',
            'avatar',
            'user',
            'created_at',
        )
        interfaces = (relay.Node, )

    avatar = String()

    @classmethod
    def get_node(cls, id, info):
        try:
            profile = cls._meta.model.objects.get(id=id)
        except cls._meta.model.DoesNotExist:
            return None

        if not profile.private:
            return profile
        return None


class ProfileQuery(object):
    """
    what is an abstract type?
    http://docs.graphene-python.org/en/latest/types/abstracttypes/
    """
    profile = relay.Node.Field(Profile)
    profiles = DjangoFilterConnectionField(Profile)


class Viewer(ObjectType):
    profile = Field(Profile)

    @staticmethod
    def resolve_profile(info, **kwargs):
        return info.context.user
