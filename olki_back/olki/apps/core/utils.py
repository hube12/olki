import functools
import random
import string
import json

from urllib.parse import parse_qs, urlencode, urlsplit, urlunsplit

from django.db import transaction
from dynamic_preferences.registries import global_preferences_registry

DEFAULT_CHAR_STRING = string.ascii_lowercase + string.digits


def generate_random_string(chars=DEFAULT_CHAR_STRING, size=6):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_nodeinfo_metadata():
    global_preferences = global_preferences_registry.manager()

    return {
        "staff_accounts": [],
        "taxonomy": {
            "postsName": "Corpus"
        },
        "nodeName": global_preferences['general__site_title'],
        "nodeDefaultClientRoute": "home",
        "nodeDescription": global_preferences['general__site_description'],
        "nodeTerms": global_preferences['general__site_tos'],
        "nodeRules": global_preferences['general__site_rules'],
        "nodeSubjects": json.loads(global_preferences['general__site_subjects']) if global_preferences['general__site_subjects'] else ""
    }


def on_commit(f, *args, **kwargs):
    return transaction.on_commit(lambda: f(*args, **kwargs))


def set_query_parameter(url, **kwargs):
    """Given a URL, set or replace a query parameter and return the
    modified URL.

    >>> set_query_parameter('http://example.com?foo=bar&biz=baz', foo='stuff')
    'http://example.com?foo=stuff&biz=baz'
    """
    scheme, netloc, path, query_string, fragment = urlsplit(url)
    query_params = parse_qs(query_string)

    for param_name, param_value in kwargs.items():
        query_params[param_name] = [param_value]
    new_query_string = urlencode(query_params, doseq=True)

    return urlunsplit((scheme, netloc, path, new_query_string, fragment))


def getter(data, *keys, default=None):
    if not data:
        return default
    v = data
    for k in keys:
        try:
            v = v[k]
        except KeyError:
            return default

    return v


class FrozenDict(dict):
    def __init__(self, *args, **kwargs):
        self._hash = None
        super(FrozenDict, self).__init__(*args, **kwargs)

    def __hash__(self):
        if self._hash is None:
            self._hash = hash(tuple(sorted(self.items())))  # iteritems() on py2
        return self._hash

    def _immutable(self, *args, **kws):
        raise TypeError('cannot change object - object is immutable')

    __setitem__ = _immutable
    __delitem__ = _immutable
    pop = _immutable
    popitem = _immutable
    clear = _immutable
    update = _immutable
    setdefault = _immutable


def hash_file(file, block_size=65536):
    import hashlib
    from functools import partial
    hasher = hashlib.md5()
    for buf in iter(partial(file.read, block_size), b''):
        hasher.update(buf)

    return hasher.hexdigest()


def require_instance(model_or_qs, parameter_name, id_kwarg_name=None):
    """
    Injects a key as parameter (id_kwarg_name), to fetch the instance of
    a model corresponding to that key (model_or_qs) and assigns it to
    the (parameter_name). Useful to get an object without serialization.

    :param model_or_qs:
    :param parameter_name:
    :param id_kwarg_name:
    :return: decorator
    """
    def decorator(function):
        @functools.wraps(function)
        def inner(*args, **kwargs):
            kw = id_kwarg_name or "_".join([parameter_name, "id"])
            pk = kwargs.pop(kw)
            try:
                instance = model_or_qs.get(pk=pk)
            except AttributeError:
                instance = model_or_qs.objects.get(pk=pk)
            kwargs[parameter_name] = instance
            return function(*args, **kwargs)

        return inner

    return decorator


def concat_dicts(*dicts):
    n = {}
    for d in dicts:
        n.update(d)

    return n
