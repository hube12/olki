# pylint: disable=redefined-builtin

"""py
    Author: Eliot Berriot
    Project of origin: Funkwhale
    Commit of origin: 49769819265170a771e747b8696de6a27a100379
    Project licence: AGPLv3.0 or later
"""
from django import forms
from django.conf import settings
from dynamic_preferences import serializers, types
from dynamic_preferences.registries import global_preferences_registry


class DefaultFromSettingMixin():
    def get_default(self):
        return getattr(settings, self.setting)


def get(pref):
    manager = global_preferences_registry.manager()
    return manager[pref]


def set(pref, value):
    manager = global_preferences_registry.manager()
    manager[pref] = value


class StringListSerializer(serializers.BaseSerializer):
    separator = ","
    sort = True

    @classmethod
    def to_db(cls, value, **kwargs):
        if not value:
            return ""

        if isinstance(value, (list, tuple)):
            raise cls.exception(
                "Cannot serialize, value {} is not a list or a tuple".format(value)
            )

        if cls.sort:
            value = sorted(value)
        return cls.separator.join(value)

    @classmethod
    def to_python(cls, value, **kwargs):
        if not value:
            return []
        return value.split(",")


class StringListPreference(types.BasePreferenceType):
    serializer = StringListSerializer
    field_class = forms.MultipleChoiceField

    def get_api_additional_data(self):
        d = super(StringListPreference, self).get_api_additional_data()
        d["choices"] = self.get("choices")
        return d
