from dynamic_preferences.types import BooleanPreference, StringPreference, LongStringPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry

from . import preferences

general = Section('general')


@global_preferences_registry.register
class SiteTitle(
        preferences.DefaultFromSettingMixin,
        StringPreference
):
    section = general
    name = 'site_title'
    required = False
    setting = 'INSTANCE_NAME'
    help_text = 'Site title, displayed and shared to indentify the instance.'


@global_preferences_registry.register
class SiteDescription(
        LongStringPreference
):
    section = general
    name = 'site_description'
    default = ''
    required = False
    help_text = 'Site description, necessary to present the purpose of your instance to potential users, or other ' \
                'admins wishing to federate with you. '


@global_preferences_registry.register
class SiteDescription(
        LongStringPreference
):
    section = general
    name = 'site_tos'
    setting = 'INSTANCE_TERMS'
    default = ''
    required = False
    help_text = 'Site terms of service, necessary to present the terms under which you provide service to potential users.'


@global_preferences_registry.register
class SiteDescription(
        LongStringPreference
):
    section = general
    name = 'site_rules'
    setting = 'INSTANCE_RULES'
    default = ''
    required = False
    help_text = 'Site rules, necessary to present the terms under which you moderate content.'


@global_preferences_registry.register
class SiteSubjects(
    LongStringPreference
):
    section = general
    name = 'site_subjects'
    default = ''
    required = False
    help_text = 'Site subjects, showing others if you favor domains over others.'


@global_preferences_registry.register
class MaintenanceMode(
        BooleanPreference
):
    """
    Is the instance in maintenance ?
    """
    section = general
    name = 'maintenance_mode'
    default = False
    help_text = 'Maintenance mode that puts the instance in a mode where users cannot login. UNIMPLEMENTED.'


@global_preferences_registry.register
class RegistrationAllowed(
        preferences.DefaultFromSettingMixin,
        BooleanPreference
):
    """
    Are new registrations allowed ?
    """
    verbose_name = 'Allow new users to register'
    section = general
    name = "registration_allowed"
    default = False
    setting = 'INSTANCE_REGISTRATIONS_OPEN'
    help_text = 'Allow new users to register on the public page. Doesn\'t affect LDAP/CAS nor invitations.'
