import requests
from django.conf import settings
from olki import __version__


def get_user_agent():
    return "OLKi {}; {}".format(
        __version__, settings.INSTANCE_URL
    )


def get_session():
    s = requests.Session()
    s.headers["User-Agent"] = get_user_agent()
    return s
