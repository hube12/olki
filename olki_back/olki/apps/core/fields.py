import django_filters
from django.db import models

from . import search

PRIVACY_LEVEL_CHOICES = [
    ("me", "Only me"),
    ("followers", "Me and my followers"),
    ("instance", "Everyone on my instance, and my followers"),
    ("everyone", "Everyone, including people on other instances"),
]


def get_privacy_field():
    return models.CharField(
        max_length=30, choices=PRIVACY_LEVEL_CHOICES, default="instance"
    )


def privacy_level_query(user, lookup_field="privacy_level", user_field="user"):
    if user.is_anonymous:
        return models.Q(**{lookup_field: "everyone"})

    return models.Q(
        **{"{}__in".format(lookup_field): ["instance", "everyone"]}
    ) | models.Q(**{lookup_field: "me", user_field: user})


class SearchFilter(django_filters.CharFilter):
    def __init__(self, *args, **kwargs):
        self.search_fields = kwargs.pop("search_fields")
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value:
            return qs
        query = search.get_query(value, self.search_fields)
        return qs.filter(query)


class SmartSearchFilter(django_filters.CharFilter):
    def __init__(self, *args, **kwargs):
        self.config = kwargs.pop("config")
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value:
            return qs
        cleaned = self.config.clean(value)
        return search.apply(qs, cleaned)


class AddFlagOneToOneField(models.OneToOneField):
    def __init__(self, *args, **kwargs):
        self.flag_name = kwargs.pop('flag_name')
        super(AddFlagOneToOneField, self).__init__(*args, **kwargs)

    def contribute_to_related_class(self, cls, related):
        super(AddFlagOneToOneField, self).contribute_to_related_class(cls, related)

        def flag(model_instance):
            return hasattr(model_instance, related.get_accessor_name())
        setattr(cls, self.flag_name, property(flag))

    def deconstruct(self):
        name, path, args, kwargs = super(AddFlagOneToOneField, self).deconstruct()
        kwargs['flag_name'] = self.flag_name
        return name, path, args, kwargs
