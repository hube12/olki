import os

from django.core.management.base import BaseCommand

from olki.settings.environment import PROJECT_DIR


class Command(BaseCommand):
    help = 'Converts static index.html to reuseable templates - to be used after collectstatic.'

    def handle(self, *args, **options):
        with open(os.path.join(PROJECT_DIR, 'olki', 'static/index.html'), mode='r') as f:
            lines = f.readlines()

            # below is adapted to the default output of Nuxt (see olki_front/dist/index.html)
            with open(os.path.join(PROJECT_DIR, 'olki', 'templates/pages/_head.html'), "w") as g:
                g.write(lines[3])
            with open(os.path.join(PROJECT_DIR, 'olki', 'templates/pages/_bottom.html'), "w") as g:
                g.write("".join([lines[6], lines[7][:-8]]))
