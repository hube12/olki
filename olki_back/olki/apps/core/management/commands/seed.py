import random
from django.core.management.base import BaseCommand
from django.apps import apps
from olki.fixtures import factories
from actstream import action

app_names = [app.name for app in apps.app_configs.values()]
factories.registry.autodiscover(app_names)
f = factories.registry


class Command(BaseCommand):
    help = 'Seeds the database.'

    def add_arguments(self, parser):
        parser.add_argument('--users',
                            default=5,
                            type=int,
                            help='The number of fake users to create.')
        parser.add_argument('--corpora',
                            default=20,
                            type=int,
                            help='The number of fake corpora to create.')

    def handle(self, *args, **options):
        users = [f['account.User']() for _ in range(options['users'])]
        for _ in range(options['corpora']):
            user = users[random.randint(0, options['users']-1)]
            action.send(
                user.actor,
                verb='Create',
                target=f['corpus.Corpus'](actor=user.actor)
            )
