import graphene
from graphene_django.forms.mutation import DjangoFormMutation
from dynamic_preferences.forms import global_preference_form_builder
from graphql_jwt.decorators import superuser_required


class GlobalPreferencesMutation(DjangoFormMutation):
    class Meta:
        form_class = global_preference_form_builder(section='general')
        only_fields = (
            'general__site_title',
            'general__site_description',
            'general__registration_allowed',
            'general__site_tos',
            'general__site_rules',
            'general__site_subjects',
        )

    @classmethod
    @superuser_required
    def perform_mutate(cls, form, info):
        form.update_preferences()
        return cls(errors=[])


class CoreMutations(graphene.AbstractType):
    editPreferencesGeneral = GlobalPreferencesMutation.Field()
