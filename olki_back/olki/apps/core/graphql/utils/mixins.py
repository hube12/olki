from graphene import Field, String, List
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission
from rolepermissions.permissions import available_perm_status


class PKMixin(object):
    pk = Field(type=String, source='id', description='A Flax ID that is used as a primary key for the model.')


class CanMixin(object):
    can = Field(List(String), name='can', description='A list of permissions the current user has on the object.')

    def resolve_can(self, info):
        content_type = ContentType.objects.get_for_model(self.__class__._meta.model)
        permissions_for_model = [i.codename for i in Permission.objects.filter(content_type=content_type)]

        if info.context.user:
            return [i for i in permissions_for_model if i in available_perm_status(info.context.user)]
        return []
