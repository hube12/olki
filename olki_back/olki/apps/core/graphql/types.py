import graphene
from django.db.models.query import QuerySet


class CountableConnectionBase(graphene.Connection):
    """Connection subclass that supports totalCount."""

    class Meta:
        abstract = True

    total_count = graphene.Int()

    def resolve_total_count(self, info, **kwargs):
        if isinstance(self.iterable, QuerySet):
            return self.iterable.count()
        return len(self.iterable)
