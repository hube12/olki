import logging
from promise import is_thenable

logger = logging.getLogger(__name__)


class DebugMiddleware(object):
    def on_error(self, error):
        print(error)

    def resolve(self, next, root, info, **args):
        result = next(root, info, **args)
        if info.parent_type.__str__() in ['Queries', 'Mutations']:
            logger.info("{operation}".format(
                operation=info.operation.selection_set.selections[0].name.value
            ))
        if is_thenable(result):
            result.catch(self.on_error)

        return result
