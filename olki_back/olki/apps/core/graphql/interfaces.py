from graphene import String
from graphene.types import Interface


class AuthorInterface(Interface):
    name = String(required=True)
    orcid = String()


class ActionTargetInterface(Interface):
    pk = String()
    title = String(required=True)
