from graphql_jwt.shortcuts import get_user_by_token
from graphql_jwt.utils import get_credentials, get_user_by_natural_key
from rest_framework import authentication


class GraphqlJWTAuthentication(authentication.BaseAuthentication):
    """
    An authentication plugin that authenticates requests through a JSON web
    token provided in a request header.
    """
    def authenticate(self, request=None, **kwargs):
        if request is None:
            return None

        token = get_credentials(request, **kwargs)

        if token is not None:
            return get_user_by_token(token, request), None

        return None

    def get_user(self, user_id):
        return get_user_by_natural_key(user_id)
