import graphene
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from graphql_jwt.decorators import login_required

from .apps.account.mutations import UserMutations
from .apps.account.schema import UserQuery, Viewer, ActorQuery
from .apps.action.schema import ActionQuery
from .apps.core.mutations import CoreMutations
from .apps.corpus.mutations import CorpusMutations
from .apps.corpus.schema import CorpusQuery
from .apps.note.mutations import NoteMutations
from .apps.profile.mutations import ProfileMutations
from .apps.oaipmh.mutations import OaipmhMutations
from .apps.federation.schema import ActivityQuery


class Queries(
    UserQuery,
    ActorQuery,
    CorpusQuery,
    ActionQuery,
    ActivityQuery,
    graphene.ObjectType
):
    """Queries wrapper
    This class inherits from multiple Queries
    as we add more apps queries to our project

    Note it is an instance (graphene.ObjectType)
    inheriting from interfaces (graphene.AbstractType)
    """
    if settings.DEBUG:
        from graphene_django.debug import DjangoDebug
        debug = graphene.Field(DjangoDebug, name='debug')

    viewer = graphene.Field(Viewer, description=_(
        'The viewer field represents the currently logged-in user.'
        'Its subfields expose data that are contextual to the user.'
    ))

    @login_required
    def resolve_viewer(self, info, **kwargs):
        return info.context.user


class Mutations(
    CoreMutations,
    UserMutations,
    ProfileMutations,
    CorpusMutations,
    NoteMutations,
    OaipmhMutations,
    graphene.ObjectType
):
    """Mutations wrapper
    This class inherits from multiple Mutations
    as we add more apps mutations to our project

    Note it is an instance (graphene.ObjectType)
    inheriting from interfaces (graphene.AbstractType)
    """
    pass


schema = graphene.Schema(
    query=Queries,
    mutation=Mutations
)
