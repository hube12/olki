# OLKi BE

The backend is meant to run on a WSGI server, served as a local service exposed by a reverse-proxy.

## Goals and non-goals

### Goals

- Python >= 3.7
- Django > 2.0
- API via GraphQL
- no web client integrated
- Off-load heavy tasks to specialised software (caching to Redis, persistence to PostgreSQL, for instance)

### Non-goals

- Python < 3.7

## Building

OLKi BE requires Python 3.7+, and dependencies of the code are dealt with by `Poetry`, all of which can be installed via `make deps-dev`.

To serve it for production (from OLKi's root):

```bash
make deps-prod-be serve-prod
```

### Updating

To keep your version of OLKI FE up to date, you can use `make` and `git` to check out the latest release (from OLKi's root):

```bash
make update-release
```

You might need to *migrate* (modify the table schemas in you database) if changes were made to the schemas.
Most migrations are innocuous, but please read the release notes for any precautions or time required to run them.

### Developing

The backend is using the Django web framework, and features are implemented as
Django apps. The WSGI entrypoint is usually replaced by Django's own development
server during development.

### Components used

See the [CREDITS](../CREDITS.md) file.


